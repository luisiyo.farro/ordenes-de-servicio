jQuery.sap.require("sap.m.MessageBox");
sap.ui.define([
	"dms4100ui/bacrcr001ord/controller/BaseController",
	"sap/m/MessageToast",
	"sap/ui/core/Fragment",
	"dms4100ui/bacrcr001ord/moment"
], function (BaseController, MessageToast, Fragment, Moment) {
	"use strict";
	var Z_OD_SCP_BASRRC001_RES_SRV;
	var Z_OD_SCP_BASRRC001_ORD_SRV;
	var Z_OD_SCP_BASRRC001_SRV;
	//var Z_OD_SCP_BASRRC001_CAT_SRV;
	var Z_OD_SCP_BAVNVT001_SRV;
	var Z_OD_SCP_CORE_0001_SRV;
	var ZCDS_VW_BASRRC001_204_CDS;
	//var ZCDS_VW_BASRRC001_116_CDS;
	var Z_OD_SCP_BASRRC001_XENTRY_SRV;
	var Z_OD_SCP_BASRRC001_SALESFORCE_SRV;
	var Z_OD_SCP_BACRCR002_SRV;
	var ZCDS_VW_BASRRC001_211_CDS;
	var Z_OD_SCP_BASRRC001_TAB_SRV;
	var ZCDS_VW_BASRRC001_216_CDS;
	var Z_OD_SCP_BASRTM001_CHIPS_SRV;
	var thes;
	var Aufnr;
	var equip;
	var Werks;
	var Bukrs;
	var Maufnr;
	var Father;
	var Hojap;
	var Kunum;
	var Expres;
	var userInfo;
	var userAttributes;
	var Operaciones_vornr_real;
	var Incidentes_ZINC;
	var Operaciones_updated;
	var Incidentes_updated;
	var Objid;
	var fechaHoy = new Date();
	var Scpapp = "bacrcr001ord";
	var Eqfnr;
	var FilterRefacciones;
	//var MensajesXentryPortal;

	return BaseController.extend("dms4100ui.bacrcr001ord.controller.Detalle", {

		onInit: function () {
			var oRouter = this.getRouter();
			oRouter.getRoute("detalle").attachMatched(this._onRouteMatched, this);
		},

		_onRouteMatched: function (oEvent) {
			thes = this;
			var oArgs = oEvent.getParameter("arguments");
			Aufnr = oArgs.Aufnr;
			Werks = oArgs.Werks;
			Maufnr = oArgs.Maufnr;
			Father = oArgs.Father;
			Hojap = oArgs.Hojap;
			Kunum = oArgs.Kunum;
			Expres = oArgs.Expres;
			Eqfnr = oArgs.Eqfnr;

			Z_OD_SCP_BASRRC001_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_SRV");

			Z_OD_SCP_BASRRC001_ORD_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_ORD_SRV");

			//Z_OD_SCP_BASRRC001_CAT_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_CAT_SRV");

			Z_OD_SCP_BAVNVT001_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BAVNVT001_SRV");

			Z_OD_SCP_CORE_0001_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_CORE_0001_SRV");

			ZCDS_VW_BASRRC001_204_CDS = this.getOwnerComponent().getModel("ZCDS_VW_BASRRC001_204_CDS");

			Z_OD_SCP_BASRRC001_XENTRY_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_XENTRY_SRV");

			Z_OD_SCP_BASRRC001_SALESFORCE_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_SALESFORCE_SRV");

			Z_OD_SCP_BACRCR002_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BACRCR002_SRV");

			ZCDS_VW_BASRRC001_211_CDS = this.getOwnerComponent().getModel("ZCDS_VW_BASRRC001_211_CDS");

			Z_OD_SCP_BASRRC001_TAB_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_TAB_SRV");

			ZCDS_VW_BASRRC001_216_CDS = this.getOwnerComponent().getModel("ZCDS_VW_BASRRC001_216_CDS");

			Z_OD_SCP_BASRTM001_CHIPS_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRTM001_CHIPS_SRV");

			//MensajesXentryPortal = [];

			this.byId("smartTableIncidentes").setModel(Z_OD_SCP_BASRRC001_ORD_SRV);
			this.byId("smartTableOperaciones").setModel(Z_OD_SCP_BASRRC001_ORD_SRV);
			this.byId("smartTableRefacciones").setModel(Z_OD_SCP_BASRRC001_ORD_SRV);
			//this.byId("smartChartKPI").setModel(Z_OD_SCP_BASRRC001_ORD_SRV);
			//this.byId("smartChartKPI").setModel(ZCDS_VW_BASRRC001_116_CDS);

			Z_OD_SCP_BASRRC001_RES_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_RES_SRV");

			var url = "/OrdenIncidenteNew(aufnr='0000000',vornr='0000000000000000')/to_oper";
			this.getView().byId("smartTableOperaciones").setTableBindingPath(url);
			//this.getView().byId("smartTableOperaciones").rebindTable();

			var url2 = "/OrdenOperacion(aufnr='0000000',vornr='0000000000000000')/to_ref";
			this.getView().byId("smartTableRefacciones").setTableBindingPath(url2);
			//this.getView().byId("smartTableRefacciones").rebindTable();

			// var url3 = "/OrdenHeader(aufnr='0000000',maufnr='0000000')/to_zinc";
			// this.getView().byId("smartTableIncidentes").setTableBindingPath(url3);
			// this.getView().byId("smartTableIncidentes").rebindTable();

			thes.byId("smartTableOperaciones").getModel().refresh();
			thes.byId("smartTableRefacciones").getModel().refresh();
			thes.byId("smartTableIncidentes").getModel().refresh();
			this.byId("smartTableIncidentes").getModel().removeData();
			this.byId("smartTableOperaciones").getModel().removeData();
			this.byId("smartTableRefacciones").getModel().removeData();

			this.byId("smartTableIncidentes").rebindTable();
			this.byId("smartTableOperaciones").rebindTable();
			this.byId("smartTableRefacciones").rebindTable();

			this.obtenerCabeceraOrden2();
			//this.obtenerCliente();
			this.obtenerAdendaCliente();
			this.obtenerAnticipos();
			this.obtenerFlujoDocumentos();
			this.obtenerUsuario();
			Objid = "";
			this.obtenerTecnicoDefault();

			thes.byId("cbAdendaCliente").setSelectedKey("");
			thes.byId("adendasCabecera").destroyContent();
			var jsonModel = new sap.ui.model.json.JSONModel([]);
			thes.getView().setModel(jsonModel, "adendaDetalle");

			var opsOperaciones = this.byId("opsInformacionGeneral");
			this.byId("ObjectPageLayout").setSelectedSection(opsOperaciones);
		},

		obtenerUsuario: function () {

			var url = "/bacrcr002-srv/security/getAuthInfo()";

			sap.ui.core.BusyIndicator.show();
			$.ajax({
				url: url,
				async: false,
				success: function (result) {
					userInfo = result.userInfo;
					userAttributes = result.userAttributes;

					if (userAttributes.ROLNAME.length === 0) {
						sap.m.MessageBox.error("No dispone de ningún Rol en el atributo (ROLNAME), contacte al administrador de sistema");
						return;
					}

					thes.obtenerControlesObjetos(userAttributes.ROLNAME);

					var jsonModel = new sap.ui.model.json.JSONModel(userInfo);
					thes.getView().setModel(jsonModel, "userInfo");

					var jsonModel2 = new sap.ui.model.json.JSONModel(userAttributes);
					thes.getView().setModel(jsonModel2, "userAttributes");

					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

			if (!userInfo.loginName) {
				userInfo.loginName = "Prueba";
			}

		},

		onBorrarDocumentosAdjuntos: function (oEvent) {
			var json = {
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": Aufnr,
				"DocumentsSet": [
					{
						"Aufnr": Aufnr,
						"Archivo": oEvent.mParameters.listItem.getList().getItems()[0].getContent()[0].getItems()[1].getItems()[0].getTarget(),
						"Descrip": oEvent.mParameters.listItem.getList().getItems()[0].getContent()[0].getItems()[1].getItems()[0].getText(),
						"IndDelete": "X"
					}
				]
			};
			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/SaveDocumentSet", json, {
				success: function (result, status) {
					thes.obtenerIdDocumentos("ORD");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.m.MessageBox.error("Error al guardar documento en SAP");
					sap.ui.core.BusyIndicator.hide();
				}
			});
		},

		obtenerEquiposParaAgregar: function (filters1) {
			var filters = [];
			var filter = new sap.ui.model.Filter("bukrs", sap.ui.model.FilterOperator.EQ, Bukrs);
			filters.push(filter);
			var filter = new sap.ui.model.Filter("werks", sap.ui.model.FilterOperator.EQ, Werks);
			filters.push(filter);

			var filter = new sap.ui.model.Filter({
				filters: filters1,
				and: true
			});

			filters.push(filter);
			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_TAB_SRV.read("/ZCDS_VW_BASRRC001_061", {
				filters: filters,
				success: function (result) {
					sap.ui.core.BusyIndicator.hide();
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "EquiposParaAgregar");
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onItemPressEliminarEquipo: function (oEvent) {
			var path = oEvent.getParameters().item.getBindingContext("EquiposAdicionales").getPath();
			var index = path.slice(1);
			var data = thes.getView().getModel("EquiposAdicionales").getData();
			var reg = data[index];

			if (reg.memoria) {
				data.splice(index, 1);
				thes.filtrarComboBoxAgregarEquipo();
			} else {
				thes.EliminarEquipo(reg, data, index);
			}
			thes.getView().getModel("EquiposAdicionales").refresh();
		},

		EliminarEquipo: function (reg, data, index) {

			var json = {
				"Aufnr": reg.Orden,
				"Vornr": reg.IncKey,
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Orden": [
					{
						"Aufnr": reg.Orden,
						"Equip": reg.equip,
						"Pctcomi": Number(reg.pctcomi).toFixed(2),
						"Deleted": "X"
					}
				]
			};

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/HdEqIncSet", json, {
				success: function (result, status) {
					sap.ui.core.BusyIndicator.hide();
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					data.splice(index, 1);
					thes.filtrarComboBoxAgregarEquipo();
					var indices = thes.byId("smartTableIncidentes").getTable().getSelectedIndices();
					var reg = thes.byId("smartTableIncidentes").getTable().getContextByIndex(indices[0]).getProperty();
					thes.obtenerEquiposInicidente(reg.vornr_real);
				},
				error: function (err) {
					sap.m.MessageBox.error("Error al eliminar equipo");
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onGuardarEquiposAdicionales: function () {
			var dataEquiposAdicionales = thes.getView().getModel("EquiposAdicionales").getData();
			var suma = 0;
			for (var i = 0; i < dataEquiposAdicionales.length; i++) {
				suma = suma + Number(dataEquiposAdicionales[i].pctcomi);
				if (Number(dataEquiposAdicionales[i].pctcomi) == 0) {
					sap.m.MessageBox.error("El equipo " + dataEquiposAdicionales[i].equip + " no puede tener un % igual a 0");
					return;
				}
			}

			if (suma !== 100) {
				sap.m.MessageBox.error("El total de los % debe ser igual a 100");
				return;
			}

			var data = thes.getView().getModel("EquiposAdicionales").getData();
			var equipos = [];
			for (var i = 0; i < data.length; i++) {
				if (data[i].Principal !== "X") {
					var equipo = {
						"Aufnr": data[i].Orden,
						"Equip": data[i].equip,
						"Pctcomi": Number(data[i].pctcomi).toFixed(2),
						"Deleted": ""
					};
					equipos.push(equipo);
				}
			};

			var json = {
				"Aufnr": data[0].Orden,
				"Vornr": data[0].IncKey,
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Orden": equipos
			};

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/HdEqIncSet", json, {
				success: function (result, status) {
					sap.ui.core.BusyIndicator.hide();
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					var indices = thes.byId("smartTableIncidentes").getTable().getSelectedIndices();
					var reg = thes.byId("smartTableIncidentes").getTable().getContextByIndex(indices[0]).getProperty();
					thes.obtenerEquiposInicidente(reg.vornr_real);
				},
				error: function (err) {
					sap.m.MessageBox.error("Error al eliminar equipo");
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onAgregarEquipo: function () {
			var indices = this.byId("smartTableIncidentes").getTable().getSelectedIndices();

			if (indices.length > 1) {
				sap.m.MessageBox.error("Seleccione un incidente");
				return;
			}
			var key = thes.byId("cbEquiposParaAgregar").getSelectedKey();

			if (!key) {
				return;
			}

			var regEquipo = thes.getView().getModel("EquiposParaAgregar").getData().find(element => element.equip === key);
			var regIncidente = this.byId("smartTableIncidentes").getTable().getContextByIndex(indices[0]).getProperty();

			var json = {
				Orden: Aufnr,
				IncKey: regIncidente.vornr_real,
				equip: regEquipo.equip,
				EqDescr: regEquipo.descripcion,
				Tecnico: regEquipo.Tecnico,
				pctcomi: 0,
				Principal: "",
				memoria: "X"
			};

			var dataEquiposAdicionales = thes.getView().getModel("EquiposAdicionales").getData();
			dataEquiposAdicionales.push(json);
			thes.getView().getModel("EquiposAdicionales").refresh();
			thes.filtrarComboBoxAgregarEquipo();
			thes.byId("cbEquiposParaAgregar").setSelectedKey("");
		},

		filtrarComboBoxAgregarEquipo: function () {
			var dataEquiposAdicionales = thes.getView().getModel("EquiposAdicionales").getData();
			var filters = [];
			for (var i = 0; i < dataEquiposAdicionales.length; i++) {
				var filter = new sap.ui.model.Filter("equip", sap.ui.model.FilterOperator.NE, dataEquiposAdicionales[i].equip);
				filters.push(filter);
			}

			var filter = new sap.ui.model.Filter({
				filters: filters,
				and: true
			});

			thes.byId("cbEquiposParaAgregar").getBinding("items").filter(filter);
		},

		onItemPressDetalleEquipo: function (oEvent) {
			var reg = oEvent.getParameters().item.getBindingContext("EquiposAdicionales").getProperty();
			thes.obtenerMiembrosEquipo(reg.equip);
		},

		obtenerMiembrosEquipo: function (equipo) {

			var filters = [];

			var filter = new sap.ui.model.Filter("equip", sap.ui.model.FilterOperator.EQ, equipo);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_TAB_SRV.read("/ZCDS_VW_BASRRC001_062", {
				filters: filters,
				success: function (result) {
					sap.ui.core.BusyIndicator.hide();
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "MiembrosEquipo");
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerEquiposInicidente: function (vornr_real) {

			var filters = [];

			var filter = new sap.ui.model.Filter("Orden", sap.ui.model.FilterOperator.EQ, Aufnr);
			filters.push(filter);

			var filter = new sap.ui.model.Filter("IncKey", sap.ui.model.FilterOperator.EQ, vornr_real);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/ZCDS_VW_EQINCORD", {
				filters: filters,
				urlParameters: {
					"$orderby": "Principal desc"
				},
				success: function (result) {
					sap.ui.core.BusyIndicator.hide();
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "EquiposAdicionales");
					thes.getView().setModel(new sap.ui.model.json.JSONModel([]), "MiembrosEquipo");
					var filters = [];
					for (var i = 0; i < result.results.length; i++) {
						var filter = new sap.ui.model.Filter("equip", sap.ui.model.FilterOperator.NE, result.results[i].equip);
						filters.push(filter);
					}

					thes.obtenerEquiposParaAgregar(filters);

				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onDialogoAsignarEquiposAdicionales: function () {
			var indices = this.byId("smartTableIncidentes").getTable().getSelectedIndices();

			if (indices.length > 1) {
				sap.m.MessageBox.error("Seleccione un registro");
				return;
			}

			var reg = this.byId("smartTableIncidentes").getTable().getContextByIndex(indices[0]).getProperty();
			thes.obtenerEquiposInicidente(reg.vornr_real);
			//thes.obtenerEquiposParaAgregar();

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgAsignarEquiposAdicionales")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					controller: this,
					name: "dms4100ui.bacrcr001ord.view.AsignarEquiposAdicionales"
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgAsignarEquiposAdicionales").open();
			}
		},

		onCgEquiposAdicionales: function (oEvent) {
			var datos = thes.getView().getModel("EquiposAdicionales").getData();
			var suma = 0;
			var indexPrincipal;
			for (var i = 0; i < datos.length; i++) {
				if (datos[i].Principal !== "X") {
					if (i === oEvent.getSource().getParent().getIndex()) {
						suma = suma + Number(oEvent.getParameter("value"));
					} else {
						suma = suma + Number(datos[i].pctcomi);
					}

				} else {
					indexPrincipal = i;
				}
			}
			if (suma >= 100) {
				sap.m.MessageBox.error("El total de los % no puede ser mayor a 100");
				oEvent.getSource().setValue(0);
				thes.getView().getModel("EquiposAdicionales").refresh();
				return;
			}

			datos[indexPrincipal].pctcomi = 100 - suma;
			thes.getView().getModel("EquiposAdicionales").refresh();
		},

		onDialogoVisualizarEquipo: function () {

			var indices = this.byId("smartTableIncidentes").getTable().getSelectedIndices();

			if (indices.length > 1) {
				sap.m.MessageBox.error("Seleccione un registro");
				return;
			}

			var reg = this.byId("smartTableIncidentes").getTable().getContextByIndex(indices[0]).getProperty();

			equip = reg.equip;

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgVisualizarEquipo")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					controller: this,
					name: "dms4100ui.bacrcr001ord.view.VisualizarEquipo"
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
					thes.byId("smartTableVisualizarEquipo").setModel(Z_OD_SCP_BASRRC001_TAB_SRV);
					thes.byId("smartTableVisualizarEquipo").getModel().refresh();
				});
			} else {
				this.byId("dlgVisualizarEquipo").open();
				thes.byId("smartTableVisualizarEquipo").rebindTable();
			}

		},

		onBeforeRebindVisualizarEquipo: function (oEvent) {

			this.byId("smartTableVisualizarEquipo").setModel(Z_OD_SCP_BASRRC001_TAB_SRV);
			var binding = oEvent.getParameter("bindingParams");
			var oFilter = new sap.ui.model.Filter("equip", sap.ui.model.FilterOperator.EQ, equip);
			binding.filters.push(oFilter);
			this.getView().byId("smartTableVisualizarEquipo").getModel().refresh();

		},
		
		onbtnGuardarAdenda: function () {

			var posicionAdenda = {
				"Vbeln": thes.cabecera.eqfnr,
				"Posnr": "",
				"Codetiq": "",
				"Valdesc": ""
			};

			var jsonAdenda = {
				"Vbeln": thes.cabecera.eqfnr,
				"Adeped": []
			};

			var itemsCabecera = this.byId("adendasCabecera").getContent();

			for (var i = 0; i < itemsCabecera.length; i++) {
				var element = this.byId("adendasCabecera").getContent()[i].getMetadata().getElementName();
				switch (element) {
					case "sap.m.Input":
						var codetiq = this.byId("adendasCabecera").getContent()[i].getCustomData()[0].getValue();
						var valor = this.byId("adendasCabecera").getContent()[i].getValue();
						var cabeceraAdenda = {
							"Vbeln": thes.cabecera.eqfnr,
							"Posnr": "1",
							"Codetiq": codetiq,
							"Valdesc": valor
						};

						jsonAdenda.Adeped.push(cabeceraAdenda);
						break;
				}
			}

			var detalle = this.getView().getModel("adendaDetalle").getData();
			var u = 2;

			for (var e = 0; e < detalle.length; e++) {

				var posicionAdenda = {
					//"Posnr": (u + e).toString(),
					"Vbeln": thes.cabecera.eqfnr,
					//"Posnr": detalle[e].contop,
					"Posnr": detalle[e].posnr,
					"Codetiq": detalle[e].codetiq,
					"Valdesc": detalle[e].valor
				};
				jsonAdenda.Adeped.push(posicionAdenda);
			}

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BAVNVT001_SRV.create("/ModAdeSet", jsonAdenda, {
				success: function (result, status) {

					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}

					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onDlgCrearPersonaContacto: function () {

			var oView = this.getView();

			if (!this.byId("dlgCrearPersonaContacto")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					controller: this,
					name: "dms4100ui.bacrcr001ord.view.CrearPersonaContacto"
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgCrearPersonaContacto").open();
			}

			var json = {
				Partner: Kunum,
				Contacto: "",
				TipoDeIcPco: "",
				RfcPco: "",
				NombrePco: "",
				ApellidoPco: "",
				EMailPco: "",
				TelefonoPco: "",
				TelExtPco: "",
				CountryPco: "",
				RegionPco: "",
				City1Pco: "",
				City2Pco: "",
				StrSuppl1Pco: "",
				StreetPco: "",
				HouseNoPco: "",
				PostlCod1Pco: "",
				FaxPco: "",
				FaxExtPco: "",
				paauthPco: "",
				pavipPco: "",
				CodCita: thes.cabecera.cod_cita
			};

			var jsonModel = new sap.ui.model.json.JSONModel(json);
			thes.getView().setModel(jsonModel, "CrearPersonaContacto");

			this.obtenerPersonaContacto();
			this.obtenerMedioContacto();
			this.obtenerPaises();
		},

		onCrearPersonaContacto: function () {

			var json = thes.getView().getModel("CrearPersonaContacto").getData();

			if (this.byId("switchQuiereSerContactado").getState() == true) {
				json.pavipPco = "1";
			} else {
				json.pavipPco = "0";
			}

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/CreaContactoSet", json, {
				success: function (result, status) {
					thes.byId("dlgCrearPersonaContacto").close();
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					thes.obtenerCabeceraOrden();
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					thes.obtenerBitacora();
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onPressEstadoXentryPortal: function () {

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgLogXentry")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					controller: this,
					name: "dms4100ui.bacrcr001ord.view.LogXentry"
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
					oDialog.setModel(Z_OD_SCP_BASRRC001_XENTRY_SRV);
					oDialog.getModel().refresh();
				});
			} else {
				this.byId("dlgLogXentry").open();
				this.byId("smartTableLogIncidentes").rebindTable();
				this.byId("smartTableLogOperaciones").rebindTable();
				this.byId("smartTableLogRefacciones").rebindTable();
			}

			//this.byId("dlgLogXentry").setModel(Z_OD_SCP_BASRRC001_XENTRY_SRV);
			//this.byId("dlgLogXentry").getModel().refresh;

			// this.byId("smartTableLogIncidentes").setModel(Z_OD_SCP_BASRRC001_XENTRY_SRV);
			// this.byId("smartTableLogOperaciones").setModel(Z_OD_SCP_BASRRC001_XENTRY_SRV);
			// this.byId("smartTableLogRefacciones").setModel(Z_OD_SCP_BASRRC001_XENTRY_SRV);

			// this.byId("smartTableLogIncidentes").getModel().refresh;
			// this.byId("smartTableLogOperaciones").getModel().refresh;
			// this.byId("smartTableLogRefacciones").getModel().refresh;

		},

		onbtnEnviarXentry: function () {

			var json = {
				"Orden": Aufnr
			};

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_XENTRY_SRV.create("/SendOrdXentrySet", json, {
				method: "POST",
				success: function (result, status) {

					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					} else {
						thes.getRouter().navTo("home");
					}
					sap.ui.core.BusyIndicator.hide();
					thes.obtenerCabeceraOrden();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onViewPDF: function (data, nombre) {
			var datos = data;
			var objbuilder = '';
			objbuilder += ('<object width="100%" height="100%" data="data:application/pdf;base64,');
			objbuilder += (datos);
			objbuilder += ('" type="application/pdf" class="internal">');
			objbuilder += ('<embed src="data:application/pdf;base64,');
			objbuilder += (datos);
			objbuilder += ('" type="application/pdf" />');
			objbuilder += ('</object>');
			var win = window.open("#", "_blank");
			var title = nombre
			win.document.write('<html><title>' + title +
				'</title><body style="margin-top:0px; margin-left: 0px; margin-right: 0px; margin-bottom: 0px;">');
			win.document.write(objbuilder);
			win.document.write('</body></html>');
			sap.ui.core.BusyIndicator.hide();
		},

		ordenPDF: function () {

			var filters = [];

			var filter = new sap.ui.model.Filter("aufnr", sap.ui.model.FilterOperator.EQ, Aufnr);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/GetPDFCamionSet", {
				filters: filters,
				success: function (result, status) {
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					if (result.results.length > 0) {
						if (result.results[0].PDF) {
							thes.onViewPDF(result.results[0].PDF, "SmartForms");
						} else {
							sap.ui.core.BusyIndicator.hide();
						}
					} else {
						sap.ui.core.BusyIndicator.hide();
					}
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onImprimir: function () {

			var json = {};

			json.Aufnr = Aufnr;

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdValdtSet", json, {
				success: function (result, status) {
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						var flag = true;

						for (var i = 0; i < mensajes.length; i++) {
							if (mensajes[i].Type === "error") {
								flag = false;
							}
						}


						if (flag) {

							var json = {
								"HdId": "1",
								"Scpuser": userInfo.loginName,
								"Scpapp": Scpapp,
								"Aufnr": Aufnr
							};

							sap.ui.core.BusyIndicator.show();
							Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenCierreSet", json, {
								success: function (result, status) {
									if (status.headers["sap-message"]) {
										var mensaje = JSON.parse(status.headers["sap-message"]);
										var mensajes = thes.armarMensaje(mensaje);
										thes.mostrarMensajes(mensajes, thes);
									}
									sap.ui.core.BusyIndicator.hide();
									thes.ordenPDF();
								},
								error: function (err) {
									sap.m.MessageBox.error("Error en el Pre-Cierre");
									sap.ui.core.BusyIndicator.hide();
								}
							});

						} else {
							thes.mostrarMensajes(mensajes, thes);
						}


					}
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onImprimirTot: function () {

			var indices = this.byId("smartTableOperaciones").getTable().getSelectedIndices();

			if (indices.length > 1) {
				sap.m.MessageBox.error("Seleccione solo un registro");
				return;
			}

			if (indices.length === 0) {
				sap.m.MessageBox.error("Seleccione un registro");
				return;
			}


			for (var i = 0; i < indices.length; i++) {
				var reg = this.byId("smartTableOperaciones").getTable().getContextByIndex(indices[i]).getProperty();

				if (reg.USR00.indexOf("TOT") === -1) {
					sap.m.MessageBox.error("Seleccione un registro TOT");
					return;
				}

				var filters = [];

				var filter = new sap.ui.model.Filter("aufnr", sap.ui.model.FilterOperator.EQ, Aufnr);
				filters.push(filter);

				var filter = new sap.ui.model.Filter("Operac", sap.ui.model.FilterOperator.EQ, reg.USR00);
				filters.push(filter);

				thes.obtenerPDFTot(filters);

			}



		},

		obtenerPDFTot: function (filters) {

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/GetPDFTOTSet", {
				filters: filters,
				success: function (result) {
					if (result.results.length > 0) {
						thes.onViewPDF(result.results[0].PDF, "SmartForms");
					} else {
						sap.ui.core.BusyIndicator.hide();
					}

				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		PreFacturaPDF: function () {

			var json = {};

			json.Aufnr = Aufnr;

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdValdtSet", json, {
				success: function (result, status) {
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						var flag = true;

						for (var i = 0; i < mensajes.length; i++) {
							if (mensajes[i].Type === "error") {
								flag = false;
							}
						}


						if (flag) {

							var filters = [];

							var filter = new sap.ui.model.Filter("aufnr", sap.ui.model.FilterOperator.EQ, Aufnr);
							filters.push(filter);

							sap.ui.core.BusyIndicator.show(4);
							Z_OD_SCP_BASRRC001_ORD_SRV.read("/PDFPreFacturaSet", {
								filters: filters,
								success: function (result) {
									thes.onViewPDF2(result.results[0].PDF, "SmartForms");
								},
								error: function (err) {
									sap.ui.core.BusyIndicator.hide();
								}
							});

						} else {
							thes.mostrarMensajes(mensajes, thes);
						}


					}
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onImprimirPreFactura: function () {

			var json = {};

			json.Aufnr = Aufnr;

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdValdtSet", json, {
				success: function (result, status) {
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						var flag = true;

						for (var i = 0; i < mensajes.length; i++) {
							if (mensajes[i].Type === "error") {
								flag = false;
							}
						}


						if (flag) {

							var json = {
								"HdId": "1",
								"Scpuser": userInfo.loginName,
								"Scpapp": Scpapp,
								"Aufnr": Aufnr
							};

							sap.ui.core.BusyIndicator.show();
							Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenCierreSet", json, {
								success: function (result, status) {
									if (status.headers["sap-message"]) {
										var mensaje = JSON.parse(status.headers["sap-message"]);
										var mensajes = thes.armarMensaje(mensaje);
										thes.mostrarMensajes(mensajes, thes);
									}
									sap.ui.core.BusyIndicator.hide();
									thes.PreFacturaPDF();
								},
								error: function (err) {
									sap.m.MessageBox.error("Error en el Pre-Cierre");
									sap.ui.core.BusyIndicator.hide();
								}
							});

						} else {
							thes.mostrarMensajes(mensajes, thes);
						}


					}
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onSetSel: function (oEvent) {
			var index = oEvent.getSource().getParent().getIndex();
			var indices = oEvent.getSource().getParent().getParent().getSelectedIndices();
			indices.push(index);
			oEvent.getSource().getParent().getParent().setSelectedIndices(indices);
		},

		onCheckInmovilizacion: function (oEvent) {
			var enabled = false;
			if (oEvent) {
				enabled = oEvent.getParameters().selected;
			}

			var habilitacion = { enabled: enabled }
			var jsonModel = new sap.ui.model.json.JSONModel(habilitacion);
			thes.getView().setModel(jsonModel, "habilitacion");

			this.byId("cbInmovilizacion").setSelectedKey();
			this.byId("iptComentarioMotivo").setValue();
			this.byId("dpFechaInmovilizaion").setValue();
			this.byId("dpFechaDesinmovilizaion").setValue();

		},

		onReaperturarOrden: function () {
			var json = {
				"Orden": Aufnr,
				"Scpapp": Scpapp,
				"Scpuser": userInfo.loginName
			};

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/ReOpenOrdSet", json, {
				success: function (result, status) {
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});
		},

		obtenerFlujoDocumentos: function () {

			var filters = [];

			var filter = new sap.ui.model.Filter("aufnr", sap.ui.model.FilterOperator.EQ, Aufnr);
			filters.push(filter);

			this.byId("opsFlujoDocumentos").setBusy(true);
			ZCDS_VW_BASRRC001_204_CDS.read("/ZCDS_VW_BASRRC001_204", {
				urlParameters: {
					"$expand": "to_DocFin,to_DocCon"
				},
				filters: filters,
				success: function (result) {

					if (result.results[0].aufnr) {
						result.results[0].estadoAufnr = "Positive";
					} else {
						result.results[0].estadoAufnr = "Planned";
					}

					if (result.results[0].rsnum) {
						result.results[0].estadoRsnum = "Positive";
					} else {
						result.results[0].estadoRsnum = "Planned";
					}

					if (result.results[0].cod_cita !== "0000000000") {
						result.results[0].estadoCod_cita = "Positive";
					} else {
						result.results[0].estadoCod_cita = "Planned";
					}

					if (result.results[0].qmnum) {
						result.results[0].estadoQmnum = "Positive";
					} else {
						result.results[0].estadoQmnum = "Planned";
					}

					if (result.results[0].Ped_Venta) {
						result.results[0].estadoPed_Venta = "Positive";
					} else {
						result.results[0].estadoPed_Venta = "Planned";
					}

					if (result.results[0].vbeln) {
						result.results[0].estadoVbeln = "Positive";
					} else {
						result.results[0].estadoVbeln = "Planned";
					}

					if (result.results[0].to_DocFin.reserva) {
						result.results[0].estadoDocRes = "Positive";
					} else {
						result.results[0].estadoDocRes = "Planned";
					}

					if (result.results[0].to_DocFin.factura) {
						result.results[0].estadoDocFac = "Positive";
					} else {
						result.results[0].estadoDocFac = "Planned";
					}

					if (result.results[0].to_DocCon.results.length > 0) {
						if (result.results[0].to_DocCon.results[0].doc_compra) {
							result.results[0].estadoDocComp = "Positive";
						} else {
							result.results[0].estadoDocComp = "Planned";
						}
					}
					else {
						result.results[0].estadoDocComp = "Planned";
					}

					if (result.results[0].to_DocCon.results.length > 0) {
						if (result.results[0].to_DocCon.results[0].hoja_ruta) {
							result.results[0].estadoHojaRuta = "Positive";
						} else {
							result.results[0].estadoHojaRuta = "Planned";
						}
					} else {
						result.results[0].estadoHojaRuta = "Planned";
					}

					var documentoCompra = "";
					var hes = "";

					for (var i = 0; i < result.results[0].to_DocCon.results.length; i++) {
						documentoCompra = documentoCompra + " " + result.results[0].to_DocCon.results[i].doc_compra;
						hes = hes + " " + result.results[0].to_DocCon.results[i].hoja_ruta;
					}

					var node = {
						"nodes": [
							{
								"id": "1",
								"lane": "0",
								"title": "No. de Servicio\n" + result.results[0].cod_cita,
								"titleAbbreviation": result.results[0].cod_cita,
								"children": [2],
								"state": result.results[0].estadoCod_cita,
								"stateText": "Listo",
								"focused": false,
								"texts": "Generado en la " + result.results[0].TipServ + "\nFecha: " + thes.dateToFecha(result.results[0].fecha_inicio)
							},
							{
								"id": "2",
								"lane": "1",
								"title": "Orden\n" + result.results[0].aufnr,
								"titleAbbreviation": result.results[0].aufnr,
								"children": [3],
								"state": result.results[0].estadoAufnr,
								"stateText": "Listo",
								"focused": false,
								"texts": "Fecha: " + thes.dateToFecha(thes.sumaFecha(1, result.results[0].gstrp))
							},
							{
								"id": "3",
								"lane": "2",
								"title": "Reserva\n" + result.results[0].rsnum,
								"titleAbbreviation": result.results[0].rsnum,
								"children": [4],
								"state": result.results[0].estadoRsnum,
								"stateText": "Listo",
								"focused": false,
								"texts": "Doc. Financiero: " + result.results[0].to_DocFin.reserva + "\nFecha: " + thes.dateToFecha(thes.sumaFecha(1, result.results[0].to_DocFin.fechares))
							},
							{
								"id": "4",
								"lane": "3",
								"title": "Doc. Compra\n" + documentoCompra,
								"titleAbbreviation": documentoCompra,
								"children": [5],
								"state": result.results[0].estadoDocComp,
								"stateText": "Listo",
								"focused": false,
								"texts": ""
							},
							{
								"id": "5",
								"lane": "4",
								"title": "HES\n" + hes,
								"titleAbbreviation": hes,
								"children": [6],
								"state": result.results[0].estadoHojaRuta,
								"stateText": "Listo",
								"focused": false,
								"texts": ""
							},
							{
								"id": "6",
								"lane": "5",
								"title": "Pedido\n" + result.results[0].Ped_Venta,
								"titleAbbreviation": result.results[0].Ped_Venta,
								"children": [7],
								"state": result.results[0].estadoPed_Venta,
								"stateText": "Listo",
								"focused": false,
								"texts": "Fecha: " + thes.dateToFecha(thes.sumaFecha(1, result.results[0].audat))
							},
							{
								"id": "7",
								"lane": "6",
								"title": "Factura\n" + result.results[0].vbeln,
								"titleAbbreviation": result.results[0].vbeln,
								"children": null,
								"state": result.results[0].estadoVbeln,
								"stateText": "Listo",
								"focused": false,
								"texts": "Fecha: " + thes.dateToFecha(thes.sumaFecha(1, result.results[0].fkdat)) + "\nDoc Financiero: " + result.results[0].to_DocFin.reserva + "\nFecha: " + thes.dateToFecha(thes.sumaFecha(1, result.results[0].to_DocFin.fechares))
							}
						],
						"lanes": [
							{
								"id": "0",
								"icon": "sap-icon://timesheet",
								"label": "No. de Servicio",
								"position": 0
							},
							{
								"id": "1",
								"icon": "sap-icon://order-status",
								"label": "Orden",
								"position": 1
							},
							{
								"id": "2",
								"icon": "sap-icon://pushpin-on",
								"label": "Reserva",
								"position": 2
							},
							{
								"id": "3",
								"icon": "sap-icon://sales-order",
								"label": "Doc. Compra",
								"position": 3
							},
							{
								"id": "4",
								"icon": "sap-icon://accounting-document-verification",
								"label": "HES",
								"position": 4
							}
							,
							{
								"id": "5",
								"icon": "sap-icon://order-status",
								"label": "Pedido",
								"position": 5
							}
							,
							{
								"id": "6",
								"icon": "sap-icon://order-status",
								"label": "Factura",
								"position": 6
							}
						]
					};

					var jsonModel = new sap.ui.model.json.JSONModel(node);
					thes.getView().setModel(jsonModel, "flujoDocumentos");
					thes.byId("opsFlujoDocumentos").setBusy(false);
				},
				error: function (err) {

				}
			});

		},		

		obtenerIdDocumentos: function (TipoDocumento) {

			var filters = [];

			var filter = new sap.ui.model.Filter("Aufnr", sap.ui.model.FilterOperator.EQ, Aufnr);
			filters.push(filter);
			var filter = new sap.ui.model.Filter("TipoDoc", sap.ui.model.FilterOperator.EQ, TipoDocumento);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/ZCDS_VW_BASRRC001_217", {
				filters: filters,
				success: function (result) {
					switch (TipoDocumento) {
						case "ORD":
							var jsonModel = new sap.ui.model.json.JSONModel(result.results);
							thes.getView().setModel(jsonModel, "documentosAdjuntos");
							sap.ui.core.BusyIndicator.hide();
							break;
						case "INV":

							if (result.results.length > 0) {
								for (var i = 0; i < result.results.length; i++) {

									var url = "https://wa.me/" + thes.cabecera.telephone + "?text=Buen día, Sr(a). " + thes.cabecera.nombre + "\n ponemos a su disposición la documentación con la operación de su vehículo de la orden " + Aufnr + ", Inventario: " + result.results[i].URL;

									window.open(url, "_blank");
								}
							} else {
								sap.m.MessageBox.warning("Sin datos disponible");
							}

							break;
					}
					;
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onCargarArchivo: function () {
			//thes.byId("fileUploader").
			if (thes.byId("fileUploader").oFileUpload.files[0]) {
				sap.ui.core.BusyIndicator.show();
				thes.CargarAdjunto(thes.byId("fileUploader").oFileUpload.files[0], "ORD");
				thes.byId("fileUploader").oFileUpload.files[0];
				thes.byId("fileUploader").setValue("");
			}

		},

		CargarAdjunto: function (archivo, TipoDocumento) {
			var url = "/aws-s3-dest/media/Pictures";
			var idDocumentos = "";
			$.ajax({
				url: url,
				type: "POST",
				async: false,
				data: JSON.stringify({}),
				contentType: "application/json",
				success: function (result) {
					idDocumentos = result.ID;
					var url2 = url + "(" + result.ID + ")/content";
					$.ajax({
						url: url2,
						type: 'PUT',
						async: false,
						contentType: archivo.type,
						processData: false,
						data: archivo,
						success: function (result2) {

							thes.grabarDocumentosAdjuntos(idDocumentos, archivo.name, TipoDocumento);

						},
						error: function (request, msg, error) {

						}
					});

				},
				error: function (request, msg, error) {

				}
			});

		},

		grabarDocumentosAdjuntos: function (id, nombre, TipoDocumento) {
			var json = {
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": Aufnr,
				"DocumentsSet": [
					{
						"Aufnr": Aufnr,
						"Archivo": id,
						"Descrip": nombre,
						"IndDelete": "",
						"TipoDoc": TipoDocumento
					}
				]
			};

			Z_OD_SCP_BASRRC001_ORD_SRV.create("/SaveDocumentSet", json, {
				success: function (result, status) {
					thes.obtenerIdDocumentos(TipoDocumento);
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.m.MessageBox.error("Error al guardar documento en SAP");
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onAbrirDocumentoAdjunto: function (oEvent) {
			var url = "/aws-s3-dest/media/Pictures(" + oEvent.getSource().getProperty("target") + ")/content";
			window.open(url, "_blank");
		},

		onDocumentosAdjuntos: function () {

			var oView = this.getView();

			if (!this.byId("dlgDocumentosAdjuntos")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					controller: this,
					name: "dms4100ui.bacrcr001ord.view.DocumentosAdjuntos"
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgDocumentosAdjuntos").open();
			}

			thes.obtenerIdDocumentos("ORD");

		},

		onExpediente: function () {

			var oView = this.getView();

			if (!this.byId("dlgExpediente")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					controller: this,
					name: "dms4100ui.bacrcr001ord.view.Expediente"
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgExpediente").open();
			}

		},

		onModificarPersonaContacto: function (oEvent) {

			var path = oEvent.getSource().getParent().getParent().getBindingContext("personasContactos").getPath();
			var reg = this.getView().getModel("personasContactos").getProperty(path);

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgEditarSocioComercial")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					controller: this,
					name: "dms4100ui.bacrcr001ord.view.EditarSocioComercial"
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgEditarSocioComercial").open();
			}

		},

		onPresContactos: function (oEvent) {
			var oView = this.getView();
			var oButton = oEvent.getSource();
			var path = oEvent.getSource().getParent().getBindingContext("personasContactos").getPath();
			var reg = oEvent.getSource().getParent().getModel("personasContactos").getProperty(path);

			var json = {
				"pages": [
					{
						"title": reg.Nomb + " " + reg.Apell,
						"icon": "sap-icon://person-placeholder",
						"groups": [
							{
								"elements": [
									{
										"label": "Telefono",
										"value": reg.Telf,
										"elementType": sap.m.QuickViewGroupElementType.phone
									},
									{
										"label": "Correo",
										"value": reg.email,
										"elementType": sap.m.QuickViewGroupElementType.email,
										"emailSubject": "Estimado " + reg.Nomb + " " + reg.Apell + ","
									}
								]
							}
						]
					}
				]
			};

			var jsonModel = new sap.ui.model.json.JSONModel(json);

			if (!this._oQuickView) {
				Fragment.load({
					id: oView.getId(),
					controller: this,
					name: "dms4100ui.bacrcr001ord.view.QuickView"
				}).then(function (oQuickView) {
					this._oQuickView = oQuickView;
					this.getView().addDependent(this._oQuickView);
					this._oQuickView.close();
					this._oQuickView.openBy(oButton);
					this._oQuickView.setModel(jsonModel);
				}.bind(this));
			} else {
				this.getView().addDependent(this._oQuickView);
				this._oQuickView.close();
				this._oQuickView.setModel(jsonModel);
				this._oQuickView.openBy(oButton);
			}

		},

		onComentarioOrdenServicio: function () {
			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgComentarioOrdenServicio")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					controller: this,
					name: "dms4100ui.bacrcr001ord.view.ComentarioOrdenServicio"
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();

					if (thes.byId("btnGuardarComentarioOrdenServicio")) {

						if (thes.cabecera.estatus === "C") {
							thes.byId("btnGuardarComentarioOrdenServicio").setVisible(false);
						} else {
							thes.byId("btnGuardarComentarioOrdenServicio").setVisible(true);
						}

						if (thes.cabecera.StatXentry === "S" && thes.cabecera.estatus !== "C") {
							thes.byId("btnGuardarComentarioOrdenServicio").setVisible(false);
						}

					}

				});
			} else {
				this.byId("dlgComentarioOrdenServicio").open();

				if (thes.byId("btnGuardarComentarioOrdenServicio")) {

					if (thes.cabecera.estatus === "C") {
						thes.byId("btnGuardarComentarioOrdenServicio").setVisible(false);
					} else {
						thes.byId("btnGuardarComentarioOrdenServicio").setVisible(true);
					}

					if (thes.cabecera.StatXentry === "S" && thes.cabecera.estatus !== "C") {
						thes.byId("btnGuardarComentarioOrdenServicio").setVisible(false);
					}

				}

			}

			thes.obtenerComentarioOrdenServicio();
		},

		obtenerComentarioOrdenServicio: function () {

			var filters = [];

			var filter = new sap.ui.model.Filter("Orden", sap.ui.model.FilterOperator.EQ, Aufnr);
			filters.push(filter);
			var filter = new sap.ui.model.Filter("IdentText", sap.ui.model.FilterOperator.EQ, "O");
			filters.push(filter);

			this.obtenerComentario(filters, "modeloComentarioOrdenServicio");

		},

		obtenerComentario: function (filters, id) {

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_SRV.read("/GetTextOSSet", {
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results[0]);
					thes.getView().setModel(jsonModel, id);
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onDialogoComentarioIncidenteCliente: function () {

			var indices = this.byId("smartTableIncidentes").getTable().getSelectedIndices();

			if (indices.length > 1) {
				sap.m.MessageBox.error("Seleccione solo un registro");
				return;
			}

			if (indices.length === 0) {
				sap.m.MessageBox.error("Seleccione un registro");
				return;
			}

			var reg = this.byId("smartTableIncidentes").getTable().getContextByIndex(indices[0]).getProperty();

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgComentarioIncidenteCliente")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.ComentarioIncidentesCliente",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();

					if (thes.byId("textAreaComentarioIncidenteCliente")) {
						thes.byId("textAreaComentarioIncidenteCliente").setValue("");
					}
					if (thes.byId("textAreaComentarioIncidenteCausa")) {
						thes.byId("textAreaComentarioIncidenteCausa").setValue("");
					}

					if (thes.byId("btnGuardarComentarioIncidenteCliente")) {

						if (thes.cabecera.estatus === "C") {
							thes.byId("btnGuardarComentarioIncidenteCliente").setVisible(false);
						} else {
							thes.byId("btnGuardarComentarioIncidenteCliente").setVisible(true);
						}

						if (thes.cabecera.StatXentry === "S" && thes.cabecera.estatus !== "C") {
							thes.byId("btnGuardarComentarioIncidenteCliente").setVisible(false);
						}

					}

				});
			} else {
				this.byId("dlgComentarioIncidenteCliente").open();

				if (thes.byId("textAreaComentarioIncidenteCliente")) {
					thes.byId("textAreaComentarioIncidenteCliente").setValue("");
				}
				if (thes.byId("textAreaComentarioIncidenteCausa")) {
					thes.byId("textAreaComentarioIncidenteCausa").setValue("");
				}

				if (thes.byId("btnGuardarComentarioIncidenteCliente")) {

					if (thes.cabecera.estatus === "C") {
						thes.byId("btnGuardarComentarioIncidenteCliente").setVisible(false);
					} else {
						thes.byId("btnGuardarComentarioIncidenteCliente").setVisible(true);
					}

					if (thes.cabecera.StatXentry === "S" && thes.cabecera.estatus !== "C") {
						thes.byId("btnGuardarComentarioIncidenteCliente").setVisible(false);
					}

				}

			}

			sap.ui.core.BusyIndicator.show(4);

			this.obtenerComentarioIncidenteCliente(reg);
			this.obtenerComentarioIncidenteCausa(reg);
		},

		obtenerComentarioIncidenteCausa: function (reg) {

			var filters = [];

			var filter = new sap.ui.model.Filter("Orden", sap.ui.model.FilterOperator.EQ, Aufnr);
			filters.push(filter);
			var filter = new sap.ui.model.Filter("Posicion", sap.ui.model.FilterOperator.EQ, reg.vornr_real);
			filters.push(filter);
			var filter = new sap.ui.model.Filter("IdentText", sap.ui.model.FilterOperator.EQ, "CA");
			filters.push(filter);

			this.obtenerComentario(filters, "modeloComentarioIncidenteCausa");

		},

		obtenerComentarioIncidenteCliente: function (reg) {

			var filters = [];

			var filter = new sap.ui.model.Filter("Orden", sap.ui.model.FilterOperator.EQ, Aufnr);
			filters.push(filter);
			var filter = new sap.ui.model.Filter("Posicion", sap.ui.model.FilterOperator.EQ, reg.vornr_real);
			filters.push(filter);
			var filter = new sap.ui.model.Filter("IdentText", sap.ui.model.FilterOperator.EQ, "IC");
			filters.push(filter);

			this.obtenerComentario(filters, "modeloComentarioIncidenteCliente");

		},

		onDialogoComentarioIncidente: function (oEvent) {

			var indices = this.byId("smartTableIncidentes").getTable().getSelectedIndices();

			if (indices.length > 1) {
				sap.m.MessageBox.error("Seleccione solo un registro");
				return;
			}

			if (indices.length === 0) {
				sap.m.MessageBox.error("Seleccione un registro");
				return;
			}

			var reg = this.byId("smartTableIncidentes").getTable().getContextByIndex(indices[0]).getProperty();

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgComentarioIncidente")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.ComentarioIncidentes",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();

					if (thes.byId("textAreaComentarioIncidente")) {
						thes.byId("textAreaComentarioIncidente").setValue("");
					}

					if (thes.byId("btnGuardarComentarioIncidente")) {

						if (thes.cabecera.estatus === "C") {
							thes.byId("btnGuardarComentarioIncidente").setVisible(false);
						} else {
							thes.byId("btnGuardarComentarioIncidente").setVisible(true);
						}

						if (thes.cabecera.StatXentry === "S" && thes.cabecera.estatus !== "C") {
							thes.byId("btnGuardarComentarioIncidente").setVisible(false);
						}

					}

				});
			} else {
				this.byId("dlgComentarioIncidente").open();

				if (thes.byId("textAreaComentarioIncidente")) {
					thes.byId("textAreaComentarioIncidente").setValue("");
				}

				if (thes.byId("btnGuardarComentarioIncidente")) {

					if (thes.cabecera.estatus === "C") {
						thes.byId("btnGuardarComentarioIncidente").setVisible(false);
					} else {
						thes.byId("btnGuardarComentarioIncidente").setVisible(true);
					}

					if (thes.cabecera.StatXentry === "S" && thes.cabecera.estatus !== "C") {
						thes.byId("btnGuardarComentarioIncidente").setVisible(false);
					}

				}

			}

			sap.ui.core.BusyIndicator.show(4);

			this.obtenerComentarioIncidente(reg);

		},

		obtenerComentarioIncidente: function (reg) {
			var filters = [];

			var filter = new sap.ui.model.Filter("Orden", sap.ui.model.FilterOperator.EQ, Aufnr);
			filters.push(filter);
			var filter = new sap.ui.model.Filter("Posicion", sap.ui.model.FilterOperator.EQ, reg.vornr_real);
			filters.push(filter);
			var filter = new sap.ui.model.Filter("IdentText", sap.ui.model.FilterOperator.EQ, "I");
			filters.push(filter);

			if (thes.byId("textAreaComentarioIncidente")) {
				thes.byId("textAreaComentarioIncidente").setValue("");
			}

			this.obtenerComentario(filters, "modeloComentarioIncidente");
		},

		onGuardarComentarioOrdenServicio: function () {

			var json = {
				"Orden": Aufnr,
				"Texto": this.byId("textAreaComentarioOrdenServicio").getValue(),
				"Identext": "O"
			};

			this.onGuardarComentarios(json, "dlgComentarioOrdenServicio");

		},

		onGuardarComentarioIncidenteCliente: function () {
			sap.ui.core.BusyIndicator.show();

			var indices = this.byId("smartTableIncidentes").getTable().getSelectedIndices();
			var reg = this.byId("smartTableIncidentes").getTable().getContextByIndex(indices[0]).getProperty();

			var json1 = {
				"Orden": Aufnr,
				"Posicion": reg.vornr_real,
				"Texto": this.byId("textAreaComentarioIncidenteCliente").getValue(),
				"Identext": "IC"
			};

			var json2 = {
				"Orden": Aufnr,
				"Posicion": reg.vornr_real,
				"Texto": this.byId("textAreaComentarioIncidenteCausa").getValue(),
				"Identext": "CA"
			};

			this.onGuardarComentarios2(json1, json2, "dlgComentarioIncidenteCliente");
		},

		onGuardarComentarioIncidente: function () {

			var indices = this.byId("smartTableIncidentes").getTable().getSelectedIndices();
			var reg = this.byId("smartTableIncidentes").getTable().getContextByIndex(indices[0]).getProperty();

			var json = {
				"Orden": Aufnr,
				"Posicion": reg.vornr_real,
				"Texto": this.byId("textAreaComentarioIncidente").getValue(),
				"Identext": "I"
			};

			this.onGuardarComentarios(json, "dlgComentarioIncidente");
		},

		onGuardarComentarios: function (json, id) {

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_SRV.create("/SaveTextOSSet", json, {
				success: function (result, status) {

					if (id) {
						thes.byId(id).close();
					}

					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.m.MessageBox.error("Error al guardar comentario");
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onGuardarComentarios2: function (json1, json2, id) {

			Z_OD_SCP_BASRRC001_SRV.create("/SaveTextOSSet", json1, {
				success: function (result, status) {
					thes.onGuardarComentarios(json2, id);
				},
				error: function (err) {
					sap.m.MessageBox.error("Error al guardar comentario");
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onDialogoSalesforceEdit: function () {

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgEditarSalesforce")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.EditarSalesforce",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgEditarSalesforce").open();
			}

			var inmovil;
			if (thes.cabecera.inmovil === "") {
				inmovil = false;
			} else {
				inmovil = true;
			}

			var feinmovil = "";
			if (thes.cabecera.feinmovil) {
				feinmovil = this.toDateMoment(thes.cabecera.feinmovil);
			}

			var femovil = "";
			if (thes.cabecera.femovil) {
				femovil = this.toDateMoment(thes.cabecera.femovil);
			}

			var salesforce = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": Aufnr,
				"Inmovil": inmovil,
				"Codtipord": thes.cabecera.codtipord,
				"Codmotivo": thes.cabecera.codmotivo,
				"Comentario": thes.cabecera.comentario,
				"Feinmovil": feinmovil,
				"Femovil": femovil,
				"Norescate": thes.cabecera.norescate,
				"Nogarant": thes.cabecera.nogarant,
				"Queja": thes.cabecera.queja
			};

			var jsonModel = new sap.ui.model.json.JSONModel(salesforce);
			thes.getView().setModel(jsonModel, "EditSalesforce");

			this.obtenerTipoOrden();
			//this.onCheckInmovilizacion();

			if (!inmovil) {
				var habilitacion = { enabled: false }
				var jsonModel = new sap.ui.model.json.JSONModel(habilitacion);
				thes.getView().setModel(jsonModel, "habilitacion");
			}

			thes.obtenerMotivo();
		},

		obtenerTipoOrden: function () {

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_SALESFORCE_SRV.read("/ZCDS_VW_SF_TIPORD", {
				success: function (result) {
					var jsonModel2 = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel2, "tipoOrden");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onChangeTipoOrden: function (oEvent) {
			var key = oEvent.getSource().getSelectedKey();
			this.obtenerTipoOperacion(key);
		},

		obtenerPersonaContacto: function () {

			var filters = [];

			var filter = new sap.ui.model.Filter("Partner", sap.ui.model.FilterOperator.EQ, Kunum);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_SRV.read("/ContactoInterlocutorSet", {
				filters: filters,
				success: function (result) {
					var jsonModel2 = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel2, "PersonaContacto");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerMedioContacto: function () {

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_SRV.read("/ZCDS_VW_BASRRC001_115", {
				success: function (result) {
					var jsonModel2 = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel2, "MedioContacto");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerPaises: function () {
			sap.ui.core.BusyIndicator.show(4);

			var filters = [];

			var filter = new sap.ui.model.Filter("Spras", sap.ui.model.FilterOperator.EQ, "ES");
			filters.push(filter);

			Z_OD_SCP_CORE_0001_SRV.read("/PaiseSet", {
				filters: filters,
				success: function (result) {
					var jsonModel2 = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel2, "Paises");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});
		},

		obtenerRegiones: function (Land1) {
			var filters = [];

			var filter = new sap.ui.model.Filter("Spras", sap.ui.model.FilterOperator.EQ, "ES");
			filters.push(filter);

			var filter = new sap.ui.model.Filter("Land1", sap.ui.model.FilterOperator.EQ, Land1);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_SRV.read("/RegionSet", {
				filters: filters,
				success: function (result) {
					var jsonModel2 = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel2, "Regiones");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});
		},

		obtenerTipoOperacion: function (tipoOrden) {

			var filters = [];

			var filter = new sap.ui.model.Filter("codtipord", sap.ui.model.FilterOperator.EQ, tipoOrden);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_SALESFORCE_SRV.read("/ZCDS_VW_SF_TIPOPE", {
				filters: filters,
				success: function (result) {
					var jsonModel2 = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel2, "tipoOperacion");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});
		},

		onDialogoDescuentosEdit: function () {

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgEditarDescuentos")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.EditarDescuentos",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgEditarDescuentos").open();
			}

			//thes.obtenerIncidentes();

			var descuentos = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": Aufnr,
				"Taller": thes.cabecera.DesTaller,
				"Material": thes.cabecera.DesMaterial
			};

			var jsonModel = new sap.ui.model.json.JSONModel(descuentos);
			thes.getView().setModel(jsonModel, "EditDescuentos");

		},

		onEditarDescuentos: function () {

			var reg = thes.getView().getModel("EditDescuentos").getData();

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenDescuentoSet", reg, {
				success: function (result, status) {
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					thes.obtenerCabeceraOrden();
					sap.ui.core.BusyIndicator.hide();
					thes.byId("dlgEditarDescuentos").close();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
					thes.byId("dlgEditarDescuentos").close();
				}
			});

		},

		obtenerEvidenciaFotografica: function () {

			var filters = [];
			var filter = new sap.ui.model.Filter("cod_cita", "EQ", thes.cabecera.cod_cita);
			filters.push(filter);

			ZCDS_VW_BASRRC001_216_CDS.read("/ZCDS_VW_BASRRC001_216", {
				filters: filters,
				success: function (result) {

					if (result.results.length > 0) {
						for (var i = 0; i < result.results.length; i++) {
							window.open(result.results[i].URL, "_blank");
						}
					} else {
						sap.m.MessageBox.warning("Sin datos disponible");
					}

				},
				error: function (err) {

				}
			});


		},

		obtenerInventario: function () {

			var _oModelTicketHeaders = {
				"cita": thes.cabecera.cod_cita,
			};

			Z_OD_SCP_BASRRC001_SRV.read("/ImpresionSet ", {
				headers: _oModelTicketHeaders,
				success: function (result) {

					if (result.results.length > 0) {
						for (var i = 0; i < result.results.length; i++) {
							thes.onViewPDF2(result.results[0].PDF, "SmartForms");
						}
					} else {

						sap.m.MessageBox.warning("Sin datos disponible");

					}

				},
				error: function (err) {

				}
			});

		},

		enviarExpedienteCorreo: function () {

			var inventario = thes.byId("ckbInventario").getSelected();
			if (inventario) {
				inventario = "X";
			} else {
				inventario = "";
			}
			var EvidenciaFotografica = thes.byId("ckbEvidenciaFotografica").getSelected();
			if (EvidenciaFotografica) {
				EvidenciaFotografica = "X";
			} else {
				EvidenciaFotografica = "";
			}

			var reg = {
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": Aufnr,
				"FlgInv": inventario,
				"FlgOrd": "",
				"FlgEfo": EvidenciaFotografica,
				"FlgHmu": "",
				"FlgOas": "",
				"FlgVcl": ""
			};

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/SendEmailSet", reg, {
				success: function (result, status) {
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					thes.obtenerCabeceraOrden();
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		convertirBase64toBlob: function (archivo, tipo) {
			const byteCharacters = atob(archivo);
			const byteNumbers = new Array(byteCharacters.length);
			for (let i = 0; i < byteCharacters.length; i++) {
				byteNumbers[i] = byteCharacters.charCodeAt(i);
			}
			const byteArray = new Uint8Array(byteNumbers);
			const blob = new Blob([byteArray], { type: tipo });
			return blob;
		},

		obtenerDetalleEjecucion: function () {

			var filters = [];

			var filter = new sap.ui.model.Filter("Orden", sap.ui.model.FilterOperator.EQ, Aufnr);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRTM001_CHIPS_SRV.read("/ZCDS_VW_BASRTM001_003", {
				filters: filters,
				success: function (result) {
					var jsonModel2 = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel2, "detalleEjecucion");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onDetalleEjecucion: function () {

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgDetalleEjecucionOrden")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					controller: this,
					name: "dms4100ui.bacrcr001ord.view.DetalleEjecucionOrden"
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgDetalleEjecucionOrden").open();
			}

			thes.obtenerDetalleEjecucion();

		},

		onEnviarMensajeWhatsapp: function () {

			var reg = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": Aufnr,
				"Tipom": "FIN",
				"Message": ""
			};

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenWhatAppMensajeSet", reg, {
				success: function (result, status) {
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}

					if (result.Message) {
						var url = "https://wa.me/" + thes.cabecera.telephone + "?text=Buen día, Sr(a). " + thes.cabecera.nombre + "\n" + result.Message;
						window.open(url, "_blank");
					}

					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		enviarWhatssap: function () {
			var inventario = thes.byId("ckbInventario").getSelected();
			if (inventario) {

				//sap.m.MessageBox.warning("No se puede enviar por whatssap el Inventario");

				var filters = [];

				var filter = new sap.ui.model.Filter("Aufnr", sap.ui.model.FilterOperator.EQ, Aufnr);
				filters.push(filter);
				var filter = new sap.ui.model.Filter("TipoDoc", sap.ui.model.FilterOperator.EQ, "INV");
				filters.push(filter);

				sap.ui.core.BusyIndicator.show();
				Z_OD_SCP_BASRRC001_ORD_SRV.read("/ZCDS_VW_BASRRC001_217", {
					filters: filters,
					success: function (result) {
						if (result.results.length > 0) {
							for (var i = 0; i < result.results.length; i++) {
								var url = "https://wa.me/" + thes.cabecera.telephone + "?text=Buen día, Sr(a). " + thes.cabecera.nombre + "\n ponemos a su disposición la documentación con la operación de su vehículo de la orden " + Aufnr + ", Inventario: " + result.results[i].URL;
								window.open(url, "_blank");
							}
							sap.ui.core.BusyIndicator.hide();

						} else {
							var _oModelTicketHeaders = {
								"cita": thes.cabecera.cod_cita
							};

							Z_OD_SCP_BASRRC001_SRV.read("/ImpresionSet ", {
								headers: _oModelTicketHeaders,
								success: function (result) {

									if (result.results.length > 0) {
										for (var i = 0; i < result.results.length; i++) {
											var blob = thes.convertirBase64toBlob(thes.hexToBase64(result.results[0].PDF), "application/pdf");
											thes.CargarAdjunto(blob, "INV");
										}
									} else {

										sap.m.MessageBox.warning("Sin datos disponible");

									}

								},
								error: function (err) {

								}
							});
						}
					},
					error: function (err) {
						sap.ui.core.BusyIndicator.hide();
					}
				});

			}
			var EvidenciaFotografica = thes.byId("ckbEvidenciaFotografica").getSelected();
			if (EvidenciaFotografica) {

				var filters = [];
				var filter = new sap.ui.model.Filter("cod_cita", "EQ", thes.cabecera.cod_cita);
				filters.push(filter);

				ZCDS_VW_BASRRC001_216_CDS.read("/ZCDS_VW_BASRRC001_216", {
					filters: filters,
					success: function (result) {

						if (result.results.length > 0) {
							for (var i = 0; i < result.results.length; i++) {

								var url = "https://wa.me/" + thes.cabecera.telephone + "?text=Buen día, Sr(a). " + thes.cabecera.nombre + "\n ponemos a su disposición la documentación con la operación de su vehículo de la orden " + Aufnr + ", Evidencia Fotrográfica: " + result.results[i].URL;

								window.open(url, "_blank");
							}
						} else {
							sap.m.MessageBox.warning("Sin datos disponible");
						}

					},
					error: function (err) {

					}
				});

			}
		},

		onChgAdendaCliente: function () {

			var regSel = thes.getView().getModel("adendaCliente").getData().filter(function buscarAdenda(reg) {
				return reg.codetiq === thes.byId("cbAdendaCliente").getSelectedKey();
			});

			thes.obtenerCamposOpcionCliente(regSel[0].CodOp);
		},

		obtenerMotivo: function () {

			Z_OD_SCP_BASRRC001_ORD_SRV.read("/OrdenMotivos", {
				success: function (result) {
					var jsonModel2 = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel2, "motivo");
				},
				error: function (err) {

				}
			});

		},

		obtenerCamposOpcionCliente: function (CodOp) {

			if (!CodOp) {
				thes.byId("adendasCabecera").destroyContent();
				var jsonModel = new sap.ui.model.json.JSONModel([]);
				thes.getView().setModel(jsonModel, "adendaDetalle");
			}

			var filters = [];
			var filter = new sap.ui.model.Filter("codcli", "EQ", Kunum);
			filters.push(filter);

			var filter = new sap.ui.model.Filter("modsrv", "EQ", "X");
			filters.push(filter);

			var filter = new sap.ui.model.Filter("contop", "EQ", CodOp);
			filters.push(filter);

			Z_OD_SCP_BAVNVT001_SRV.read("/ZCDS_VW_SD30", {
				filters: filters,
				urlParameters: {
					"$expand": "to_ValCmp"
				},
				success: function (result) {

					var cabecera = [];
					var posicion = [];
					var posicion2 = [];
					var reg1 = {};
					var reg2 = {};

					thes.byId("adendasCabecera").destroyContent();

					for (var i = 0; i < result.results.length; i++) {
						if (result.results[i].indcabpos === "C") {
							cabecera.push(result.results[i]);

							var label = new sap.m.Label({
								text: result.results[i].etiqueta
							});

							if (result.results[i].to_ValCmp.valdesc) {
								var input = new sap.m.Input("adendaCabecera" + i, {
									value: result.results[i].valor,
									customData: {
										Type: "sap.ui.core.CustomData",
										key: "codetiq",
										value: result.results[i].codetiq
									}
								});
							} else {
								var input = new sap.m.Input("adendaCabecera" + i, {
									value: "",
									customData: {
										Type: "sap.ui.core.CustomData",
										key: "codetiq",
										value: result.results[i].codetiq
									}
								});
							}

							thes.byId("adendasCabecera").addContent(label);
							thes.byId("adendasCabecera").addContent(input);


						}

						if (result.results[i].indcabpos === "P") {
							if (result.results[i].to_ValCmp.results.length > 0) {
								result.results[i].combo = true;
							} else {
								result.results[i].combo = false;
							}
							let reg1 = result.results[i];
							let reg2 = JSON.parse(JSON.stringify(reg1));

							posicion.push(reg1);
							posicion2.push(reg2);
						}
					}

					var jsonModel = new sap.ui.model.json.JSONModel(posicion);
					thes.getView().setModel(jsonModel, "adendaDetalleIni");
					var jsonModel2 = new sap.ui.model.json.JSONModel(posicion2);
					thes.getView().setModel(jsonModel2, "adendaDetalle");
					var filters = [];
					var filter = new sap.ui.model.Filter("CodCli", "EQ", Kunum);
					filters.push(filter);

					var filters = [];
					var filter = new sap.ui.model.Filter("NoPed", "EQ", thes.cabecera.eqfnr);
					filters.push(filter);
					thes.obtenerValoresAdenda(filters);
				},
				error: function (err) {

				}
			});

		},

		obtenerValoresAdenda: function (filters) {

			Z_OD_SCP_BAVNVT001_SRV.read("/ZCDS_VW_SD38", {
				filters: filters,
				success: function (result) {

					if (result.results.length === 0) {
						return;
					}

					var adendasCabecera = thes.byId("adendasCabecera").getContent();
					for (var a = 0; a < adendasCabecera.length; a++) {
						var control = thes.byId("adendasCabecera").getContent()[a].getMetadata().getElementName();
						if (control === "sap.m.Input") {
							var regCab = result.results.find(element => element.CodEtCampo === adendasCabecera[a].getCustomData()[0].getValue() && element.indcabpos === "C");
							thes.byId("adendasCabecera").getContent()[a].setValue(regCab.valdesc);
						}
					}


					var adendaDetalle = thes.getView().getModel("adendaDetalle").getData();
					var adendaDetalle2 = [];

					if (adendaDetalle.length === 0) {
						var posiciones = result.results.length
					} else {
						var posiciones = result.results.length / adendaDetalle.length;
					}

					var catnPos = result.results.length / posiciones;
					var posActual = 0;
					var pos = 2;
					for (var o = 0; o < posiciones; o++) {
						for (var e = 0; e < adendaDetalle.length; e++) {
							adendaDetalle2.push(JSON.parse(JSON.stringify(adendaDetalle[e])));
						}
					}

					adendaDetalle = adendaDetalle2;

					// for (var u = 0; u < adendaDetalle2.length; u++) {
					// 	adendaDetalle.push(JSON.parse(JSON.stringify(adendaDetalle2[u])));
					// }

					for (var i = 0; i < adendaDetalle.length; i++) {
						posActual = posActual + 1;

						//pos = pos + i;
						var pos2 = thes.zfill(pos, 6);
						// var pos2 = thes.zfill(2, 6);
						var reg = result.results.find(element => element.CodEtCampo === adendaDetalle[i].codetiq && element.posnr === pos2);

						if (reg) {
							adendaDetalle[i].valor = reg.valdesc;
							adendaDetalle[i].posnr = reg.posnr;
						}

						if (posActual === catnPos) {
							posActual = 0;
							pos = pos + 1;
						}

					}
					var jsonModel = new sap.ui.model.json.JSONModel(adendaDetalle);
					thes.getView().setModel(jsonModel, "adendaDetalle");
					thes.getView().getModel("adendaDetalle").refresh();
				},
				error: function (err) {

				}
			});

		},

		obtenerTecnicoDefault: function () {

			var filters = [];
			var filter = new sap.ui.model.Filter("Werks", "EQ", Werks);
			filters.push(filter);
			var filter = new sap.ui.model.Filter("Tipo", "EQ", "T");
			filters.push(filter);
			var filter = new sap.ui.model.Filter("Expres", "EQ", "");
			filters.push(filter);
			var filter = new sap.ui.model.Filter("Pdefault", "EQ", "X");
			filters.push(filter);

			Z_OD_SCP_BASRRC001_SRV.read("/CitaPuestosSet", {
				filters: filters,
				success: function (result) {
					if (result.results.length > 0) {
						Objid = result.results[0].Objid;
					}
				},
				error: function (err) {

				}
			});

		},

		obtenerTecnico: function () {

			var filters = [];
			var filter = new sap.ui.model.Filter("Werks", "EQ", Werks);
			filters.push(filter);
			var filter = new sap.ui.model.Filter("Tipo", "EQ", "T");
			filters.push(filter);
			var filter = new sap.ui.model.Filter("Expres", "EQ", "");
			filters.push(filter);
			var filter = new sap.ui.model.Filter("Principal", "EQ", "X");
			filters.push(filter);

			Z_OD_SCP_BASRRC001_SRV.read("/CitaPuestosSet", {
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "tecnico");
				},
				error: function (err) {

				}
			});

		},

		obtenerAdendaCliente: function () {

			var filters = [];
			var filter = new sap.ui.model.Filter("CodCli", "EQ", Kunum);
			filters.push(filter);

			var filter = new sap.ui.model.Filter("modsrv", "EQ", "X");
			filters.push(filter);

			Z_OD_SCP_BAVNVT001_SRV.read("/ZCDS_VW_SD29", {
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "adendaCliente");
				},
				error: function (err) {

				}
			});

		},		

		obtenerAnticipos: function () {

			var filters = [];
			var filter = new sap.ui.model.Filter("NoPedido", "EQ", Eqfnr);
			filters.push(filter);

			Z_OD_SCP_BAVNVT001_SRV.read("/ZCDS_VW_SD13", {
				filters: filters,
				success: function (result) {

					for (var i = 0; i < result.results.length; i++) {
						if (result.results[i].Anulado === "X") {
							result.results[i].EstadoAnulado = "Error";
						} else {
							result.results[i].EstadoAnulado = "Success";
						}
					}

					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "anticipos");
				},
				error: function (err) {

				}
			});

		},		

		onDialogoCabeceraEdit: function () {

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgEditarCabecera")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.EditarCabecera",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgEditarCabecera").open();
			}

			//thes.obtenerIncidentes();
			var FechaPromesa = "";
			var HoraPromesa = "";
			//var FechaHoraPromesa = "";
			if (thes.cabecera.FechaHoraPromesa.length == 14) {
				//FechaHoraPromesa = thes.stringTodateTime(thes.cabecera.FechaHoraPromesa); 
				FechaPromesa = thes.cabecera.FechaHoraPromesa.substring(0, 8);
				FechaPromesa = thes.stringTodate(FechaPromesa);
				HoraPromesa = thes.cabecera.FechaHoraPromesa.substring(8, 15);
			}

			if (FechaPromesa === undefined || FechaPromesa === "") {
				FechaPromesa = new Date();
			}

			var cabecera = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": Aufnr,
				"Descripcion": thes.cabecera.ktext,
				"Beber": thes.cabecera.beber,
				"UserStatInt": thes.cabecera.user_stat_int,
				"Pagador": thes.cabecera.pagador,
				"Siniestro": thes.cabecera.siniestro,
				"FechaPromesa": FechaPromesa,
				"FechaHora": HoraPromesa,
				"Stand": thes.cabecera.stort,
				"Kilometraje": thes.cabecera.kilometraje,
				"Gasolina": thes.cabecera.gasolina,
				"Idcono": thes.cabecera.idcono,
				"FechaPromesa2": FechaPromesa,
				"FechaHora2": HoraPromesa,
				"Estatus": thes.cabecera.estatusc,
				"Contacto": thes.cabecera.contacto,
				"Equip" : thes.cabecera.equip
			};

			var jsonModel = new sap.ui.model.json.JSONModel(cabecera);
			thes.getView().setModel(jsonModel, "EditCabecera");

			thes.obtenerPagador(thes, Kunum, thes.cabecera.vkorg, thes.cabecera.vtweg);
			thes.obtenerEstatus(thes);
			thes.obtenerUbicacion(thes, Werks);
			thes.ObtenerPersonasContactos(thes, Kunum);
			thes.obtenerTaller(thes, Werks);
			thes.obtenerEstadosGarantias(thes);
			thes.obtenerEquipoAdministrativo();
		},

		onEditarSalesforce: function () {

			var reg = thes.getView().getModel("EditSalesforce").getData();

			if (reg.Inmovil) {
				reg.Inmovil = "X";
			} else {
				reg.Inmovil = "";
			}

			if (reg.Codtipord === "") {
				sap.m.MessageBox.error("Ingrese Tipo de Orden");
				return;
			}

			if (reg.Inmovil === "X") {
				if (reg.Codmotivo === "") {
					sap.m.MessageBox.error("Ingrese Motivo de Inmovilización");
					return;
				}
				if (reg.Comentario === "") {
					sap.m.MessageBox.error("Ingrese Comentario Motivo");
					return;
				}
				if (reg.Feinmovil === "" || reg.Feinmovil === undefined) {
					sap.m.MessageBox.error("Ingrese Fecha Inmovilización");
					return;
				}

				if (reg.Femovil === "") {
					reg.Femovil = null;
				}

				reg.Motivo = thes.byId("cbInmovilizacion").getValue();

			} else {
				reg.Femovil = null;
				reg.Feinmovil = null;
			}

			if (reg.Femovil !== "" && reg.Femovil !== null) {
				reg.Femovil = moment(new Date(reg.Femovil)).format('YYYY-MM-DD[T]HH:mm:ss');
			}

			if (reg.Feinmovil !== "" && reg.Femovil !== null) {
				reg.Feinmovil = moment(new Date(reg.Feinmovil)).format('YYYY-MM-DD[T]HH:mm:ss');
			}

			reg.Sfmod = "X";

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenChangeOrdenSet", reg, {
				success: function (result, status) {
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					thes.obtenerCabeceraOrden();
					sap.ui.core.BusyIndicator.hide();
					thes.byId("dlgEditarSalesforce").close();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
					thes.byId("dlgEditarSalesforce").close();
				}
			});

		},

		obtenerEquipoAdministrativo: function () {
			
			var filters = [];

			var filter = new sap.ui.model.Filter("ARBPL", "EQ", thes.cabecera.vaplz);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_TAB_SRV.read("/ZCDS_VW_BASRRC001_062", {
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "equipoAdministrativo");					
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onEditarCabecera: function () {

			var reg = thes.getView().getModel("EditCabecera").getData();

			if (reg.FechaPromesa2) {
				if (reg.FechaPromesa2 != reg.FechaPromesa || reg.FechaHora != reg.FechaHora2) {
					var fecha = this.dateToFechaSAP(reg.FechaPromesa2);
					var fechaHora2 = fecha + reg.FechaHora2;
					fechaHora2 = thes.stringTodateTime(fechaHora2);
					if (fechaHora2.getTime() < new Date().getTime()) {
						sap.m.MessageBox.error("La fecha y hora promesa debe ser mayor a la actual");
						return;
					} else {
						reg.FechaPromesa = reg.FechaPromesa2;
						reg.FechaHora = reg.FechaHora2;
					}
				}
			}

			if (reg.FechaPromesa) {
				reg.FechaPromesa = thes.dateToFechaSAP(reg.FechaPromesa);
			}

			delete reg["FechaPromesa2"];
			delete reg["FechaHora2"];

			if (reg.Kilometraje) {
				reg.Kilometraje = parseFloat(reg.Kilometraje.replace(',', ""));
				reg.Kilometraje = reg.Kilometraje.toString();
			}

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenChangeOrdenSet", reg, {
				success: function (result, status) {
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					thes.obtenerCabeceraOrden();
					thes.obtenerFlujoDocumentos();
					sap.ui.core.BusyIndicator.hide();
					thes.byId("dlgEditarCabecera").close();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
					thes.byId("dlgEditarCabecera").close();
				}
			});

		},

		onAgregarIncidentesSinHR: function (oEvent) {
			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgAgregarIncidentesSinHR")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.AgregarIncidentesSinHR",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgAgregarIncidentesSinHR").open();
			}

			this.byId("dlgAgregarIncidentes").close();

			//thes.obtenerIncidentes();

			var incidentesSinHR = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": Aufnr,
				"Plnnr": "",
				"Ltxa1": "",
				"Ilart": "",
				"Objid": Objid,
				"Codope": ""
			};

			var jsonModel = new sap.ui.model.json.JSONModel(incidentesSinHR);
			thes.getView().setModel(jsonModel, "incidentesSinHR");

			thes.obtenerCanalFacturacion(Z_OD_SCP_BASRRC001_ORD_SRV, thes);
			thes.obtenerActividad();
			thes.obtenerClasificacion();
			thes.obtenerTecnico();
		},

		onChangeMaterial: function (oEvent) {
			var key = oEvent.getSource().getSelectedKey();
			this.obtenerMaterialSustituto(key);
		},

		onChangePais: function (oEvent) {
			var key = oEvent.getSource().getSelectedKey();
			this.obtenerRegiones(key);
		},

		obtenerMaterialSustituto: function (jsonServicio, jsonServicio2, registros) {
			var filters = [];

			for (var i = 0; i < jsonServicio.length; i++) {
				var filter = new sap.ui.model.Filter("Matnr", "EQ", jsonServicio[i].Matnr);
				filters.push(filter);
			}

			var filter = new sap.ui.model.Filter("Werks", "EQ", Werks);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/OrdenMaterialSustitutoSet", {
				filters: filters,
				urlParameters: {
					"$orderby": "Ersda asc"
				},
				success: function (result) {

					if (result.results.length > 1) {
						for (var i = 0; i < registros.length; i++) {
							var busqueda = result.results.filter(registro => registro.MatnrOri === registros[i].matnr);
							if (busqueda) {
								registros[i].noVer = "X";
								var cantidad = Number(registros[i].Cantidad);
								for (var e = 0; e < busqueda.length; e++) {
									// if (cantidad >= Number(busqueda[e].StockAlm)) {
									// 	cantidad = cantidad - Number(busqueda[e].StockAlm);
									// 	busqueda[e].Cantidad = Number(busqueda[e].StockAlm);
									// } else {
									// 	busqueda[e].Cantidad = cantidad;
									// }

									if (e === 0) {
										cantidad = cantidad - Number(busqueda[e].StockAlm);
										busqueda[e].Cantidad = Number(busqueda[e].StockAlm);
									} else {
										busqueda[e].Cantidad = cantidad;
									}

									if (Number(busqueda[e].Cantidad) === Number(busqueda[e].StockAlm)) {
										busqueda[e].noEditable = "X";
									} else {
										busqueda[e].noEditable = "";
									}
								}
							}
						}

						var registrosLimpios = registros.filter(registro => registro.noVer !== "X");

						var jsonModel = new sap.ui.model.json.JSONModel(busqueda);
						thes.getView().setModel(jsonModel, "materialesSustitutos");
						var jsonModel = new sap.ui.model.json.JSONModel(registrosLimpios);
						thes.getView().setModel(jsonModel, "materiales");
						thes.dialogoMaterialesSutitutos();
						thes.byId("dlgAgregarRefacciones").close();
						sap.ui.core.BusyIndicator.hide();

						// for (var i = 0; i < result.results.length; i++) {
						// 	result.results[i].Cantidad = jsonServicio.Bdmng;

						// 	var busqueda = data.find(element => element.matnr === result.results[i].Matnr);
						// 	if (busqueda){
						// 		var index = data.indexOf(busqueda);
						// 		if (index >= 0){
						// 			data = data.splice(index, 1);
						// 		}								
						// 	}
						// }
						// var jsonModel = new sap.ui.model.json.JSONModel(result.results);
						// thes.getView().setModel(jsonModel, "materialesSustitutos");
						// var jsonModel = new sap.ui.model.json.JSONModel(data);
						// thes.getView().setModel(jsonModel, "materiales");
						// //thes.OrdenMaterialSustitutoSet();
						// thes.dialogoMaterialesSutitutos();
						// thes.byId("dlgAgregarRefacciones").close();
						// sap.ui.core.BusyIndicator.hide();
					} else {
						thes.crearRefaccion(jsonServicio2);
						thes.byId("dlgAgregarRefacciones").close();
					}



				},
				error: function (err) {

				}
			});
		},

		obtenerMaterial: function () {

			var filters = [];
			var filter = new sap.ui.model.Filter("werks", "EQ", Werks);
			filters.push(filter);
			var filter = new sap.ui.model.Filter("emplazamiento", "EQ", thes.cabecera.stort);
			filters.push(filter);
			//var filter = new sap.ui.model.Filter("lgort", "EQ", Aufnr);
			//filters.push(filter);

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_RES_SRV.read("/StockCentroAlmacen", {
				filters: filters,
				urlParameters: {
					"$select": "matnr,DescripcionMaterial,werks,Stock_Alm,lgort,DescripcionAlmacen"
				},
				success: function (result) {
					sap.ui.core.BusyIndicator.hide();
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "materiales");
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerCliente: function () {

			var url = "/OrdenCliente('" + Aufnr + "')";

			Z_OD_SCP_BASRRC001_ORD_SRV.read(url, {
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result);
					thes.getView().setModel(jsonModel, "cliente");
				},
				error: function (err) {
				}
			});

		},

		onItemPressIncidentes: function (oEvent) {

			var indices = thes.byId("smartTableIncidentes").getTable().getSelectedIndices();

			if (indices.length > 1) {

				var filters = [];
				var url2 = "/OrdenOperacion";
				this.getView().byId("smartTableOperaciones").setTableBindingPath(url2);
				this.getView().byId("smartTableOperaciones").rebindTable();

				var filter = new sap.ui.model.Filter("aufnr_real", sap.ui.model.FilterOperator.EQ, Aufnr);
				filters.push(filter);

				for (var i = 0; i < indices.length; i++) {
					var reg = this.byId("smartTableIncidentes").getTable().getContextByIndex(indices[i]).getProperty();
					var filter = new sap.ui.model.Filter("key_zinc", sap.ui.model.FilterOperator.EQ, reg.key_zinc);
					filters.push(filter);
				}

				this.getView().byId("smartTableOperaciones").getTable().getBinding("rows").filter(filters);

			} else {
				var reg = oEvent.getParameters().item.getBindingContext().getProperty();

				Incidentes_ZINC = reg.key_zinc;
				Incidentes_updated = reg.updated;

				var url = oEvent.getParameters().item.getBindingContext().getPath() + "/to_oper";

				this.getView().byId("smartTableOperaciones").setTableBindingPath(url);
				this.getView().byId("smartTableOperaciones").rebindTable();

			}

			var opsOperaciones = this.byId("opsOperaciones");
			this.byId("ObjectPageLayout").setSelectedSection(opsOperaciones);

			//var url = oEvent.getParameters().item.getBindingContext().getPath();
			//url = url.replace("OrdenIncidente", "ZCDS_VW_BASRRC001_048");

			//var url2 = "/ZCDS_VW_BASRRC001_042?$filter=aufnr eq '" + Aufnr + "'";

			// Z_OD_SCP_BASRRC001_ORD_SRV.read(url, {
			// 	success: function (result) {
			// 		var jsonModel = new sap.ui.model.json.JSONModel(result.results[0]);
			// 		thes.byId("smartTableOperaciones").setModel(jsonModel);
			// 	},
			// 	error: function (err) {

			// 	}
			// });

		},

		onBeforeRebindTablaRefacciones: function (oEvent) {

			if (FilterRefacciones) {
				var binding = oEvent.getParameter("bindingParams");
				binding.filters.push(FilterRefacciones);
			}

		},

		onItemPressOperaciones: function (oEvent) {

			var indices = thes.byId("smartTableOperaciones").getTable().getSelectedIndices();

			if (indices.length > 1) {

				var filters = [];
				var filters1 = [];
				var url2 = "/ZCDS_VW_BASRRC001_042";
				this.getView().byId("smartTableRefacciones").setTableBindingPath(url2);
				this.getView().byId("smartTableRefacciones").rebindTable();

				for (var i = 0; i < indices.length; i++) {
					var reg = this.byId("smartTableOperaciones").getTable().getContextByIndex(indices[i]).getProperty();

					var filter1 = new sap.ui.model.Filter("aufnr", sap.ui.model.FilterOperator.EQ, reg.aufnr_real);
					var filter2 = new sap.ui.model.Filter("vornr", sap.ui.model.FilterOperator.EQ, reg.vornr_real);
					var filter3 = new sap.ui.model.Filter({
						filters: [filter1, filter2],
						and: true
					});
					filters1.push(filter3);
				}

				var filters = new sap.ui.model.Filter({
					filters: filters1,
					and: false
				});

				FilterRefacciones = filters;

				this.getView().byId("smartTableRefacciones").getTable().getBinding("rows").filter(filters);
			} else {
				var reg = oEvent.getParameters().item.getBindingContext().getProperty();

				Operaciones_vornr_real = reg.vornr_real;
				Operaciones_updated = reg.updated;

				var url = oEvent.getParameters().item.getBindingContext().getPath() + "/to_ref";
				this.getView().byId("smartTableRefacciones").setTableBindingPath(url);
				this.getView().byId("smartTableRefacciones").rebindTable();
			}

			var opsRefacciones = this.byId("opsRefacciones");
			this.byId("ObjectPageLayout").setSelectedSection(opsRefacciones);

		},

		botonesDetalleVisible: function (visible) {

			//var btnEnviarXentry = this.byId("btnEnviarXentry");
			//var btnReaperturarOrden = this.byId("btnReaperturarOrden");
			var btnModificarCabecera = this.byId("btnModificarCabecera");
			var btnModificarDescuentos = this.byId("btnModificarDescuentos");
			var btnModificarSalesforce = this.byId("btnModificarSalesforce");
			var btnEliminarIncidentes = this.byId("btnEliminarIncidentes");
			var btnAgregarIncidentes = this.byId("btnAgregarIncidentes");
			var btnEditarIncidentes = this.byId("btnEditarIncidentes");
			var btnAnularNotificacion = this.byId("btnAnularNotificacion");
			var btnNotificar = this.byId("btnNotificar");
			var btnImprimirTot = this.byId("btnImprimirTot");
			var btnEliminarOperaciones = this.byId("btnEliminarOperaciones");
			var btnAgregarOperaciones = this.byId("btnAgregarOperaciones");
			var btnModificarOperaciones = this.byId("btnModificarOperaciones");
			var btnEliminarRefacciones = this.byId("btnEliminarRefacciones");
			var btnAgregarRefacciones = this.byId("btnAgregarRefacciones");
			var btnModificarRefacciones = this.byId("btnModificarRefacciones");
			var btnGuardarAdenda = this.byId("btnGuardarAdenda");
			var btnAgregarPosicionesAdendas = this.byId("btnAgregarPosicionesAdendas");
			var btnImprimirOrdenPdf = this.byId("btnImprimirOrdenPdf");
			//var btnDescargarFacturaXML = this.byId("btnDescargarFacturaXML");
			//var btnImprimirFacturaPDF = this.byId("btnImprimirFacturaPDF");
			var btnImprimirFacturaTXT = this.byId("btnImprimirFacturaTXT");
			var btnImprimirPreFacturaPDF = this.byId("btnImprimirPreFacturaPDF");
			var btnPreCierre = this.byId("btnPreCierre");

			//btnEnviarXentry.setVisible(visible);
			//btnReaperturarOrden.setVisible(visible);
			btnModificarCabecera.setVisible(visible);
			btnModificarDescuentos.setVisible(visible);
			btnModificarSalesforce.setVisible(visible);
			btnEliminarIncidentes.setVisible(visible);
			btnAgregarIncidentes.setVisible(visible);
			btnEditarIncidentes.setVisible(visible);
			btnAnularNotificacion.setVisible(visible);
			btnNotificar.setVisible(visible);
			btnImprimirTot.setVisible(visible);
			btnEliminarOperaciones.setVisible(visible);
			btnAgregarOperaciones.setVisible(visible);
			btnModificarOperaciones.setVisible(visible);
			btnEliminarRefacciones.setVisible(visible);
			btnAgregarRefacciones.setVisible(visible);
			btnModificarRefacciones.setVisible(visible);
			btnGuardarAdenda.setVisible(visible);
			btnAgregarPosicionesAdendas.setVisible(visible);
			btnImprimirOrdenPdf.setVisible(visible);
			//btnDescargarFacturaXML.setVisible(visible);
			//btnImprimirFacturaPDF.setVisible(visible);
			btnImprimirFacturaTXT.setVisible(visible);
			btnImprimirPreFacturaPDF.setVisible(visible);
			btnPreCierre.setVisible(visible);

		},

		obtenerCabeceraOrden2: function (oEvent) {

			if (sap.cabecera) {

				if (sap.cabecera.rampa === true) {
					sap.cabecera.rampa2 = "SI";
					sap.cabecera.rampaIcono = "sap-icon://message-success";
					sap.cabecera.rampaEstado = "Success";
				} else {
					sap.cabecera.rampa2 = "NO";
					sap.cabecera.rampaIcono = "sap-icon://message-error";
					sap.cabecera.rampaEstado = "Error";
				}

				if (sap.cabecera.aseguradora === "X") {
					sap.cabecera.aseguradoraIcono = "sap-icon://message-success";
					sap.cabecera.aseguradoraEstado = "Success";
				} else {
					sap.cabecera.aseguradoraIcono = "sap-icon://message-error";
					sap.cabecera.aseguradoraEstado = "Error";
				}

				if (sap.cabecera.estatus === "C") {
					thes.botonesDetalleVisible(false);
					thes.byId("btnDescargarFacturaXML").setVisible(false);
				} else {
					thes.botonesDetalleVisible(true);
					thes.byId("btnDescargarFacturaXML").setVisible(true);
				}

				if (sap.cabecera.StatXentry === "S" && sap.cabecera.estatus !== "C") {
					//sap.m.MessageBox.information("Orden enviada a Xentry");
					thes.botonesDetalleVisible(false);
				}

				sap.cabecera.kilometraje = thes.stringToFormatNumber(sap.cabecera.kilometraje);

				thes.cabecera = sap.cabecera;
				Bukrs = thes.cabecera.bukrs;
				var jsonModel = new sap.ui.model.json.JSONModel(sap.cabecera);
				thes.getView().setModel(jsonModel, "cabeceraOrden");

			} else {

				var filters = [];
				var filter = new sap.ui.model.Filter("aufnr", "EQ", Aufnr);
				filters.push(filter);

				sap.ui.core.BusyIndicator.show(4);
				Z_OD_SCP_BASRRC001_ORD_SRV.read("/OrdenHeader", {
					filters: filters,
					success: function (result) {
						if (result.results[0].rampa === true) {
							result.results[0].rampa2 = "SI";
							result.results[0].rampaIcono = "sap-icon://message-success";
							result.results[0].rampaEstado = "Success";
						} else {
							result.results[0].rampa2 = "NO";
							result.results[0].rampaIcono = "sap-icon://message-error";
							result.results[0].rampaEstado = "Error";
						}

						if (result.results[0].aseguradora === "X") {
							result.results[0].aseguradoraIcono = "sap-icon://message-success";
							result.results[0].aseguradoraEstado = "Success";
						} else {
							result.results[0].aseguradoraIcono = "sap-icon://message-error";
							result.results[0].aseguradoraEstado = "Error";
						}

						if (result.results[0].estatus === "C") {
							thes.botonesDetalleVisible(false);
							thes.byId("btnDescargarFacturaXML").setVisible(false);
						} else {
							thes.botonesDetalleVisible(true);
							thes.byId("btnDescargarFacturaXML").setVisible(true);
						}

						if (result.results[0].StatXentry === "S" && result.results[0].estatus !== "C") {
							//sap.m.MessageBox.information("Orden enviada a Xentry");
							thes.botonesDetalleVisible(false);
						}

						result.results[0].kilometraje = thes.stringToFormatNumber(result.results[0].kilometraje);

						thes.cabecera = result.results[0];
						Bukrs = thes.cabecera.bukrs;
						var jsonModel = new sap.ui.model.json.JSONModel(result.results[0]);
						thes.getView().setModel(jsonModel, "cabeceraOrden");
						sap.ui.core.BusyIndicator.hide();
					},
					error: function (err) {
						sap.ui.core.BusyIndicator.hide();
					}
				});

			}

		},

		obtenerCabeceraOrden: function (oEvent) {

			var filters = [];
			var filter = new sap.ui.model.Filter("aufnr", "EQ", Aufnr);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/OrdenHeader", {
				filters: filters,
				success: function (result) {
					if (result.results[0].rampa === true) {
						result.results[0].rampa2 = "SI";
						result.results[0].rampaIcono = "sap-icon://message-success";
						result.results[0].rampaEstado = "Success";
					} else {
						result.results[0].rampa2 = "NO";
						result.results[0].rampaIcono = "sap-icon://message-error";
						result.results[0].rampaEstado = "Error";
					}

					if (result.results[0].aseguradora === "X") {
						result.results[0].aseguradoraIcono = "sap-icon://message-success";
						result.results[0].aseguradoraEstado = "Success";
					} else {
						result.results[0].aseguradoraIcono = "sap-icon://message-error";
						result.results[0].aseguradoraEstado = "Error";
					}

					if (result.results[0].estatus === "C") {
						thes.botonesDetalleVisible(false);
						thes.byId("btnDescargarFacturaXML").setVisible(false);
					} else {
						thes.botonesDetalleVisible(true);
						thes.byId("btnDescargarFacturaXML").setVisible(true);
					}

					if (result.results[0].StatXentry === "S" && result.results[0].estatus !== "C") {
						//sap.m.MessageBox.information("Orden enviada a Xentry");
						thes.botonesDetalleVisible(false);
					}

					result.results[0].kilometraje = thes.stringToFormatNumber(result.results[0].kilometraje);

					thes.cabecera = result.results[0];
					Bukrs = thes.cabecera.bukrs;
					var jsonModel = new sap.ui.model.json.JSONModel(result.results[0]);
					thes.getView().setModel(jsonModel, "cabeceraOrden");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerServicioAdicionales: function () {
			var arrayFilters = [];
			var filter = new sap.ui.model.Filter("Marca", sap.ui.model.FilterOperator.EQ, thes.cabecera.cod_marca);
			arrayFilters.push(filter);
			var filter = new sap.ui.model.Filter("Modelo", sap.ui.model.FilterOperator.EQ, thes.cabecera.cod_modelo);
			arrayFilters.push(filter);
			var filter = new sap.ui.model.Filter("TipoServicio", sap.ui.model.FilterOperator.EQ, "3");
			arrayFilters.push(filter);

			this.getServicios(arrayFilters, "servicioAdicionales", "3");
		},

		getServicios: function (arrayFilters, nameModel, tipoServicio) {
			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_SRV.read("/CitaServicioDispoSet", {
				filters: arrayFilters,
				success: function (result) {
					sap.ui.core.BusyIndicator.hide();
					if (result.results.length === 0) {

						if (tipoServicio === "4") {
							thes.byId("cbServPrePaga").setVisible(false);
						}

						return;
					}

					if (tipoServicio === "4") {
						thes.byId("cbServPrePaga").setVisible(true);
						sap.m.MessageBox.information("La unidad posee un servicio pre-pagado");
					}

					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, nameModel);
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}

			});

		},

		onItemPressDocumentoFinanciero: function (oEvent) {

			var path = oEvent.mParameters.row.getBindingContext("DocumentosFinancieros").getPath();
			var reg = oEvent.mParameters.row.getBindingContext("DocumentosFinancieros").getProperty(path);

			this.obtenerDetalleDocumentosFinancieros(reg.documento);

		},

		obtenerDetalleDocumentosFinancieros: function (documento) {

			var filters = [];

			var filter = new sap.ui.model.Filter("sociedad", sap.ui.model.FilterOperator.EQ, Bukrs);
			filters.push(filter);
			var filter = new sap.ui.model.Filter("documento", sap.ui.model.FilterOperator.EQ, documento);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show();
			ZCDS_VW_BASRRC001_211_CDS.read("/ZCDS_VW_BASRRC001_211", {
				urlParameters: {
					"$expand": "to_association_name"
				},
				filters: filters,
				success: function (result) {
					if (result.results.length > 0) {
						if (result.results[0].to_association_name.results.length > 0) {

							for (var i = 0; i < result.results[0].to_association_name.results.length; i++) {
								result.results[0].to_association_name.results[i].dmbtr = Number(result.results[0].to_association_name.results[i].dmbtr).toFixed(2);
								result.results[0].to_association_name.results[i].wrbtr = Number(result.results[0].to_association_name.results[i].wrbtr).toFixed(2);
							}

							var jsonModel = new sap.ui.model.json.JSONModel(result.results[0].to_association_name.results);
							thes.getView().setModel(jsonModel, "DocumentoFinancieroDetalle");
						}
					}


					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		completarCeros: function (number, width) {
			var numberOutput = Math.abs(number); /* Valor absoluto del número */
			var length = number.toString().length; /* Largo del número */
			var zero = "0"; /* String de cero */

			if (width <= length) {
				if (number < 0) {
					return ("-" + numberOutput.toString());
				} else {
					return numberOutput.toString();
				}
			} else {
				if (number < 0) {
					return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
				} else {
					return ((zero.repeat(width - length)) + numberOutput.toString());
				}
			}
		},

		obtenerCabeceraDocumentosFinancieros: function () {

			var filters = [];
			var Aufnr2 = Aufnr;

			// if (Aufnr.length !== 12) {
			// 	Aufnr2 = "00000" + Aufnr;
			// }

			Aufnr2 = this.completarCeros(Aufnr2, 12);

			var filter = new sap.ui.model.Filter("referencia", sap.ui.model.FilterOperator.EQ, Aufnr2);
			filters.push(filter);
			// var filter = new sap.ui.model.Filter("Xblnr", sap.ui.model.FilterOperator.EQ, Xblnr);
			// filters.push(filter);

			var jsonModel2 = new sap.ui.model.json.JSONModel([]);
			thes.getView().setModel(jsonModel2, "DocumentoFinancieroDetalle");

			sap.ui.core.BusyIndicator.show();
			ZCDS_VW_BASRRC001_211_CDS.read("/ZCDS_VW_BASRRC001_211", {
				urlParameters: {
					"$orderby": "fecha_entrada asc, hora_entrada asc"
				},
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "DocumentosFinancieros");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onVisualizarDocumentosFinancieros: function (oEvent) {

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgDoccumentosFinancieros")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					controller: this,
					name: "dms4100ui.bacrcr001ord.view.DocumentosFinancieros"
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgDoccumentosFinancieros").open();
			}

			thes.obtenerCabeceraDocumentosFinancieros();

		},

		onBeforeRebindIncidentes: function (oEvent) {
			// var binding = oEvent.getParameter("bindingParams");
			// var oFilter = new sap.ui.model.Filter("aufnr", sap.ui.model.FilterOperator.EQ, Aufnr);
			// binding.filters.push(oFilter);


			if (Father === "V") {
				var url = "/OrdenHeader(aufnr='" + Aufnr + "',maufnr='" + Maufnr + "')/to_zinc";
			} else {
				var url = "/OrdenHeader(aufnr='" + Aufnr + "',maufnr='" + Maufnr + "')/to_zincs";
			}

			this.getView().byId("smartTableIncidentes").setTableBindingPath(url);
			thes.byId("smartTableIncidentes").getModel().refresh();
			//this.getView().byId("smartTableIncidentes").rebindTable();

		},

		onBeforeRebindLogIncidentes: function (oEvent) {
			this.byId("smartTableLogIncidentes").setModel(Z_OD_SCP_BASRRC001_XENTRY_SRV);
			var binding = oEvent.getParameter("bindingParams");
			var oFilter = new sap.ui.model.Filter("orden_servicio", sap.ui.model.FilterOperator.EQ, Aufnr);
			binding.filters.push(oFilter);
			this.getView().byId("smartTableLogIncidentes").getModel().refresh();
		},

		onBeforeRebindLogOperaciones: function (oEvent) {
			this.byId("smartTableLogOperaciones").setModel(Z_OD_SCP_BASRRC001_XENTRY_SRV);
			var binding = oEvent.getParameter("bindingParams");
			var oFilter = new sap.ui.model.Filter("orden_servicio", sap.ui.model.FilterOperator.EQ, Aufnr);
			binding.filters.push(oFilter);
			this.getView().byId("smartTableLogOperaciones").getModel().refresh();
		},

		onBeforeRebindLogRefacciones: function (oEvent) {
			this.byId("smartTableLogRefacciones").setModel(Z_OD_SCP_BASRRC001_XENTRY_SRV);
			var binding = oEvent.getParameter("bindingParams");
			var oFilter = new sap.ui.model.Filter("orden_servicio", sap.ui.model.FilterOperator.EQ, Aufnr);
			binding.filters.push(oFilter);
			this.getView().byId("smartTableLogRefacciones").getModel().refresh();
		},

		onButtonPress: function (oEvent) {
			var oButton = oEvent.getSource();
			this.byId("actionSheet").openBy(oButton);
		},

		onComentarioFactura: function (oEvent) {
			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgComentarioFactura")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					controller: this,
					name: "dms4100ui.bacrcr001ord.view.ComentarioFactura"
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();

					if (thes.byId("btnGuardarComentarioFactura")) {

						if (thes.cabecera.estatus === "C") {
							thes.byId("btnGuardarComentarioFactura").setVisible(false);
						} else {
							thes.byId("btnGuardarComentarioFactura").setVisible(true);
						}

						if (thes.cabecera.StatXentry === "S" && thes.cabecera.estatus !== "C") {
							thes.byId("btnGuardarComentarioFactura").setVisible(false);
						}

					}

				});
			} else {
				this.byId("dlgComentarioFactura").open();

				if (thes.byId("btnGuardarComentarioFactura")) {

					if (thes.cabecera.estatus === "C") {
						thes.byId("btnGuardarComentarioFactura").setVisible(false);
					} else {
						thes.byId("btnGuardarComentarioFactura").setVisible(true);
					}

					if (thes.cabecera.StatXentry === "S" && thes.cabecera.estatus !== "C") {
						thes.byId("btnGuardarComentarioFactura").setVisible(false);
					}

				}

			}

			thes.obtenerComentarioFactura();

		},

		obtenerComentarioFactura: function () {

			var filters = [];

			var filter = new sap.ui.model.Filter("Aufnr", sap.ui.model.FilterOperator.EQ, Aufnr);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/OrdenGetFacSet", {
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results[0]);
					thes.getView().setModel(jsonModel, "modeloComentarioFactura");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onBitacora: function (oEvent) {

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgBitacora")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					controller: this,
					name: "dms4100ui.bacrcr001ord.view.Bitacora"
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgBitacora").open();
			}

			this.obtenerBitacora();

		},

		onCreateBitacora: function (oEvent) {

			var observacion = oEvent.getParameter("value");

			var json = {
				UuidKey: "",
				Aufnr: Aufnr,
				Tipo: "1",
				Observaciones: observacion,
				Scpuser: userInfo.loginName,
				Scpapp: Scpapp,
				FechaHora: new Date()
			};

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_RES_SRV.create("/ReservaBitacoraSet", json, {
				success: function (result) {
					thes.obtenerBitacora();
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					thes.obtenerBitacora();
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerBitacora: function () {

			var filters = [];

			var filter = new sap.ui.model.Filter("Aufnr", sap.ui.model.FilterOperator.EQ, Aufnr);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_RES_SRV.read("/ReservaBitacoraSet", {
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "modelBitacora");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onPresss: function (oEvent) {

		},

		onIconPress: function (oEvent) {

		},

		deleteIncidentes: function (oEvent) {

			var indices = this.byId("smartTableIncidentes").getTable().getSelectedIndices();

			var eliminar = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": Aufnr,
				"Vornr": "",
				"OperacSet": []
			};

			for (var i = 0; i < indices.length; i++) {
				var reg = this.byId("smartTableIncidentes").getTable().getContextByIndex(indices[i]).getProperty();
				var OperacSet = {
					"HdId": "1",
					"Vornr": reg.vornr_real
				};
				eliminar.OperacSet.push(OperacSet);
			}

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenDeleteOpSet", eliminar, {
				success: function (result, status) {

					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}

					thes.byId("smartTableIncidentes").getTable().clearSelection();
					thes.byId("smartTableIncidentes").getTable().getModel().refresh();
					sap.ui.core.BusyIndicator.hide();
					//sap.m.MessageBox.success("Se eliminó con éxito!");
				},
				error: function (err) {
					thes.byId("smartTableIncidentes").getTable().clearSelection();
					thes.byId("smartTableIncidentes").getTable().getModel().refresh();
					sap.m.MessageBox.error("Error al eliminar");
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onGuardarMotivoRechazoIncidente: function () {

			var indices = this.byId("smartTableIncidentes").getTable().getSelectedIndices();
			var reg = this.byId("smartTableIncidentes").getTable().getContextByIndex(indices[0]).getProperty();

			var json = {
				"Orden": Aufnr,
				"Posicion": reg.vornr_real,
				"Texto": this.byId("textAreaMotivoRechazoIncidente").getValue(),
				"Identext": "M"
			};

			this.onGuardarComentarios(json, "dlgMotivoRechazoIncidente");

			this.deleteIncidentes();

		},

		onDialogoMotivoRechazoIncidente: function () {

			if (Expres === false) {
				sap.m.MessageBox.error("No se puede eliminar un incidente en orden expres");
				return;
			}

			var indices = this.byId("smartTableIncidentes").getTable().getSelectedIndices();

			if (indices.length === 0) {
				sap.m.MessageBox.error("Seleccione un registro");
				return;
			}

			var reg = this.byId("smartTableIncidentes").getTable().getContextByIndex(indices[0]).getProperty();

			if (reg.ilart === "ZG2") {
			} else {
				this.deleteIncidentes();
				return;
			}

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgMotivoRechazoIncidente")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.MotivoRechazoIncidentes",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgMotivoRechazoIncidente").open();
			}

			this.byId("textAreaMotivoRechazoIncidente").setValue("");

		},

		onDeleteOperaciones: function (oEvent) {

			if (Expres === false) {
				sap.m.MessageBox.error("No se puede eliminar una operación en orden expres");
				return;
			}

			var indices = this.byId("smartTableOperaciones").getTable().getSelectedIndices();

			var eliminar = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": Aufnr,
				"Vornr": "",
				"OperacSet": []
			};

			for (var i = 0; i < indices.length; i++) {
				var reg = this.byId("smartTableOperaciones").getTable().getContextByIndex(indices[i]).getProperty();
				var OperacSet = {
					"HdId": "1",
					"Vornr": reg.vornr_real
				};
				eliminar.OperacSet.push(OperacSet);
			}

			if (indices.length > 0) {
				sap.ui.core.BusyIndicator.show();
				Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenDeleteOpSet", eliminar, {
					success: function (result, status) {

						if (status.headers["sap-message"]) {
							var mensaje = JSON.parse(status.headers["sap-message"]);
							var mensajes = thes.armarMensaje(mensaje);
							thes.mostrarMensajes(mensajes, thes);
						}

						thes.byId("smartTableOperaciones").getTable().clearSelection();
						thes.byId("smartTableOperaciones").getTable().getModel().refresh();
						sap.ui.core.BusyIndicator.hide();
						//sap.m.MessageBox.success("Se eliminó con éxito!");
					},
					error: function (err) {
						thes.byId("smartTableOperaciones").getTable().clearSelection();
						thes.byId("smartTableOperaciones").getTable().getModel().refresh();
						sap.m.MessageBox.error("Error al eliminar");
						sap.ui.core.BusyIndicator.hide();
					}
				});
			}

		},

		onNotificarOperaciones: function (oEvent) {

			var indices = this.byId("smartTableOperaciones").getTable().getSelectedIndices();

			var notificar = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Anul": "",
				"OrdenChangeChannelIncSet": []
			};

			for (var i = 0; i < indices.length; i++) {
				var reg = this.byId("smartTableOperaciones").getTable().getContextByIndex(indices[i]).getProperty();

				if (reg.Notificado === "X") {
					sap.m.MessageBox.error("Seleccione registros que no estén notificados");
					return;
				}

				var pos = {
					"HdId": "1",
					"Aufnr": Aufnr,
					"Vornr": reg.vornr_real
				};

				notificar.OrdenChangeChannelIncSet.push(pos);

			}

			if (notificar.OrdenChangeChannelIncSet.length > 0) {

				sap.ui.core.BusyIndicator.show();
				Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenNotifOperationSet", notificar, {
					success: function (result, status) {

						if (status.headers["sap-message"]) {
							var mensaje = JSON.parse(status.headers["sap-message"]);
							var mensajes = thes.armarMensaje(mensaje);
							thes.mostrarMensajes(mensajes, thes);
						}

						thes.byId("smartTableOperaciones").getTable().clearSelection();
						thes.byId("smartTableOperaciones").getTable().getModel().refresh();
						sap.ui.core.BusyIndicator.hide();
						//sap.m.MessageBox.success("Se eliminó con éxito!");
					},
					error: function (err) {
						thes.byId("smartTableOperaciones").getTable().clearSelection();
						thes.byId("smartTableOperaciones").getTable().getModel().refresh();
						sap.m.MessageBox.error("Error al eliminar");
						sap.ui.core.BusyIndicator.hide();
					}
				});

			}

		},

		onAnularNotificacionOperaciones: function (oEvent) {
			var indices = this.byId("smartTableOperaciones").getTable().getSelectedIndices();

			var notificar = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Anul": "X",
				"OrdenChangeChannelIncSet": []
			};

			for (var i = 0; i < indices.length; i++) {
				var reg = this.byId("smartTableOperaciones").getTable().getContextByIndex(indices[i]).getProperty();

				if (reg.Notificado === "") {
					sap.m.MessageBox.error("Seleccione registros que estén notificados");
					return;
				}

				var pos = {
					"HdId": "1",
					"Aufnr": Aufnr,
					"Vornr": reg.vornr_real
				};

				notificar.OrdenChangeChannelIncSet.push(pos);

			}

			if (notificar.OrdenChangeChannelIncSet.length > 0) {

				sap.ui.core.BusyIndicator.show();
				Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenNotifOperationSet", notificar, {
					success: function (result, status) {

						if (status.headers["sap-message"]) {
							var mensaje = JSON.parse(status.headers["sap-message"]);
							var mensajes = thes.armarMensaje(mensaje);
							thes.mostrarMensajes(mensajes, thes);
						}

						thes.byId("smartTableOperaciones").getTable().clearSelection();
						thes.byId("smartTableOperaciones").getTable().getModel().refresh();
						sap.ui.core.BusyIndicator.hide();
						//sap.m.MessageBox.success("Se eliminó con éxito!");
					},
					error: function (err) {
						thes.byId("smartTableOperaciones").getTable().clearSelection();
						thes.byId("smartTableOperaciones").getTable().getModel().refresh();
						sap.m.MessageBox.error("Error al eliminar");
						sap.ui.core.BusyIndicator.hide();
					}
				});

			}
		},


		onDeleteRefacciones: function (oEvent) {

			if (Expres === false) {
				sap.m.MessageBox.error("No se puede eliminar una refacción en orden expres");
				return;
			}

			var indices = this.byId("smartTableRefacciones").getTable().getSelectedIndices();

			var eliminar = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": Aufnr,
				"BorraCompSet": []
			};


			for (var i = 0; i < indices.length; i++) {
				var reg = this.byId("smartTableRefacciones").getTable().getContextByIndex(indices[i]).getProperty();

				var BorraCompSet = {
					"HdId": "1",
					"Aufnr": reg.aufnr,
					"Rsnum": reg.rsnum,
					"Vornr": reg.vornr,
					"Posnr": reg.posnr
				};

				eliminar.BorraCompSet.push(BorraCompSet);

			}

			if (indices.length > 0) {
				sap.ui.core.BusyIndicator.show();
				Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenDeleteCompSet", eliminar, {
					success: function (result, status) {

						if (status.headers["sap-message"]) {
							var mensaje = JSON.parse(status.headers["sap-message"]);
							var mensajes = thes.armarMensaje(mensaje);
							thes.mostrarMensajes(mensajes, thes);
						}

						thes.byId("smartTableRefacciones").getTable().clearSelection();
						thes.byId("smartTableRefacciones").getTable().getModel().refresh();
						sap.ui.core.BusyIndicator.hide();
						//sap.m.MessageBox.success("Se eliminó con éxito!");
					},
					error: function (err) {
						thes.byId("smartTableRefacciones").getTable().clearSelection();
						thes.byId("smartTableRefacciones").getTable().getModel().refresh();
						sap.m.MessageBox.error("Error al eliminar");
						sap.ui.core.BusyIndicator.hide();
					}
				});
			}

		},

		onDialogoIncidentes: function () {

			if (Expres === false) {
				sap.m.MessageBox.error("No se puede agregar una incidentes en orden expres");
				return;
			}

			if (thes.cabecera.Salesforce === "X") {
				if (thes.cabecera.codtipord === "") {
					sap.m.MessageBox.error("Ingrese Tipo de Orden para Salesforce");
					return;
				}
			}


			this.obtenerTipoOperacion(thes.cabecera.codtipord);

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgAgregarIncidentes")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.AgregarIncidentes",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgAgregarIncidentes").open();
			}

			thes.obtenerIncidentes();
			thes.obtenerCanalFacturacion(Z_OD_SCP_BASRRC001_ORD_SRV, thes);
			thes.obtenerActividad();
			thes.obtenerClasificacion();
			thes.obtenerTecnico();

		},

		obtenerActividad: function () {

			var filters = [];

			var filter = new sap.ui.model.Filter("marca", sap.ui.model.FilterOperator.EQ, thes.cabecera.cod_marca);
			filters.push(filter);

			Z_OD_SCP_BASRRC001_ORD_SRV.read("/ActividadxMarca", {
				filters: filters,
				success: function (result, status) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "actividad");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {

				}
			});

		},

		obtenerClasificacion: function () {

			var filters = [];

			var filter = new sap.ui.model.Filter("marca", sap.ui.model.FilterOperator.EQ, thes.cabecera.cod_marca);
			filters.push(filter);

			Z_OD_SCP_BASRRC001_ORD_SRV.read("/OrdenClasificacion", {
				filters: filters,
				success: function (result, status) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "clasificacion");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {

				}
			});

		},

		obtenerIncidentes2: function () {
			var year = new Date().getFullYear().toString();
			var filters = [];

			filters.push(new sap.ui.model.Filter("Modelo", sap.ui.model.FilterOperator.EQ, thes.cabecera.cod_modelo));
			filters.push(new sap.ui.model.Filter("Marca", sap.ui.model.FilterOperator.EQ, thes.cabecera.cod_marca));
			filters.push(new sap.ui.model.Filter("Anno", sap.ui.model.FilterOperator.EQ, year));
			filters.push(new sap.ui.model.Filter("TipoServicio", sap.ui.model.FilterOperator.EQ, "1"));
			//this.ModelCatalogo.read("/CitaServicioDispoSet", {
			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_SRV.read("/CitaServicioDispoHRSet", {
				filters: filters,
				success: function (res) {
					if (res.results.length > 0 && res.results[0].Messcode != 1001) {
						for (var i = 0; i < result.value.length; i++) {
							result.value[i].Objid = Objid;
						}
						var jsonModel = new sap.ui.model.json.JSONModel(result.value);
						thes.getView().setModel(jsonModel, "incidentes");
						sap.ui.core.BusyIndicator.hide();
					}
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerIncidentes: function () {

			var fechaHoyString = this.dateToFecha2(fechaHoy);

			var year = new Date().getFullYear().toString();

			if (Hojap === "X") {
				var url = "/bacrcr003-srv/bacrcr004/HojaRuta?$filter=(codmarca eq '" + thes.cabecera.cod_marca +
					"' and codmodelo eq '' and codanno eq '' and vin eq null and utilizacion eq 'HP' and fechaIni le " + fechaHoyString + " and fechaFin ge " + fechaHoyString + ") or (codmarca eq '" + thes.cabecera.cod_marca +
					"' and codmodelo eq '" + thes.cabecera.cod_modelo +
					"' and codanno eq '' and vin eq null and utilizacion eq 'HP' and fechaIni le " + fechaHoyString + " and fechaFin ge " + fechaHoyString + ") or (codmarca eq '" + thes.cabecera.cod_marca +
					"' and codmodelo eq '" + thes.cabecera.cod_modelo +
					"' and codanno eq '" + year + "' and vin eq null and utilizacion eq 'HP' and fechaIni le " + fechaHoyString + " and fechaFin ge " + fechaHoyString + ") or (codmarca eq '" + thes.cabecera.cod_marca +
					"' and codmodelo eq '" + thes.cabecera.cod_modelo +
					"' and codanno eq '" + year + "' and vin eq '" + thes.cabecera.fleet_vin + "' and utilizacion eq 'HP' and fechaIni le " + fechaHoyString + " and fechaFin ge " + fechaHoyString + ")";
			} else {
				var url = "/bacrcr003-srv/bacrcr004/HojaRuta?$filter=(codmarca eq '" + thes.cabecera.cod_marca +
					"' and codmodelo eq '' and codanno eq '' and vin eq null and fechaIni le " + fechaHoyString + " and fechaFin ge " + fechaHoyString + ") or (codmarca eq '" + thes.cabecera.cod_marca + "' and codmodelo eq '" +
					thes.cabecera.cod_modelo + "' and codanno eq '' and vin eq null and fechaIni le " + fechaHoyString + " and fechaFin ge " + fechaHoyString + ") or (codmarca eq '" + thes.cabecera.cod_marca +
					"' and codmodelo eq '" + thes.cabecera.cod_modelo + "' and codanno eq '" + year + "' and vin eq null and fechaIni le " + fechaHoyString + " and fechaFin ge " + fechaHoyString + ") or (codmarca eq '" +
					thes.cabecera.cod_marca + "' and codmodelo eq '" + thes.cabecera.cod_modelo + "' and codanno eq '" + year + "' and vin eq '" +
					thes.cabecera.fleet_vin + "' and fechaIni le " + fechaHoyString + " and fechaFin ge " + fechaHoyString + ")";
			}

			//var url = "/bacrcr003-srv/bacrcr004/HojaRuta";

			sap.ui.core.BusyIndicator.show();
			$.ajax({
				url: url,
				success: function (result) {
					for (var i = 0; i < result.value.length; i++) {
						result.value[i].Objid = Objid;
					}
					var jsonModel = new sap.ui.model.json.JSONModel(result.value);
					thes.getView().setModel(jsonModel, "incidentes");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		validarOperacionesLocal: function (codigoOperacion) {

			var flag = false;
			var url = "/bacrcr003-srv/bacrcr004/Operaciones?$filter=(codigoOperacion eq '" + codigoOperacion + "' and esLocal eq true)";

			sap.ui.core.BusyIndicator.show();
			$.ajax({
				async: false,
				url: url,
				success: function (result) {
					if (result.value.length > 0) {
						flag = true;
					}
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

			return flag;

		},

		clearFilterOperaciones: function () {
			this.byId("iptSearchCodigoOperacion").setValue();
			this.byId("iptSearchDescripcionOperacion").setValue();
			this.byId("iptSearchDuracion").setValue();
			this.byId("iptSearchUnidad").setValue();
		},

		onDialogoOperaciones: function () {

			if (Expres === false) {
				sap.m.MessageBox.error("No se puede agregar una operación en orden expres");
				return;
			}

			var oView = this.getView();

			var flag = false;

			if (Incidentes_updated === "X") {
				flag = true;
			}

			if (thes.cabecera.Hojap === "X") {
				flag = true;
			}

			if (!flag) {
				sap.m.MessageBox.error("No se permite agregar operaciones");
				return;
			}

			// create dialog lazily
			if (!this.byId("dlgAgregarOperaciones")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.AgregarOperaciones",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgAgregarOperaciones").open();
			}

			//thes.obtenerServicioAdicionales();
			thes.obtenerOperaciones();
			thes.obtenerOperacionesMRA();
			thes.clearFilterOperaciones();
			thes.byId("tableOperaciones").clearSelection();
			thes.byId("tableOperacionesMRA").clearSelection();
		},

		obtenerOperacionesMRA: function () {

			var filters = [];
			var filter = new sap.ui.model.Filter("VIN", "EQ", thes.cabecera.fleet_vin);
			filters.push(filter);

			Z_OD_SCP_BASRRC001_ORD_SRV.read("/ZCDS_VW_BASRRC001_050", {
				urlParameters: {
					"$expand": "to_Oper"
				},
				filters: filters,
				success: function (result) {
					if (result.results[0]) {
						var jsonModel = new sap.ui.model.json.JSONModel(result.results[0].to_Oper.results);
						thes.getView().setModel(jsonModel, "operacionesMRA");
					}
				},
				error: function (err) {

				}
			});

		},

		buscarOperaciones: function () {

			var codOpe = this.byId("iptSearchCodigoOperacion").getValue();
			var desOpe = this.byId("iptSearchDescripcionOperacion").getValue();
			var duranc = this.byId("iptSearchDuracion").getValue();
			var unidad = this.byId("iptSearchUnidad").getValue();
			var locales = this.byId("chkOperacionesLocales").getSelected();
			var url = "/bacrcr003-srv/bacrcr004/Operaciones?$filter=codmarca eq '" + thes.cabecera.cod_marca + "'";

			if (codOpe !== "") {
				url = url + " and contains(codigoOperacion,'" + codOpe + "')";
			}

			if (desOpe !== "") {
				url = url + " and contains(descripcion,'" + desOpe + "')";
			}

			if (duranc !== "") {
				url = url + " and duracion eq " + duranc;
			}

			if (unidad !== "") {
				url = url + " and contains(unidadMedida,'" + unidad + "')";
			}

			if (locales !== "") {
				url = url + " and esLocal eq " + locales;
			}

			this.getOperaciones(url);

		},

		getOperaciones: function (url) {

			sap.ui.core.BusyIndicator.show();
			$.ajax({
				url: url,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.value);
					thes.getView().setModel(jsonModel, "operaciones");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerOperaciones: function () {

			var year = new Date().getFullYear().toString();

			if (Hojap === "X") {
				var url = "/bacrcr003-srv/bacrcr004/Operaciones?$filter=(codmarca eq '" + thes.cabecera.cod_marca +
					"' and codmodelo eq '' and codanno eq '' and esHojalateria eq true) or (codmarca eq '" + thes.cabecera.cod_marca +
					"' and codmodelo eq '" + thes.cabecera.cod_modelo +
					"' and codanno eq '' and esHojalateria eq true) or (codmarca eq '" + thes.cabecera.cod_marca + "' and codmodelo eq '" + thes.cabecera
						.cod_modelo +
					"' and codanno eq '" + year + "' and esHojalateria eq true)";
			} else {
				var url = "/bacrcr003-srv/bacrcr004/Operaciones?$filter=(codmarca eq '" + thes.cabecera.cod_marca +
					"' and codmodelo eq '' and codanno eq '' and esHojalateria eq false) or (codmarca eq '" + thes.cabecera.cod_marca +
					"' and codmodelo eq '" + thes.cabecera.cod_modelo +
					"' and codanno eq '' and esHojalateria eq false) or (codmarca eq '" + thes.cabecera.cod_marca + "' and codmodelo eq '" + thes.cabecera
						.cod_modelo +
					"' and codanno eq '" + year + "' and esHojalateria eq false)";
			}

			this.getOperaciones(url);
		},

		onEditarIncidentes: function () {
			var reg = thes.getView().getModel("EditRegisInci").getData();

			var json = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"OrdenChangeChannelIncSet": [{
					"HdId": "1",
					"Aufnr": Aufnr,
					"Vornr": reg.vornr_real,
					"Ilart": reg.ilart,
					"Objid": reg.OBJID,
					"Ltxa1": reg.descripcion,
					"Anlzu": reg.anlzu,
					"Usr01": reg.usr1,
					"Codope": reg.Codope,
					"Usr03": thes.cabecera.cod_marca
				}]
			};

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenChangeChannelSet", json, {
				success: function (result, status) {

					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					thes.byId("smartTableIncidentes").getTable().clearSelection();
					thes.byId("smartTableIncidentes").getTable().getModel().refresh();
					thes.byId("dlgEditarIncidentes").close();
					sap.ui.core.BusyIndicator.hide();
					//sap.m.MessageBox.success("Se eliminó con éxito!");
				},
				error: function (err) {
					thes.byId("smartTableIncidentes").getTable().clearSelection();
					thes.byId("smartTableIncidentes").getTable().getModel().refresh();
					sap.m.MessageBox.error("Error al Editar");
					sap.ui.core.BusyIndicator.hide();
				}
			});
		},

		onEditarOperaciones: function () {

			var reg = thes.getView().getModel("EditRegisOpe").getData();

			var editar = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": reg.aufnr,
				"OrdenAddOpItemSet": [{
					"HdId": "1",
					"Vornr": reg.vornr_real,
					"Ltxa1": reg.descripcion,
					"Work": reg.duracion,
					"UnWork": reg.unidad
				}]
			};

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenChangeOpSet", editar, {
				success: function (result, status) {

					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}

					thes.byId("smartTableOperaciones").getTable().clearSelection();
					thes.byId("smartTableOperaciones").getTable().getModel().refresh();
					thes.byId("dlgEditarOperaciones").close();
					sap.ui.core.BusyIndicator.hide();
					//sap.m.MessageBox.success("Se eliminó con éxito!");
				},
				error: function (err) {
					thes.byId("smartTableOperaciones").getTable().clearSelection();
					thes.byId("smartTableOperaciones").getTable().getModel().refresh();
					sap.m.MessageBox.error("Error al Editar");
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onEditarRefacciones: function () {

			var reg = thes.getView().getModel("EditRegis").getData();

			if (reg.Precio == reg.PrecioAnt) {
				reg.Precio = "0";
			}

			var editar = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": reg.aufnr,
				"Rsnum": reg.rsnum,
				"Vornr": reg.vornr,
				"Posnr": reg.posnr,
				"Bdmng": reg.bdmng,
				"Meins": reg.meins,
				"Netpr": reg.Precio
			};

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenChangeCompSet", editar, {
				success: function (result, status) {

					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}

					thes.byId("smartTableRefacciones").getTable().clearSelection();
					thes.byId("smartTableRefacciones").getTable().getModel().refresh();
					thes.byId("dlgEditarRefacciones").close();
					sap.ui.core.BusyIndicator.hide();
					//sap.m.MessageBox.success("Se eliminó con éxito!");
				},
				error: function (err) {
					thes.byId("smartTableRefacciones").getTable().clearSelection();
					thes.byId("smartTableRefacciones").getTable().getModel().refresh();
					sap.m.MessageBox.error("Error al Editar");
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onEditarIncidentesMasivos: function () {

			var regs = thes.getView().getModel("EditRegisInciMasi").getData();

			var Ilart = thes.byId("cbCanalFacturacion").getSelectedKey();

			for (var i = 0; i < regs.OrdenChangeChannelIncSet.length; i++) {

				regs.OrdenChangeChannelIncSet[i].Ilart = Ilart;
				regs.OrdenChangeChannelIncSet[i].Usr03 = thes.cabecera.cod_marca;

			}

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenChangeChannelSet", regs, {
				success: function (result, status) {

					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}

					thes.byId("smartTableIncidentes").getTable().clearSelection();
					thes.byId("smartTableIncidentes").getTable().getModel().refresh();
					thes.byId("dlgEditarIncidentesMasivos").close();
					sap.ui.core.BusyIndicator.hide();
					//sap.m.MessageBox.success("Se eliminó con éxito!");
				},
				error: function (err) {
					thes.byId("smartTableIncidentes").getTable().clearSelection();
					thes.byId("smartTableIncidentes").getTable().getModel().refresh();
					sap.m.MessageBox.error("Error al Editar");
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onDialogoIncidentesEdit: function () {

			if (Expres === false) {
				sap.m.MessageBox.error("No se puede editar un incidente en orden expres");
				return;
			}

			var indices = this.byId("smartTableIncidentes").getTable().getSelectedIndices();

			if (indices.length !== 1) {

				var json = {
					"HdId": "1",
					"Scpuser": userInfo.loginName,
					"Scpapp": Scpapp,
					"OrdenChangeChannelIncSet": [],
				};

				for (var i = 0; i < indices.length; i++) {

					var reg = this.byId("smartTableIncidentes").getTable().getContextByIndex(indices[i]).getProperty();

					var flag = false;

					if (reg.updated === "X") {
						flag = true;
					}

					if (thes.cabecera.Hojap === "X") {
						flag = true;
					}


					if (!flag) {
						sap.m.MessageBox.error("No se permite editar el Inicidente");
						return;
					}

					var OrdenChangeChannelIncSet = {
						"HdId": "1",
						"Aufnr": Aufnr,
						"Vornr": reg.vornr_real,
						"Ilart": reg.ilart,
						"Codope": reg.Codope
					};

					json.OrdenChangeChannelIncSet.push(OrdenChangeChannelIncSet);

					if (json.OrdenChangeChannelIncSet.length === indices.length) {

						var oModel = new sap.ui.model.json.JSONModel(json);
						thes.getView().setModel(oModel, "EditRegisInciMasi");

						var oView = this.getView();

						// create dialog lazily
						if (!this.byId("dlgEditarIncidentesMasivos")) {
							// load asynchronous XML fragment
							Fragment.load({
								id: oView.getId(),
								name: "dms4100ui.bacrcr001ord.view.EditarIncidentesMasivos",
								controller: this
							}).then(function (oDialog) {
								// connect dialog to the root view of this component (models, lifecycle)
								oView.addDependent(oDialog);
								oDialog.open();
							});
						} else {
							this.byId("dlgEditarIncidentesMasivos").open();
						}

						thes.obtenerCanalFacturacion(Z_OD_SCP_BASRRC001_ORD_SRV, thes);
						thes.obtenerActividad();
						thes.obtenerClasificacion();

					}

				}

				return;
			}

			var reg = this.byId("smartTableIncidentes").getTable().getContextByIndex(indices[0]).getProperty();

			var flag = false;

			reg.Codope = reg.SfCodOpe;

			if (reg.updated === "X") {
				flag = true;
			}

			if (thes.cabecera.Hojap === "X") {
				flag = true;
			}


			if (!flag) {
				//sap.m.MessageBox.error("No se permite editar el Inicidente");
				//return;
				var propiedades = {
					editable: false
				};
				var oModel2 = new sap.ui.model.json.JSONModel(propiedades);
				thes.getView().setModel(oModel2, "propiedades");

			} else {
				var propiedades = {
					editable: true
				};
				var oModel2 = new sap.ui.model.json.JSONModel(propiedades);
				thes.getView().setModel(oModel2, "propiedades");
			}

			var oModel = new sap.ui.model.json.JSONModel(reg);
			thes.getView().setModel(oModel, "EditRegisInci");

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgEditarIncidentes")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.EditarIncidentes",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgEditarIncidentes").open();
			}

			thes.obtenerCanalFacturacion(Z_OD_SCP_BASRRC001_ORD_SRV, thes);
			thes.obtenerTecnico();
			thes.obtenerActividad();
			thes.obtenerClasificacion();
			this.obtenerTipoOperacion(thes.cabecera.codtipord);

		},

		onDialogoOperacionesEdit: function () {

			if (Expres === false) {
				sap.m.MessageBox.error("No se puede editar una operación en orden expres");
				return;
			}

			var indices = this.byId("smartTableOperaciones").getTable().getSelectedIndices();

			if (indices.length !== 1) {
				sap.m.MessageBox.error("Selecione un registro");
				return;
			}

			var reg = this.byId("smartTableOperaciones").getTable().getContextByIndex(indices[0]).getProperty();

			var flag = false;

			if (thes.cabecera.Hojap === "X") {
				flag = true;
			}

			flag = thes.validarOperacionesLocal(reg.USR00);

			if (!flag) {
				sap.m.MessageBox.error("No se permite editar la operación");
				return;
			}

			var oModel = new sap.ui.model.json.JSONModel(reg);
			thes.getView().setModel(oModel, "EditRegisOpe");

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgEditarOperaciones")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.EditarOperaciones",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgEditarOperaciones").open();
			}
		},

		onDialogoRefaccionesEdit: function () {

			if (Expres === false) {
				sap.m.MessageBox.error("No se puede editar una refacción en orden expres");
				return;
			}

			var indices = this.byId("smartTableRefacciones").getTable().getSelectedIndices();

			if (indices.length !== 1) {
				sap.m.MessageBox.error("Selecione un registro");
				return;
			}

			if (!Operaciones_vornr_real) {
				return;
			}

			var reg = this.byId("smartTableRefacciones").getTable().getContextByIndex(indices[0]).getProperty();

			var flag = false;

			if (reg.updated === "X") {
				flag = true;
			}

			if (thes.cabecera.Hojap === "X") {
				flag = true;
			}

			if (!flag) {
				sap.m.MessageBox.error("No se permite editar la refación");
				return;
			}

			reg.PrecioAnt = reg.Precio;

			var oModel = new sap.ui.model.json.JSONModel(reg);
			thes.getView().setModel(oModel, "EditRegis");

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgEditarRefacciones")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.EditarRefacciones",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgEditarRefacciones").open();
			}
		},

		onDialogoRefacciones: function () {

			if (Expres === false) {
				sap.m.MessageBox.error("No se puede agregar una refacción en orden expres");
				return;
			}

			if (!Operaciones_vornr_real) {
				return;
			}

			var flag = false;

			if (Operaciones_updated === "X") {
				flag = true;
			}

			if (thes.cabecera.Hojap === "X") {
				flag = true;
			}

			if (!flag) {
				sap.m.MessageBox.error("No se permite agregar refaciones");
				return;
			}

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgAgregarRefacciones")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.AgregarRefacciones",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgAgregarRefacciones").open();
			}

			this.obtenerMaterial();
		},

		dialogoMaterialesSutitutos: function () {
			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgMaterialSustituto")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.MaterialSustituto",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgMaterialSustituto").open();
			}
		},

		onCrearRefacciones: function (oEvent) {
			var id = oEvent.getSource().data("id");

			switch (id) {
				case "dlgAgregarRefacciones":
					var idTabla = "tableMaterial";
					var model = "materiales";
					break;
				case "dlgMaterialSustituto":
					var idTabla = "tableMaterialSustituto";
					var model = "materialesSustitutos";
					break;
			}

			var index = thes.byId(idTabla).getSelectedIndices();

			var jsonServicio = {
				"HdId": "1",
				"OrdenAdicionarCompSet": []
			};

			switch (id) {
				case "dlgAgregarRefacciones":
					if (index.length <= 0) {
						sap.m.MessageBox.error("Seleccione mínimo un registro");
						return;
					}

					var materialesBusquedaSustitutos = [];
					var data = [];

					for (var i = 0; i < index.length; i++) {

						var path = thes.byId(idTabla).getContextByIndex(index[i]).getPath();
						var reg = thes.getView().getModel(model).getProperty(path);
						data.push(reg);

						if (isNaN(reg.Cantidad) || Number(reg.Cantidad) === 0) {
							sap.m.MessageBox.error("Ingrese una cantidad en el Material " + reg.matnr);
							return;
						}

						if (!reg.matnr) {
							reg.matnr = reg.Matnr;
						}

						var OrdenAdicionarCompSet = {
							"HdId": "1",
							"Scpuser": userInfo.loginName,
							"Scpapp": Scpapp,
							"Aufnr": thes.cabecera.aufnr,
							"Rsnum": thes.cabecera.rsnum,
							"Vornr": Operaciones_vornr_real,
							"Lgort": reg.lgort,
							"Matnr": reg.matnr,
							"Bdmng": reg.Cantidad
						};

						jsonServicio.OrdenAdicionarCompSet.push(OrdenAdicionarCompSet);
						if (Number(reg.Cantidad) > Number(reg.Stock_Alm)) {
							materialesBusquedaSustitutos.push(OrdenAdicionarCompSet);
						}

					}

					if (materialesBusquedaSustitutos.length > 0) {
						this.obtenerMaterialSustituto(materialesBusquedaSustitutos, jsonServicio, data);
					} else {
						thes.crearRefaccion(jsonServicio);
						thes.byId("dlgAgregarRefacciones").close();
					}

					break;
				case "dlgMaterialSustituto":

					var materiales = thes.getView().getModel("materiales").getData();
					var materialesSustitutos = thes.getView().getModel("materialesSustitutos").getData();

					for (var i = 0; i < materiales.length; i++) {
						var OrdenAdicionarCompSet = {
							"HdId": "1",
							"Scpuser": userInfo.loginName,
							"Scpapp": Scpapp,
							"Aufnr": thes.cabecera.aufnr,
							"Rsnum": thes.cabecera.rsnum,
							"Vornr": Operaciones_vornr_real,
							"Lgort": materiales[i].Lgort,
							"Matnr": materiales[i].Matnr,
							"Bdmng": materiales[i].Cantidad.toString()
						};
						jsonServicio.OrdenAdicionarCompSet.push(OrdenAdicionarCompSet);
					}

					for (var i = 0; i < materialesSustitutos.length; i++) {
						var OrdenAdicionarCompSet = {
							"HdId": "1",
							"Scpuser": userInfo.loginName,
							"Scpapp": Scpapp,
							"Aufnr": thes.cabecera.aufnr,
							"Rsnum": thes.cabecera.rsnum,
							"Vornr": Operaciones_vornr_real,
							"Lgort": materialesSustitutos[i].Lgort,
							"Matnr": materialesSustitutos[i].Matnr,
							"Bdmng": materialesSustitutos[i].Cantidad.toString()
						};
						jsonServicio.OrdenAdicionarCompSet.push(OrdenAdicionarCompSet);
					}

					thes.crearRefaccion(jsonServicio);
					thes.byId("dlgMaterialSustituto").close();

					break;
			}



			// switch (id) {
			// 	case "dlgAgregarRefacciones":
			// 		this.obtenerMaterialSustituto(jsonServicio);
			// 		break;
			// 	case "dlgMaterialSustituto":						
			// 		thes.byId("dlgMaterialSustituto").close();
			// 		break;
			// }

		},

		onCrearIncidentesSinHR: function () {

			var reg = thes.getView().getModel("incidentesSinHR").getData();

			if (reg.descripcion === "" || reg.descripcion === undefined) {
				sap.m.MessageBox.error("Ingrese una descripción");
				return;
			}

			if (reg.Ilart === "" || reg.Ilart === undefined) {
				sap.m.MessageBox.error("Ingrese una Canal de Facturación");
				return;
			}

			if (reg.ANLZU === "" || reg.ANLZU === undefined) {
				sap.m.MessageBox.error("Ingrese una Actividad");
				return;
			}

			if (reg.USR01 === "" || reg.USR01 === undefined) {
				sap.m.MessageBox.error("Ingrese una Clasificación");
				return;
			}

			if (reg.Objid === "" || reg.Objid === undefined) {
				sap.m.MessageBox.error("Ingrese un Técnico");
				return;
			}

			if (thes.cabecera.Salesforce === "X") {
				if (reg.Codope === "" || reg.Codope === undefined) {
					sap.m.MessageBox.error("Ingrese un Tipo de Operación");
					return;
				}
			} else {
				reg.Codope = "";
			}


			var jsonServicio = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": Aufnr,
				"Plnnr": "",
				"Ltxa1": reg.descripcion,
				"Ilart": reg.Ilart,
				"Plnty": "",
				"Plnal": "",
				"Zaehl": "",
				"Anlzu": reg.ANLZU,
				"Usr01": reg.USR01,
				"Objid": reg.Objid,
				"Codope": reg.Codope,
				"Usr03": thes.cabecera.cod_marca
			};

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenAddIncidenteSet", jsonServicio, {
				success: function (result, status) {
					thes.byId("smartTableIncidentes").getTable().clearSelection();
					thes.byId("smartTableIncidentes").getTable().getModel().refresh();

					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}

					sap.ui.core.BusyIndicator.hide();
					thes.byId("dlgAgregarIncidentesSinHR").close();
					//sap.m.MessageBox.success("Se guardó con éxito!");
				},
				error: function (err) {
					thes.byId("smartTableIncidentes").getTable().clearSelection();
					thes.byId("smartTableIncidentes").getTable().getModel().refresh();
					thes.byId("dlgAgregarIncidentesSinHR").close();
					sap.m.MessageBox.error("Error al guardar");
					sap.ui.core.BusyIndicator.hide();
				}
			});
		},

		onCrearIncidentes: function () {

			var index = thes.byId("tableIncidentes").getSelectedIndex();

			if (index < 0) {
				sap.m.MessageBox.error("Seleccione un registro");
				return;
			}

			var path = thes.byId("tableIncidentes").getContextByIndex(index).getPath();
			var reg = thes.getView().getModel("incidentes").getProperty(path);

			if (reg.descripcion === "" || reg.descripcion === undefined) {
				sap.m.MessageBox.error("Ingrese una descripción");
				return;
			}

			if (reg.Ilart === "" || reg.Ilart === undefined) {
				sap.m.MessageBox.error("Ingrese una Canal de Facturación");
				return;
			}

			if (reg.ANLZU === "" || reg.ANLZU === undefined) {
				sap.m.MessageBox.error("Ingrese una Actividad");
				return;
			}

			if (reg.USR01 === "" || reg.USR01 === undefined) {
				sap.m.MessageBox.error("Ingrese una Clasificación");
				return;
			}

			if (reg.Objid === "" || reg.Objid === undefined) {
				sap.m.MessageBox.error("Ingrese un Técnico");
				return;
			}

			if (thes.cabecera.Salesforce === "X") {
				if (reg.Codope === "" || reg.Codope === undefined) {
					sap.m.MessageBox.error("Ingrese un Tipo de Operación");
					return;
				}
			} else {
				reg.Codope = "";
			}

			var jsonServicio = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": Aufnr,
				"Plnnr": reg.grupo,
				"Ltxa1": reg.descripcion,
				"Ilart": reg.Ilart,
				"Plnty": "",
				"Plnal": "",
				"Zaehl": "",
				"Anlzu": reg.ANLZU,
				"Usr01": reg.USR01,
				"Objid": reg.Objid,
				"Codope": reg.Codope,
				"Usr03": thes.cabecera.cod_marca
			};

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenAddIncidenteSet", jsonServicio, {
				success: function (result, status) {
					thes.byId("smartTableIncidentes").getTable().clearSelection();
					thes.byId("smartTableIncidentes").getTable().getModel().refresh();

					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					sap.ui.core.BusyIndicator.hide();
					thes.byId("dlgAgregarIncidentes").close();
					//sap.m.MessageBox.success("Se guardó con éxito!");
				},
				error: function (err) {
					thes.byId("smartTableIncidentes").getTable().clearSelection();
					thes.byId("smartTableIncidentes").getTable().getModel().refresh();
					thes.byId("dlgAgregarIncidentes").close();
					sap.m.MessageBox.error("Error al guardar");
					sap.ui.core.BusyIndicator.hide();
				}
			});
		},

		crearRefaccion: function (jsonServicio) {
			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenAdicionarCompMultiSet", jsonServicio, {
				success: function (result, status) {
					thes.byId("smartTableRefacciones").getTable().clearSelection();
					thes.byId("smartTableRefacciones").getTable().getModel().refresh();

					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					sap.ui.core.BusyIndicator.hide();
					//sap.m.MessageBox.success("Se guardó con éxito!");
				},
				error: function (err) {
					thes.byId("smartTableRefacciones").getTable().clearSelection();
					thes.byId("smartTableRefacciones").getTable().getModel().refresh();
					thes.byId("dlgAgregarRefacciones").close();
					sap.m.MessageBox.error("Error al guardar");
					sap.ui.core.BusyIndicator.hide();
				}
			});
		},

		onCrearOperacionesZtot: function () {

			var operacionesZtot = thes.getView().getModel("operacionesZtot").getData();
			var index = thes.byId("tableOperaciones").getSelectedIndex();
			var path = thes.byId("tableOperaciones").getContextByIndex(index).getPath();
			var reg = thes.getView().getModel("operaciones").getProperty(path);

			if (operacionesZtot.Lifnr === "") {
				sap.m.MessageBox.error("Seleccione un proveedor");
				return;
			}
			if (operacionesZtot.Asnum === "") {
				sap.m.MessageBox.error("Seleccione un servicio");
				return;
			}
			if (operacionesZtot.Precio === "") {
				sap.m.MessageBox.error("Ingrese un precio");
				return;
			}
			if (isNaN(operacionesZtot.Precio)) {
				sap.m.MessageBox.error("Ingrese un precio numérico");
				operacionesZtot.Precio = "";
				return;
			}
			if (operacionesZtot.Sbrtwr === "") {
				sap.m.MessageBox.error("Ingrese un costo");
				return;
			}
			if (isNaN(operacionesZtot.Sbrtwr)) {
				sap.m.MessageBox.error("Ingrese un costo numérico");
				operacionesZtot.Precio = "";
				return;
			}
			if (operacionesZtot.Waers === "") {
				sap.m.MessageBox.error("Seleccione una moneda");
				return;
			}

			reg.Lifnr = operacionesZtot.Lifnr;
			reg.Asnum = operacionesZtot.Asnum;
			reg.Precio = operacionesZtot.Precio;
			reg.Sbrtwr = operacionesZtot.Sbrtwr;
			reg.Waers = operacionesZtot.Waers;

			var refacciones = [];
			refacciones = thes.obtenerRefaccionesOpe(reg.ID);

			if (!reg.descripcion) {
				reg.descripcion = "";
			}
			if (!reg.puestoTrabajoObjty) {
				reg.puestoTrabajoObjty = "";
			}
			if (!reg.puestoTrabajoObjid) {
				reg.puestoTrabajoObjid = "";
			}
			if (!reg.claveControl) {
				reg.claveControl = "";
			}
			if (!reg.actividad) {
				reg.actividad = "";
			}
			if (!reg.unidadMedida) {
				reg.unidadMedida = "";
			}
			if (!reg.usr00) {
				reg.usr00 = "";
			}
			if (!reg.usr01) {
				reg.usr01 = "";
			}
			if (!reg.usr03) {
				reg.usr03 = "";
			}

			var jsonServicio = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": Aufnr,
				"OrdenAddOpSingleItemSet": [{
					"HdId": "1",
					"Vornr": "0200",
					"Ltxa1": reg.descripcion,
					"Objty": reg.puestoTrabajoObjty,
					"Objid": reg.puestoTrabajoObjid,
					"Werks": Werks,
					"Steus": reg.claveControl,
					"Anlzu": reg.actividad,
					"Work": reg.duracion.toString(),
					"UnWork": reg.unidadMedida,
					"Usr00": reg.codigoOperacion,
					"Usr01": reg.usr01,
					"Usr02": Incidentes_ZINC,
					"Usr03": reg.usr03,
					"Lifnr": reg.Lifnr,
					"Asnum": reg.Asnum,
					"Sbrtwr": reg.Sbrtwr.toString(),
					"Waers": reg.Waers,
					"Precio": reg.Precio.toString(),
					"OrdenAdicionarCompSet": refacciones
				}]
			};

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenAddOpSingleSet", jsonServicio, {
				success: function (result, status) {
					thes.byId("smartTableOperaciones").getTable().clearSelection();
					thes.byId("smartTableOperaciones").getTable().getModel().refresh();
					thes.byId("dlgAgregarOperaciones").close();
					thes.byId("dlgOperacionesZtot").close();

					var filters = [];

					var filter = new sap.ui.model.Filter("aufnr", sap.ui.model.FilterOperator.EQ, result.Aufnr);
					filters.push(filter);

					var filter = new sap.ui.model.Filter("Operac", sap.ui.model.FilterOperator.EQ, result.OrdenAddOpSingleItemSet.results[0].Usr00);
					filters.push(filter);
					thes.obtenerPDFTot(filters);
					thes.obtenerFlujoDocumentos();

					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					thes.byId("smartTableOperaciones").getTable().clearSelection();
					thes.byId("smartTableOperaciones").getTable().getModel().refresh();
					thes.byId("dlgAgregarOperaciones").close();
					thes.byId("dlgOperacionesZtot").close();
					sap.m.MessageBox.error("Error al guardar");
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onDialogoOperacionesZtot: function () {

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgOperacionesZtot")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.AgregarOperacionesZtot",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgOperacionesZtot").open();
			}

			var operacionZtot = {
				Lifnr: "",
				Asnum: "",
				Precio: "",
				Sbrtwr: "",
				Waers: ""
			};

			var jsonModel = new sap.ui.model.json.JSONModel(operacionZtot);
			thes.getView().setModel(jsonModel, "operacionesZtot");

			thes.obtenerProveedor(" ");
			thes.obtenerServicio();
			thes.obtenerMoneda();

		},

		sisProveedor: function (oEvent) {

			var KeyProveedor = oEvent.getParameters().selectedItem.getAdditionalText();
			oEvent.getSource().setSelectedKey(KeyProveedor);
			var textProveedor = oEvent.getParameters().selectedItem.getText();
			oEvent.getSource().setValue(textProveedor);

		},

		LcProveedor: function (oEvent) {

			var proveedor = oEvent.getSource().getValue();

			//var urlPro = " and (Proveedor eq '" + proveedor + "')";

			var filters = [];
			var filter = new sap.ui.model.Filter("Nombre", "Contains", proveedor);
			filters.push(filter);
			var filter = new sap.ui.model.Filter("Proveedor", "Contains", proveedor);
			filters.push(filter);

			var filter2 = new sap.ui.model.Filter({
				filters: filters,
				and: false
			});

			this.obtenerProveedor(filter2);

		},

		obtenerProveedor: function (filters) {

			if (filters === " ") {
				var filters = [];
			}

			Z_OD_SCP_BACRCR002_SRV.read("/ProveedorSocSet", {
				filters: [filters],
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "proveedor");
					thes.getView().getModel("proveedor").refresh();
				},
				error: function (err) {

				}
			});

		},

		// obtenerProveedor: function (proveedor) {

		// 	var url = "/bacrcr002-srv/bacrcr002/Proveedores?$filter=Sociedad  eq '" + Bukrs + "'" + proveedor;

		// 	sap.ui.core.BusyIndicator.show();
		// 	$.ajax({
		// 		url: url,
		// 		success: function (result) {
		// 			var jsonModel = new sap.ui.model.json.JSONModel(result.value);
		// 			thes.getView().setModel(jsonModel, "proveedor");
		// 			thes.getView().getModel("proveedor").refresh();
		// 			sap.ui.core.BusyIndicator.hide();
		// 		},
		// 		error: function (err) {
		// 			sap.ui.core.BusyIndicator.hide();
		// 		}
		// 	});

		// },

		obtenerServicio: function () {

			Z_OD_SCP_BASRRC001_ORD_SRV.read("/OrdenNumServicio", {
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "servicio");
				},
				error: function (err) {

				}
			});

		},

		obtenerMoneda: function () {

			var filters = [];
			var filter = new sap.ui.model.Filter("Bukrs", "EQ", Bukrs);
			filters.push(filter);

			Z_OD_SCP_CORE_0001_SRV.read("/ModenasAppSet", {
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "moneda");
				},
				error: function (err) {

				}
			});

		},

		onCrearOperaciones: function (oEvent) {

			var tab = this.byId("iconTabBarOperaciones");

			switch (tab.getSelectedKey()) {
				case 'operaciones':
					var idTabla = "tableOperaciones";
					var idModelo = "operaciones";
					break;

				case 'operacionesMRA':
					var idTabla = "tableOperacionesMRA";
					var idModelo = "operacionesMRA";
					break;
			}

			var index = thes.byId(idTabla).getSelectedIndex();

			if (index < 0) {
				sap.m.MessageBox.error("Seleccione un registro");
				return;
			}

			if (Incidentes_updated === "") {
				if (!Incidentes_ZINC) {
					return;
				}
			}

			var path = thes.byId(idTabla).getContextByIndex(index).getPath();
			var reg = thes.getView().getModel(idModelo).getProperty(path);

			if (reg.claveControl === "ZTOT") {
				thes.onDialogoOperacionesZtot();
				return;
			}

			switch (tab.getSelectedKey()) {
				case 'operaciones':

					var refacciones = [];
					refacciones = thes.obtenerRefaccionesOpe(reg.ID);

					if (!reg.descripcion) {
						reg.descripcion = "";
					}
					if (!reg.puestoTrabajoObjty) {
						reg.puestoTrabajoObjty = "";
					}
					if (!reg.puestoTrabajoObjid) {
						reg.puestoTrabajoObjid = "";
					}
					if (!reg.claveControl) {
						reg.claveControl = "";
					}
					if (!reg.actividad) {
						reg.actividad = "";
					}
					if (!reg.unidadMedida) {
						reg.unidadMedida = "";
					}
					if (!reg.usr00) {
						reg.usr00 = "";
					}
					if (!reg.usr01) {
						reg.usr01 = "";
					}
					if (!reg.usr03) {
						reg.usr03 = "";
					}

					if (!reg.asnum) {
						reg.asnum = "";
					}

					if (!userInfo.loginName) {
						userInfo.loginName = "Prueba";
					}

					var jsonServicio = {
						"HdId": "1",
						"Scpuser": userInfo.loginName,
						"Scpapp": Scpapp,
						"Aufnr": Aufnr,
						"OrdenAddOpSingleItemSet": [{
							"HdId": "1",
							"Vornr": "0200",
							"Ltxa1": reg.descripcion.substr(0, 40),
							"Objty": reg.puestoTrabajoObjty,
							"Objid": reg.puestoTrabajoObjid,
							"Werks": Werks,
							"Steus": reg.claveControl,
							"Anlzu": reg.actividad,
							"Work": reg.duracion.toString(),
							"UnWork": reg.unidadMedida,
							"Usr00": reg.codigoOperacion,
							"Usr01": reg.usr01,
							"Usr02": Incidentes_ZINC,
							"Usr03": reg.usr03,
							"Lifnr": "",
							"Asnum": reg.asnum,
							"Sbrtwr": "0",
							"Waers": "",
							"OrdenAdicionarCompSet": refacciones
						}]
					};

					break;

				case 'operacionesMRA':

					var refacciones = [];

					if (!reg.DescOper) {
						reg.DescOper = "";
					}
					if (!reg.puestoTrabajoObjty) {
						reg.puestoTrabajoObjty = "";
					}
					if (!reg.puestoTrabajoObjid) {
						reg.puestoTrabajoObjid = "";
					}
					if (!reg.Steus) {
						reg.Steus = "";
					}
					if (!reg.actividad) {
						reg.actividad = "";
					}
					if (!reg.Unidad) {
						reg.Unidad = "";
					}
					if (!reg.usr00) {
						reg.usr00 = "";
					}
					if (!reg.usr01) {
						reg.usr01 = "";
					}
					if (!reg.usr03) {
						reg.usr03 = "";
					}

					if (!reg.asnum) {
						reg.asnum = "";
					}

					if (!userInfo.loginName) {
						userInfo.loginName = "Prueba";
					}

					var jsonServicio = {
						"HdId": "1",
						"Scpuser": userInfo.loginName,
						"Scpapp": Scpapp,
						"Aufnr": Aufnr,
						"OrdenAddOpSingleItemSet": [{
							"HdId": "1",
							"Vornr": "0200",
							"Ltxa1": reg.DescOper.substr(0, 40),
							"Objty": reg.puestoTrabajoObjty,
							"Objid": reg.puestoTrabajoObjid,
							"Werks": Werks,
							"Steus": reg.Steus,
							"Anlzu": reg.actividad,
							"Work": reg.Duracion0,
							"UnWork": reg.Unidad,
							"Usr00": reg.CodOper,
							"Usr01": reg.usr01,
							"Usr02": Incidentes_ZINC,
							"Usr03": reg.usr03,
							"Lifnr": "",
							"Asnum": reg.asnum,
							"Sbrtwr": "0",
							"Waers": "",
							"OrdenAdicionarCompSet": refacciones
						}]
					};

					break;
			}

			if (thes.cabecera.eqfnr) {

				sap.ui.core.BusyIndicator.show();
				Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenAddOpSingleSet", jsonServicio, {
					success: function (result, status) {
						thes.byId("smartTableOperaciones").getTable().clearSelection();
						thes.byId("smartTableOperaciones").getTable().getModel().refresh();
						thes.byId("dlgAgregarOperaciones").close();

						if (status.headers["sap-message"]) {
							var mensaje = JSON.parse(status.headers["sap-message"]);
							var mensajes = thes.armarMensaje(mensaje);
							thes.mostrarMensajes(mensajes, thes);
						}
						sap.ui.core.BusyIndicator.hide();

						//sap.m.MessageBox.success("Se guardó con éxito!");
					},
					error: function (err) {
						thes.byId("smartTableOperaciones").getTable().clearSelection();
						thes.byId("smartTableOperaciones").getTable().getModel().refresh();
						thes.byId("dlgAgregarOperaciones").close();
						sap.m.MessageBox.error("Error al guardar");
						sap.ui.core.BusyIndicator.hide();
					}
				});

			} else {

				var json = {};

				json.Aufnr = Aufnr;

				sap.ui.core.BusyIndicator.show(4);
				Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdValdtSet", json, {
					success: function (result, status) {
						if (status.headers["sap-message"]) {
							var mensaje = JSON.parse(status.headers["sap-message"]);
							var mensajes = thes.armarMensaje(mensaje);
							var flag = true;

							for (var i = 0; i < mensajes.length; i++) {
								if (mensajes[i].Type === "error") {
									flag = false;
								}
							}

							if (flag) {

								sap.ui.core.BusyIndicator.show();
								Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenAddOpSingleSet", jsonServicio, {
									success: function (result, status) {
										if (status.headers["sap-message"]) {
											var mensaje = JSON.parse(status.headers["sap-message"]);
											var mensajes = thes.armarMensaje(mensaje);
											thes.mostrarMensajes(mensajes, thes);
										}
										sap.ui.core.BusyIndicator.hide();

										var json = {
											"HdId": "1",
											"Scpuser": userInfo.loginName,
											"Scpapp": Scpapp,
											"Aufnr": Aufnr
										};

										sap.ui.core.BusyIndicator.show();
										Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenCierreSet", json, {
											success: function (result, status) {
												thes.byId("smartTableRefacciones").rebindTable();
												thes.byId("smartTableOperaciones").getTable().clearSelection();
												thes.byId("smartTableOperaciones").getTable().getModel().refresh();
												thes.byId("dlgAgregarOperaciones").close();
												thes.obtenerFlujoDocumentos();
												thes.obtenerCabeceraOrden();
												sap.ui.core.BusyIndicator.hide();
											},
											error: function (err) {
												sap.m.MessageBox.error("Error al crear pedido");
												sap.ui.core.BusyIndicator.hide();
											}
										});
									},
									error: function (err) {
										thes.byId("smartTableOperaciones").getTable().clearSelection();
										thes.byId("smartTableOperaciones").getTable().getModel().refresh();
										thes.byId("dlgAgregarOperaciones").close();
										sap.m.MessageBox.error("Error al guardar");
										sap.ui.core.BusyIndicator.hide();
									}
								});

							} else {
								thes.mostrarMensajes(mensajes, thes);
							}

						}
						sap.ui.core.BusyIndicator.hide();
					},
					error: function (err) {
						sap.ui.core.BusyIndicator.hide();
					}
				});

			}
		},

		obtenerRefaccionesOpe: function (id) {
			var refacciones = [];
			var url = "/bacrcr003-srv/bacrcr004/RefaccionOperacion?$filter=operacion_ID eq " + id;

			$.ajax({
				url: url,
				async: false,
				success: function (result) {
					for (var i = 0; i < result.value.length; i++) {
						var regRefacciones = {
							"HdId": "1",
							"Vornr": "0200",
							"Matnr": result.value[i].refaccion_ID,
							"Bdmng": result.value[i].cantidad.toString(),
							"Meins": result.value[i].unidadMedida
						};
						refacciones.push(regRefacciones);
					}
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

			return refacciones;

		},

		onCierreTecnico: function (oEvent) {

			var json = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": Aufnr
			};

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenCierreTecnicoSet", json, {
				success: function (result, status) {

					thes.byId("smartTableOperaciones").getTable().clearSelection();
					thes.byId("smartTableOperaciones").getTable().getModel().refresh();

					thes.byId("smartTableRefacciones").getTable().clearSelection();
					thes.byId("smartTableRefacciones").getTable().getModel().refresh();

					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onGuardarComentarioFactura: function () {

			var json = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": Aufnr,
				"Observacion": this.byId("textAreaComentarioFactura").getValue()
			};

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenComentarioFacSet", json, {
				success: function (result, status) {

					thes.byId("dlgComentarioFactura").close();

					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});
		},

		ImprimirFacturaPDF: function () {
			thes.onSaveDialog("PDF");
		},

		ImprimirFacturaXML: function () {
			thes.onSaveDialog("XML");
		},

		ImprimirFacturaText: function () {
			thes.onSaveDialog("TXT");
		},

		onSaveDialog: function (tipo) {
			sap.ui.core.BusyIndicator.show(0);

			var _oModelTicketHeaders = {
				"fact": thes.cabecera.Factura,
			};

			let that = this;
			let i = 0;
			let index = -1;
			let gen = false;
			Z_OD_SCP_BAVNVT001_SRV.read("/GetAttFactSet", {
				headers: _oModelTicketHeaders,
				success: function (oData, response) {
					console.log("La respuesta", oData.results, response.oData)
					var hdrMessageObject = new sap.ui.model.json.JSONModel(oData.results);
					oData.results.forEach(item => {
						console.log("El item", item)
						if (item.ObjType == tipo) {
							index = i
							gen = true
						}
						i++
					})
					//INICIO
					if (gen) {
						if (gen) {

							var cleaned_hex = oData.results[index].DocXstr;
							console.log("cleanedex", cleaned_hex);
							//  var nombre = "Documento-" + self.getView().getModel("data").getProperty("/mblnr") + ".pdf";
							var nombre = "Documento-"

							switch (tipo) {
								case "PDF":
									that.onViewPDF(cleaned_hex, nombre);
									break;
								case "XML":
									that.downloadXMl(cleaned_hex, nombre);
									break;
								case "TXT":
									that.onViewTXT(cleaned_hex, nombre);
									break;
							}


							console.log("La respuesta", oData);
							sap.ui.core.BusyIndicator.hide(0);
						} else {
							sap.m.MessageBox.show(hdrMessageObject.message, {
								icon: sap.m.MessageBox.Icon.ERROR,
								title: "Error",
								actions: [sap.m.MessageBox.Action.OK],
								onClose: function (oAction) {
									sap.ui.core.BusyIndicator.hide(0);
								}
							});
						}
					} else {
						var msg = "No hay datos de impresión para la factura";
						sap.m.MessageBox.show(msg, {
							icon: sap.m.MessageBox.Icon.ERROR,
							title: "Error",
							actions: [sap.m.MessageBox.Action.OK],
							onClose: function (oAction) {
								sap.ui.core.BusyIndicator.hide(0);
							}
						});
					}
					//FIN
				},
				error: function (oError) {
					sap.ui.core.BusyIndicator.hide(0);
					if (oError.responseText) {
						oError.responseText = JSON.parse(oError.responseText);
						sap.m.MessageBox.error(oError.responseText.error.message.value);
					}
				}
			});
		},

		onViewXML: function (data, nombre) {
			var xmlDoc = decodeURIComponent(escape(window.atob(data)));
			thes.downloadXMl(data);
			console.log("El xml", xmlDoc);
			var link = document.createElement('a');
			link.innerHTML = 'Download XML file';
			link.download = 'Factura.xml';
			link.href = 'data:application/octet-stream;base64,' + data;
			document.body.appendChild(link);

			let blob = new Blob([xmlDoc], {
				type: 'text/xml'
			});
			let url = URL.createObjectURL(blob);
			window.open(url);
			URL.revokeObjectURL(url); //Releases the resources
		},
		downloadXMl: function (data) {

			var element = document.createElement('a');
			element.setAttribute('href', 'data:application/xml;base64,' + data);
			element.setAttribute('download', "Factura");
			element.style.display = 'none';
			document.body.appendChild(element);
			element.click();
			document.body.removeChild(element)
		},

		onViewTXT: function (data, nombre) {
			var xmlDoc = decodeURIComponent(escape(window.atob(data)));
			console.log("El TXT", xmlDoc)

			var fileBlob = new Blob([xmlDoc], {
				type: "text/plain"
			});
			var link = document.createElement("a");
			link.download = 'Factura.txt';
			link.href = 'data:text/plain;charset=utf-8,' + xmlDoc;
			document.body.appendChild(link);
			link.click();
			document.body.removeChild(link);
			let url = URL.createObjectURL(fileBlob);
			window.open(url);
			URL.revokeObjectURL(url); //Releases the resources
		},
		onViewPDF2: function (data, nombre) {
			var datos = this.hexToBase64(data);
			//var datos = data;
			var objbuilder = '';
			objbuilder += ('<object width="100%" height="100%" data="data:application/pdf;base64,');
			objbuilder += (datos);
			objbuilder += ('" type="application/pdf" class="internal">');
			objbuilder += ('<embed src="data:application/pdf;base64,');
			objbuilder += (datos);
			objbuilder += ('" type="application/pdf" />');
			objbuilder += ('</object>');
			var win = window.open("#", "_blank");
			var title = nombre
			win.document.write('<html><title>' + title +
				'</title><body style="margin-top:0px; margin-left: 0px; margin-right: 0px; margin-bottom: 0px;">');
			win.document.write(objbuilder);
			win.document.write('</body></html>');
			sap.ui.core.BusyIndicator.hide();
		},
		hexToBase64: function (hexstring) {
			return btoa(hexstring.match(/\w{2}/g).map(function (a) {
				return String.fromCharCode(parseInt(a, 16));
			}).join(""));
		},
		obtenerCanalFacturacion: function () {

			var filters = [];

			var filter = new sap.ui.model.Filter("flgMO", sap.ui.model.FilterOperator.EQ, true);
			filters.push(filter);

			var filter = new sap.ui.model.Filter("werks", "EQ", Werks);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/OrdenCatCanalFact", {
				filters: filters,
				success: function (result) {

					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "canalFacturacion");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onCrearPedido: function () {

			var json = {
				"Aufnr": Aufnr,
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp
			};

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenCreatePedidoSet", json, {
				success: function (result, status) {
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});
		},

		onPreCierre: function () {

			var json = {};

			json.Aufnr = Aufnr;

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdValdtSet", json, {
				success: function (result, status) {
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						var flag = true;

						for (var i = 0; i < mensajes.length; i++) {
							if (mensajes[i].Type === "error") {
								flag = false;
							}
						}

						if (flag) {

							var json = {
								"HdId": "1",
								"Scpuser": userInfo.loginName,
								"Scpapp": Scpapp,
								"Aufnr": Aufnr
							};

							sap.ui.core.BusyIndicator.show();
							Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenCierreSet", json, {
								success: function (result, status) {
									if (status.headers["sap-message"]) {
										var mensaje = JSON.parse(status.headers["sap-message"]);
										var mensajes = thes.armarMensaje(mensaje);
										thes.mostrarMensajes(mensajes, thes);
									}
									thes.obtenerCabeceraOrden();
									thes.getView().byId("smartTableRefacciones").rebindTable();
									sap.ui.core.BusyIndicator.hide();
								},
								error: function (err) {
									sap.m.MessageBox.error("Error al crear pedido");
									sap.ui.core.BusyIndicator.hide();
								}
							});

						} else {
							thes.mostrarMensajes(mensajes, thes);
						}

					}
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onAgregarPosicionesAdendas: function () {
			var reg;
			var regIni = this.getView().getModel("adendaDetalleIni").getData();
			var regActual = this.getView().getModel("adendaDetalle").getData();
			var pos = regActual.length / regIni.length;

			for (var i = 0; i < regIni.length; i++) {
				var reg = regIni[i];
				reg.valor = "";
				reg.posnr = thes.zfill(pos + 2, 6);
				regActual.push(reg);
			}

			var jsonModel = new sap.ui.model.json.JSONModel(regActual);
			thes.getView().setModel(jsonModel, "adendaDetalle");
			this.getView().getModel("adendaDetalle").refresh();
		}

	});
});