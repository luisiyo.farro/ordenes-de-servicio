jQuery.sap.require("sap.m.MessageBox");
sap.ui.define([
	"dms4100ui/bacrcr001ord/controller/BaseController",
	"sap/m/MessageToast",
	"sap/ui/core/Fragment"
], function (BaseController, MessageToast, Fragment) {
	"use strict";
	var Z_OD_SCP_BASRRC001_RES_SRV;
	var Z_OD_SCP_BASRRC001_ORD_SRV;
	var Z_OD_SCP_BASRRC001_SRV;
	var Z_OD_SCP_CORE_0001_SRV;
	var Z_OD_SCP_BAVNVT001_SRV;
	var ZCDS_VW_BASRRC001_116_CDS;
	var thes;
	var userInfo;
	var userAttributes;
	var bukrs;
	var werks;
	var stort;
	var botonEditarFiltro;
	var Scpapp = "bacrcr001ord";
	return BaseController.extend("dms4100ui.bacrcr001ord.controller.Home", {

		onInit: function (oEvent) {
			var oRouter = this.getRouter();
			oRouter.getRoute("home").attachMatched(this._onRouteMatched, this);
		},

		_onRouteMatched: function () {
			thes = this;
			Z_OD_SCP_BASRRC001_ORD_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_ORD_SRV");
			Z_OD_SCP_BASRRC001_RES_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_RES_SRV");
			Z_OD_SCP_BASRRC001_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_SRV");
			Z_OD_SCP_CORE_0001_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_CORE_0001_SRV");
			Z_OD_SCP_BAVNVT001_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BAVNVT001_SRV");
			ZCDS_VW_BASRRC001_116_CDS = this.getOwnerComponent().getModel("ZCDS_VW_BASRRC001_116_CDS");
			botonEditarFiltro = true;
			this.byId("iconTabFilAbiertas").setModel(Z_OD_SCP_BASRRC001_ORD_SRV);
			this.byId("iconTabFilCerradas").setModel(Z_OD_SCP_BASRRC001_ORD_SRV);
			this.byId("iconTabFilAnuladas").setModel(Z_OD_SCP_BASRRC001_ORD_SRV);
			this.byId("iconTabFilFacturadas").setModel(Z_OD_SCP_BASRRC001_ORD_SRV);
			this.datosCabecera();
			this.obtenerUsuario();
			this.obtenerSociedades();
			this.obtenerCentros();
			this.obtenerEmplazamientos();
			this.EditarBotonFiltro();
			thes.obtenerKPICitasEstado();
		},

		EditarBotonFiltro: function () {
			//thes.byId("btnEditarFiltros").setVisible(botonEditarFiltro);
		},

		datosCabecera: function () {

			var json = {
				bukrs: "",
				butxt: "",
				werks: "",
				name1: "",
				stort: "",
				ktext: ""
			};

			var jsonModel = new sap.ui.model.json.JSONModel(json);
			thes.getView().setModel(jsonModel, "datosCabecera");

		},

		obtenerUsuario: function () {

			var url = "/bacrcr002-srv/security/getAuthInfo()";

			sap.ui.core.BusyIndicator.show();
			$.ajax({
				url: url,
				async: false,
				success: function (result) {
					userInfo = result.userInfo;
					userAttributes = result.userAttributes;

					if (userAttributes.ROLNAME.length === 0) {
						sap.m.MessageBox.error("No dispone de ningún Rol en el atributo (ROLNAME), contacte al administrador de sistema");
						return;
					}

					thes.obtenerControlesObjetos(userAttributes.ROLNAME);

					var jsonModel = new sap.ui.model.json.JSONModel(userInfo);
					thes.getView().setModel(jsonModel, "userInfo");

					var jsonModel2 = new sap.ui.model.json.JSONModel(userAttributes);
					thes.getView().setModel(jsonModel2, "userAttributes");

					if (sessionStorage.bukrs) {
						bukrs = sessionStorage.bukrs;
					}
					if (sessionStorage.werks) {
						werks = sessionStorage.werks;
					}
					if (sessionStorage.stort) {
						stort = sessionStorage.stort;
					} else {
						stort = userAttributes.STORT[0];
					}

					var cabecera = thes.getView().getModel("datosCabecera").getData();
					if (bukrs) {
						cabecera.bukrs = bukrs;
					}
					if (werks) {
						cabecera.werks = werks;
					}
					if (stort) {
						cabecera.stort = stort;
					}

					thes.getView().getModel("datosCabecera").refresh();

					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onChCentroCabecera: function (oEvent) {
			var centro = oEvent.getSource().getSelectedKey();
			var filters = [];

			if (centro === undefined || centro === "") {
				return;
			}

			if (userAttributes.STORT[0] !== "ALL") {
				if (userAttributes.STORT.length === 1) {
					botonEditarFiltro = false;
				}
				for (var i = 0; i < userAttributes.STORT.length; i++) {

					var filter = new sap.ui.model.Filter("stand", "EQ", userAttributes.STORT[i]);
					filters.push(filter);

				}
			}

			var filter = new sap.ui.model.Filter("werks", "EQ", centro);
			filters.push(filter);

			this.byId("cbEmplazamientoCabecera").setSelectedKey("");

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/OrdenEmplazamiento", {
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "emplazamientos");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});
		},

		onChSociedadCabecera: function (oEvent) {
			var sociedad = oEvent.getSource().getSelectedKey();

			if (sociedad === undefined || sociedad === "") {
				return;
			}

			var url = "/bacrcr002-srv/core/Centros";

			if (userAttributes.WERKS[0] !== "ALL") {
				if (userAttributes.STORT.length === 1) {
					botonEditarFiltro = false;
				}
				for (var i = 0; i < userAttributes.WERKS.length; i++) {

					if (i === 0) {
						url = url + "?$filter=Bukrs eq '" + sociedad + "' and Werks eq '" + userAttributes.WERKS[i] + "'";
					} else {
						url = url + " or Werks eq '" + userAttributes.WERKS[i] + "'";
					}

				}
			} else {
				url = url + "?$filter=Bukrs eq '" + sociedad + "'";
			}

			this.byId("cbCentroCabecera").setSelectedKey("");
			this.byId("cbEmplazamientoCabecera").setSelectedKey("");

			sap.ui.core.BusyIndicator.show();
			$.ajax({
				url: url,
				async: false,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.value);
					thes.getView().setModel(jsonModel, "Centros");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerSociedades: function () {

			var url = "/bacrcr002-srv/core/Sociedades";

			if (userAttributes.BUKRS[0] !== "ALL") {
				if (userAttributes.STORT.length === 1) {
					botonEditarFiltro = false;
				}
				for (var i = 0; i < userAttributes.BUKRS.length; i++) {

					if (i === 0) {
						url = url + "?$filter=Bukrs eq '" + userAttributes.BUKRS[i] + "'";
					} else {
						url = url + " or Bukrs eq '" + userAttributes.BUKRS[i] + "'";
					}
				}
			}

			sap.ui.core.BusyIndicator.show();
			$.ajax({
				url: url,
				async: false,
				success: function (result) {

					var cabecera = thes.getView().getModel("datosCabecera").getData();

					if (sessionStorage.bukrs) {
						var regBukrs = result.value.find(element => element.Bukrs === sessionStorage.bukrs);
					}
					if (regBukrs) {
						cabecera.bukrs = regBukrs.Bukrs;
						cabecera.butxt = regBukrs.Butxt;
						bukrs = regBukrs.Bukrs;
					} else {
						cabecera.bukrs = result.value[0].Bukrs;
						cabecera.butxt = result.value[0].Butxt;
						bukrs = result.value[0].Bukrs;
					}
					thes.getView().getModel("datosCabecera").refresh();
					var jsonModel = new sap.ui.model.json.JSONModel(result.value);
					thes.getView().setModel(jsonModel, "Sociedades");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerCentros: function () {

			var url = "/bacrcr002-srv/core/Centros";

			if (userAttributes.WERKS[0] !== "ALL") {
				if (userAttributes.STORT.length === 1) {
					botonEditarFiltro = false;
				}
				for (var i = 0; i < userAttributes.WERKS.length; i++) {
					if (i === 0) {
						url = url + "?$filter=Bukrs eq '" + bukrs + "' and Werks eq '" + userAttributes.WERKS[i] + "'";
					} else {
						url = url + " or Werks eq '" + userAttributes.WERKS[i] + "'";
					}
				}
			} else {
				url = url + "?$filter=Bukrs eq '" + bukrs + "'";
			}


			sap.ui.core.BusyIndicator.show();
			$.ajax({
				url: url,
				async: false,
				success: function (result) {

					var cabecera = thes.getView().getModel("datosCabecera").getData();

					if (sessionStorage.werks) {
						var regWerks = result.value.find(element => element.Werks === sessionStorage.Werks);
					}
					if (regWerks) {
						cabecera.Werks = regWerks.Werks;
						cabecera.Name1 = regWerks.Name1;
						Werks = regWerks.Werks;
					} else {
						cabecera.werks = result.value[0].Werks;
						cabecera.name1 = result.value[0].Name1;
						werks = result.value[0].Werks;
					}

					thes.getView().getModel("datosCabecera").refresh();
					var jsonModel = new sap.ui.model.json.JSONModel(result.value);
					thes.getView().setModel(jsonModel, "Centros");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerEmplazamientos: function () {
			var filters = [];

			if (userAttributes.STORT[0] !== "ALL") {
				if (userAttributes.STORT.length === 1) {
					botonEditarFiltro = false;
				}
				for (var i = 0; i < userAttributes.STORT.length; i++) {
					var filter = new sap.ui.model.Filter("stand", "EQ", userAttributes.STORT[i]);
					filters.push(filter);
				}
			}

			if (werks !== "ALL") {
				var filter = new sap.ui.model.Filter("werks", "EQ", werks);
				filters.push(filter);
			}

			// var filter = new sap.ui.model.Filter("stand", "EQ", stort);
			// filters.push(filter);

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/OrdenEmplazamiento", {
				filters: filters,
				success: function (result) {

					var cabecera = thes.getView().getModel("datosCabecera").getData();
					if (sessionStorage.stort) {
						var regStort = result.results.find(element => element.stand === sessionStorage.stort);
					}
					if (regStort) {
						cabecera.stort = regStort.stand;
						cabecera.ktext = regStort.ktext;
						stort = regStort.stand;
					} else {
						cabecera.stort = result.results[0].stand;
						cabecera.ktext = result.results[0].ktext;
						stort = result.results[0].stand;
					}
					thes.getView().getModel("datosCabecera").refresh();
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "emplazamientos");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onItbSelect: function (oEvent) {
			var key = oEvent.getParameters().key;

			switch (key) {
				case "Abiertas":
					thes.onBeforeRebindTablaAbiertas2();
					break;
				case "Cerradas":
					thes.onBeforeRebindTablaCerradas();
					break;
				case "Anuladas":
					thes.onBeforeRebindTablaAnuladas();
					break;
				case "Facturadas":
					thes.onBeforeRebindTablaFacturadas();
					break;
			}

		},

		onPreCierre: function () {

			var indices = thes.byId("stTablaAbiertas").getTable().getSelectedIndices();

			if (indices.length > 1) {
				sap.m.MessageBox.error("Seleccione un registro");
				return;
			}

			var reg = thes.byId("stTablaAbiertas").getTable().getContextByIndex(indices[0]).getProperty();

			var json = {};

			json.Aufnr = reg.aufnr;

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdValdtSet", json, {
				success: function (result, status) {
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						var flag = true;

						for (var i = 0; i < mensajes.length; i++) {
							if (mensajes[i].Type === "error") {
								flag = false;
							}
						}


						if (flag) {

							var indices = thes.byId("stTablaAbiertas").getTable().getSelectedIndices();

							if (indices.length > 1) {
								sap.m.MessageBox.error("Seleccione un registro");
								return;
							}

							var reg = thes.byId("stTablaAbiertas").getTable().getContextByIndex(indices[0]).getProperty();

							var json = {
								"HdId": "1",
								"Scpuser": userInfo.loginName,
								"Scpapp": Scpapp,
								"Aufnr": reg.aufnr
							};

							sap.ui.core.BusyIndicator.show();
							Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenCierreSet", json, {
								success: function (result, status) {
									thes.byId("stTablaAbiertas").getTable().clearSelection();
									thes.byId("stTablaAbiertas").getTable().getModel().refresh();

									if (status.headers["sap-message"]) {
										var mensaje = JSON.parse(status.headers["sap-message"]);
										var mensajes = thes.armarMensaje(mensaje);
										thes.mostrarMensajes(mensajes, thes);
									}
									sap.ui.core.BusyIndicator.hide();
								},
								error: function (err) {
									thes.byId("stTablaAbiertas").getTable().clearSelection();
									thes.byId("stTablaAbiertas").getTable().getModel().refresh();
									sap.m.MessageBox.error("Error en el Pre-Cierre");
									sap.ui.core.BusyIndicator.hide();
								}
							});

						} else {
							thes.mostrarMensajes(mensajes, thes);
						}

					}
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onDialogoGrafico: function () {

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgGrafico")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.Grafico",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgAgregarOrdenes").open();
			}

		},

		newModelFacturar: function (reg) {

			var facturar = {
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": reg.aufnr,
				"CfdiUse": "",
				"Zlsch": "",
				"Banknumber": "",
				"Bankaccount": ""

			};

			this.obtenerCuentasBancarias(reg.pagador);

			var jsonModel = new sap.ui.model.json.JSONModel(facturar);
			thes.getView().setModel(jsonModel, "facturar");

		},

		onValDialogoFacturar: function () {

			var indices = this.byId("stTablaCerradas").getTable().getSelectedIndices();

			if (indices.length > 1) {
				sap.m.MessageBox.error("Seleccione un registro");
			}


			var reg = this.byId("stTablaCerradas").getTable().getContextByIndex(indices[0]).getProperty();
			this.newModelFacturar(reg);

			var filters = [];
			var filter = new sap.ui.model.Filter("auart_os", "EQ", reg.auart);
			filters.push(filter);

			Z_OD_SCP_BASRRC001_ORD_SRV.read("/OrdenClaseFactura", {
				filters: filters,
				success: function (result) {
					if (result.results.length > 0) {
						thes.onDialogoFacturar();
					} else {
						thes.facturar();
					}
				},
				error: function (err) {

				}
			});

		},

		facturar: function () {

			var json = thes.getView().getModel("facturar").getData();

			thes.facturarOrden(json);

		},

		cierre: function () {

			var indices = this.byId("stTablaAbiertas").getTable().getSelectedIndices();

			if (indices.length > 1) {
				sap.m.MessageBox.error("Seleccione un registro");
			}


			var reg = this.byId("stTablaAbiertas").getTable().getContextByIndex(indices[0]).getProperty();

			var json = {
				HdId: "1",
				Scpuser: userInfo.loginName,
				Scpapp: Scpapp,
				Aufnr: reg.aufnr,
				FlgClse: true,
				CfdiUse: "",
				Zlsch: ""
			};

			thes.OrdenCierre(json);

		},

		onViewPDF: function (data, nombre) {
			var datos = data;
			var objbuilder = '';
			objbuilder += ('<object width="100%" height="100%" data="data:application/pdf;base64,');
			objbuilder += (datos);
			objbuilder += ('" type="application/pdf" class="internal">');
			objbuilder += ('<embed src="data:application/pdf;base64,');
			objbuilder += (datos);
			objbuilder += ('" type="application/pdf" />');
			objbuilder += ('</object>');
			var win = window.open("#", "_blank");
			var title = nombre
			win.document.write('<html><title>' + title +
				'</title><body style="margin-top:0px; margin-left: 0px; margin-right: 0px; margin-bottom: 0px;">');
			win.document.write(objbuilder);
			win.document.write('</body></html>');
			sap.ui.core.BusyIndicator.hide();
		},

		facturarOrden: function (json) {
			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/FactOrdSet", json, {
				success: function (result, status) {
					thes.byId("stTablaCerradas").getTable().clearSelection();
					thes.byId("stTablaCerradas").getTable().getModel().refresh();
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						var flag = true;

						for (var i = 0; i < mensajes.length; i++) {
							if (mensajes[i].Type === "error") {
								flag = false;
							}
						}

						if (flag) {
							thes.onImprimirPreFactura(result.Aufnr);
						}

						thes.mostrarMensajes(mensajes, thes);
					}

					sap.ui.core.BusyIndicator.hide();
					thes.byId("dlgFacturarOrden").close();
				},
				error: function (err) {
					sap.m.MessageBox.error("Error al facturar");
					sap.ui.core.BusyIndicator.hide();
				}
			});
		},

		OrdenCierre: function (json) {

			var json2 = {};

			json2.Aufnr = json.Aufnr;

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdValdtSet", json2, {
				success: function (result, status) {
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						var flag = true;

						for (var i = 0; i < mensajes.length; i++) {
							if (mensajes[i].Type === "error") {
								flag = false;
							}
						}

						if (flag) {

							sap.ui.core.BusyIndicator.show();
							Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenCierreSet", json, {
								success: function (result, status) {
									thes.byId("stTablaAbiertas").getTable().clearSelection();
									thes.byId("stTablaAbiertas").getTable().getModel().refresh();
									if (status.headers["sap-message"]) {
										var mensaje = JSON.parse(status.headers["sap-message"]);
										var mensajes = thes.armarMensaje(mensaje);
										thes.mostrarMensajes(mensajes, thes);
									}

									sap.ui.core.BusyIndicator.hide();
								},
								error: function (err) {
									sap.m.MessageBox.error("Error en el Cierre");
									sap.ui.core.BusyIndicator.hide();
								}
							});

						} else {
							thes.mostrarMensajes(mensajes, thes);
						}


					}
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerCondicionPago: function () {

			Z_OD_SCP_BASRRC001_ORD_SRV.read("/ZCDS_VW_PAYMENTTERMS", {
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "condicionPago");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onDialogoFacturar: function () {

			var indices = this.byId("stTablaCerradas").getTable().getSelectedIndices();

			// if (indices.length > 1) {
			// 	sap.m.MessageBox.error("Seleccione un registro");
			// }

			var reg = this.byId("stTablaCerradas").getTable().getContextByIndex(indices[0]).getProperty();

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgFacturarOrden")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.FacturarOrden",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgFacturarOrden").open();
			}

			thes.obtenerCfdiUse();
			thes.obtenerZlsch();
			thes.obtenerCondicionPago();

		},

		obtenerCfdiUse: function () {

			Z_OD_SCP_BAVNVT001_SRV.read("/ZCDS_VW_SD17", {
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "cfdiUse");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerZlsch: function () {

			var filters = [];
			var filter = new sap.ui.model.Filter("bukrs", "EQ", '4100');
			filters.push(filter);

			Z_OD_SCP_BASRRC001_ORD_SRV.read("/ZCDS_VW_BASRRC001_108", {
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "zlsch");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onImprimirPreFactura: function (Aufnr) {

			var filters = [];

			var filter = new sap.ui.model.Filter("aufnr", sap.ui.model.FilterOperator.EQ, Aufnr);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show(4);
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/PDFPreFacturaSet", {
				filters: filters,
				success: function (result) {
					thes.onViewPDF(result.results[0].PDF, "SmartForms");
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerKPICitasEstado: function () {

			ZCDS_VW_BASRRC001_116_CDS.read("/ZCDS_VW_BASRRC001_116", {
				urlParameters: {
					"$orderby": "user_stat"
				},
				success: function (result) {

					var estatus;
					var sumCant = 0;
					var datos = [];

					for (var i = 0; i < result.results.length; i++) {

						if (i === 0) {
							var sumCant = result.results[i].cant;
							var estatus = result.results[i].user_stat;
							var estatusDesc = result.results[i].user_stat_des;
						} else {
							if (estatus === result.results[i].user_stat) {
								sumCant = sumCant + result.results[i].cant;
								estatus = result.results[i].user_stat;
								estatusDesc = result.results[i].user_stat_des;
								if (result.results.length === (i + 1)) {

									var reg = {};
									reg.descr = estatusDesc;
									reg.valor = sumCant
									datos.push(reg);
								}
							} else {
								if (result.results.length === (i + 1)) {
									sumCant = sumCant + result.results[i].cant;
									estatus = result.results[i].user_stat;
									estatusDesc = result.results[i].user_stat_des;
								}

								var reg = {};
								reg.descr = estatusDesc;
								reg.valor = sumCant
								datos.push(reg);

								sumCant = result.results[i].cant;
								estatus = result.results[i].user_stat;
								estatusDesc = result.results[i].user_stat_des;
							}
						}
					}

					// for (var i = 0; i < result.results.length; i++) {
					// 	var interactiveDonutChartSegment = new sap.suite.ui.microchart.InteractiveDonutChartSegment({
					// 		value: result.results[i].cant,
					// 		label: result.results[i].user_stat_des,
					// 		displayedValue: result.results[i].cant
					// 	});
					// 	intDonChaKPICitaEstado.addSegment(interactiveDonutChartSegment);
					// }
					var jsonModel = new sap.ui.model.json.JSONModel(datos);
					thes.byId("idVizFrame").setModel(jsonModel);
				},
				error: function (err) {

				}
			});

		},

		onFacturarOrden: function () {

			var json = thes.getView().getModel("facturar").getData();

			if (json.CfdiUse === "") {
				sap.m.MessageBox.error("Ingrese el uso CFDI");
				return;
			};

			if (json.Zlsch === "") {
				sap.m.MessageBox.error("Ingrese la vía de pago");
				return;
			};

			if (json.Zterm === "") {
				sap.m.MessageBox.error("Ingrese la condición de pago");
				return;
			};

			var cuentasBancarias = thes.getView().getModel("cuentasBancarias").getData();

			cuentasBancarias = cuentasBancarias.find(element => element.BankNumber === json.Banknumber);

			if (cuentasBancarias) {
				json.Bankaccount = cuentasBancarias.BankAccount;
			}

			thes.facturarOrden(json);

		},


		obtenerToXentry: function (aufnr) {

			var filters = [];

			var filter = new sap.ui.model.Filter("NoOrd", sap.ui.model.FilterOperator.EQ, Aufnr);
			filters.push(filter);

			Z_OD_SCP_BASRRC001_ORD_SRV.read("/ZCDS_VW_XENTRY_URL", {
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "xentry_url");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onBeforeRebindTablaAbiertas: function (oEvent) {
			var binding = oEvent.getParameter("bindingParams");
			var oFilter = new sap.ui.model.Filter("estatus", sap.ui.model.FilterOperator.EQ, "A");
			binding.filters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("bukrs", sap.ui.model.FilterOperator.EQ, bukrs);
			binding.filters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("werks", sap.ui.model.FilterOperator.EQ, werks);
			binding.filters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("stort", sap.ui.model.FilterOperator.EQ, stort);
			binding.filters.push(oFilter);

			var oSorter = new sap.ui.model.Sorter("aufnr", true);
			binding.sorter.push(oSorter);
			//binding.parameters["expand"] = "to_Xentry";

		},

		onBeforeRebindTablaAbiertas2: function (oEvent) {
			this.getView().byId("stTablaAbiertas").rebindTable();
			var oFilters = [];
			var oFilter = new sap.ui.model.Filter("estatus", sap.ui.model.FilterOperator.EQ, "A");
			oFilters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("bukrs", sap.ui.model.FilterOperator.EQ, bukrs);
			oFilters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("werks", sap.ui.model.FilterOperator.EQ, werks);
			oFilters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("stort", sap.ui.model.FilterOperator.EQ, stort);
			oFilters.push(oFilter);

			var oSorter = new sap.ui.model.Sorter("aufnr", true);
			this.getView().byId("stTablaAbiertas").getTable().getBinding("rows").filter(oFilters);
			this.getView().byId("stTablaAbiertas").getTable().getBinding("rows").sort(oSorter);
		},

		onBeforeRebindTablaCerradas: function (oEvent) {
			this.getView().byId("stTablaCerradas").rebindTable();
			var oFilters = [];
			var oFilter = new sap.ui.model.Filter("estatus", sap.ui.model.FilterOperator.EQ, "C");
			oFilters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("bukrs", sap.ui.model.FilterOperator.EQ, bukrs);
			oFilters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("werks", sap.ui.model.FilterOperator.EQ, werks);
			oFilters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("stort", sap.ui.model.FilterOperator.EQ, stort);
			oFilters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("Factura", sap.ui.model.FilterOperator.EQ, null);
			oFilters.push(oFilter);

			var oSorter = new sap.ui.model.Sorter("aufnr", true);
			this.getView().byId("stTablaCerradas").getTable().getBinding("rows").filter(oFilters);
			this.getView().byId("stTablaCerradas").getTable().getBinding("rows").sort(oSorter);
		},

		onBeforeRebindTablaAnuladas: function (oEvent) {
			this.getView().byId("stTablaAnuladas").rebindTable();
			var oFilters = [];
			var oFilter = new sap.ui.model.Filter("estatus", sap.ui.model.FilterOperator.EQ, "N");
			oFilters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("bukrs", sap.ui.model.FilterOperator.EQ, bukrs);
			oFilters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("werks", sap.ui.model.FilterOperator.EQ, werks);
			oFilters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("stort", sap.ui.model.FilterOperator.EQ, stort);
			oFilters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("Factura", sap.ui.model.FilterOperator.EQ, null);
			oFilters.push(oFilter);

			var oSorter = new sap.ui.model.Sorter("aufnr", true);

			var filters2 = [];

			var filter = new sap.ui.model.Filter({
				filters: oFilters,
				and: true
			});

			filters2.push(filter);

			this.getView().byId("stTablaAnuladas").getTable().getBinding("rows").filter(filters2);
			this.getView().byId("stTablaAnuladas").getTable().getBinding("rows").sort(oSorter);
		},

		onBeforeRebindTablaFacturadas: function (oEvent) {
			this.getView().byId("stTablaFacturadas").rebindTable();
			var oFilters = [];
			var oFilter = new sap.ui.model.Filter("estatus", sap.ui.model.FilterOperator.EQ, "C");
			oFilters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("bukrs", sap.ui.model.FilterOperator.EQ, bukrs);
			oFilters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("werks", sap.ui.model.FilterOperator.EQ, werks);
			oFilters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("stort", sap.ui.model.FilterOperator.EQ, stort);
			oFilters.push(oFilter);

			var oFilter = new sap.ui.model.Filter("Factura", sap.ui.model.FilterOperator.NE, null);
			oFilters.push(oFilter);

			var oSorter = new sap.ui.model.Sorter("aufnr", true);

			var filters2 = [];

			var filter = new sap.ui.model.Filter({
				filters: oFilters,
				and: true
			});

			filters2.push(filter);

			this.getView().byId("stTablaFacturadas").getTable().getBinding("rows").filter(filters2);
			this.getView().byId("stTablaFacturadas").getTable().getBinding("rows").sort(oSorter);
		},

		toNumber: function (numero) {
			if (numero) {
				return Math.round(numero);
			} else {
				return 0;
			}
		},

		obtenerAsesor: function () {

		},

		obtenerCentro: function () {

			var filters = [];
			var filter = new sap.ui.model.Filter("Werks", "EQ", werks);
			filters.push(filter);

			Z_OD_SCP_CORE_0001_SRV.read("/CentrosSet", {
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "centro");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onGuardarCabecera: function () {

			if (thes.byId("cbSociedadCabecera").getSelectedKey() === undefined || thes.byId("cbSociedadCabecera").getSelectedKey() === "") {
				sap.m.MessageBox.error("Seleccione una sociedad");
				return;
			}

			if (thes.byId("cbCentroCabecera").getSelectedKey() === undefined || thes.byId("cbCentroCabecera").getSelectedKey() === "") {
				sap.m.MessageBox.error("Seleccione un centro");
				return;
			}

			if (thes.byId("cbEmplazamientoCabecera").getSelectedKey() === undefined || thes.byId("cbEmplazamientoCabecera").getSelectedKey() === "") {
				sap.m.MessageBox.error("Seleccione un emplazamiento");
				return;
			}

			var cabecera = thes.getView().getModel("datosCabecera").getData();
			cabecera.bukrs = thes.byId("cbSociedadCabecera").getSelectedKey();
			cabecera.butxt = thes.byId("cbSociedadCabecera").getValue();
			cabecera.werks = thes.byId("cbCentroCabecera").getSelectedKey();
			cabecera.name1 = thes.byId("cbCentroCabecera").getValue();
			cabecera.stort = thes.byId("cbEmplazamientoCabecera").getSelectedKey();
			cabecera.ktext = thes.byId("cbEmplazamientoCabecera").getValue();

			bukrs = cabecera.bukrs;
			werks = cabecera.werks;
			stort = cabecera.stort;

			sessionStorage.bukrs = bukrs;
			sessionStorage.werks = werks;
			sessionStorage.stort = stort;

			thes.getView().getModel("datosCabecera").refresh();

			this.byId("stTablaAbiertas").rebindTable();
			this.byId("stTablaCerradas").rebindTable();
			this.byId("stTablaAnuladas").rebindTable();

			thes.byId("dlgEditarCabeceraHome").close();
		},

		onDialogoCabeceraHomeEdit: function () {

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgEditarCabeceraHome")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.EditarCabeceraHome",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
					thes.byId("cbSociedadCabecera").setSelectedKey(bukrs);
					thes.byId("cbCentroCabecera").setSelectedKey(werks);
					thes.byId("cbEmplazamientoCabecera").setSelectedKey(stort);
				});
			} else {
				this.byId("dlgEditarCabeceraHome").open();
				thes.byId("cbSociedadCabecera").setSelectedKey(bukrs);
				thes.byId("cbCentroCabecera").setSelectedKey(werks);
				thes.byId("cbEmplazamientoCabecera").setSelectedKey(stort);
			}

		},

		obtenerEmplazamiento: function (centro) {

			var filters = [];
			var filter = new sap.ui.model.Filter("werks", "EQ", centro);
			filters.push(filter);
			var filter = new sap.ui.model.Filter("stand", "EQ", stort);
			filters.push(filter);

			Z_OD_SCP_BASRRC001_ORD_SRV.read("/OrdenEmplazamiento", {
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "emplazamiento");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onItemPress: function (oEvent) {
			//var sPath = oEvent.getParameter("listItem").getBindingContext().getPath();
			//var reg = oEvent.getParameter("listItem").getBindingContext().getProperty();

			var reg = oEvent.getParameters().item.getBindingContext().getProperty();

			var Hojap = reg.Hojap;

			if (Hojap === "") {
				Hojap = "F";
			}

			var Father = reg.father;

			if (Father === "") {
				Father = "F";
			} else {
				Father = "V";
			}

			if (reg.StatXentry === "S" && reg.estatus === "A") {
				//sap.m.MessageBox.information("Orden enviada a Xentry");
				window.open(reg.browserurl, '_blank');
				//return;
			}

			//thes.obtenerToXentry(reg.aufnr);

			sap.cabecera = reg;

			this.getRouter().navTo("detalle", {
				Aufnr: reg.aufnr,
				Werks: reg.werks,
				Maufnr: reg.maufnr,
				Father: Father,
				Hojap: Hojap,
				Kunum: reg.kunum,
				Expres: reg.expres,
				Eqfnr: reg.eqfnr
			});
		},

		onDialogoOrdenServicio: function () {

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgAgregarOrdenes")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.AgregarOrdenes",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgAgregarOrdenes").open();
			}

			thes.obtenerCanalFacturacion();
			thes.ordenNueva();
			thes.obtenerCentro();
			thes.newVehiculo();
		},

		onChCento: function (oEvent) {

			var centro = oEvent.getSource().getSelectedKey();

			if (centro === "") {
				thes.byId("cbEmplazamiento").setEditable(false);
			} else {
				thes.byId("cbEmplazamiento").setEditable(true);
			}

			thes.obtenerEmplazamiento(centro);
		},

		onSbVin: function (oEvent) {

			var vin = oEvent.getSource().getValue();

			if (vin === "") {
				sap.m.MessageBox.error("Ingrese una placa");
				return;
			}

			var filters = [];
			var filter = new sap.ui.model.Filter("Nrochasis", "EQ", vin);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_SRV.read("/VehiculoSet", {
				filters: filters,
				success: function (result) {

					if (result.results.length === 0) {
						sap.m.MessageBox.error("El Vin no existe");
						sap.ui.core.BusyIndicator.hide();
						return;
					}

					if (result.results[0].FlgAF === "X") {
						sap.m.MessageBox.error("El vehículo es un Activo Fijo");
						sap.ui.core.BusyIndicator.hide();
						return;
					}

					var jsonModel = new sap.ui.model.json.JSONModel(result.results[0]);
					thes.getView().setModel(jsonModel, "vehiculo");

					var ordenNueva = thes.getView().getModel("ordenNueva").getData();
					ordenNueva.Idvehi = result.results[0].Idvehi;
					thes.getView().getModel("ordenNueva").refresh();

					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		newVehiculo: function () {

			var vehiculo = {
				Nrochasis: "",
				Matricula: "",
				Marca: "",
				Modelotecnico: "",
				Modelo: ""
			};

			var jsonModel = new sap.ui.model.json.JSONModel(vehiculo);
			thes.getView().setModel(jsonModel, "vehiculo");

		},

		onSbMatricula: function (oEvent) {

			var matricula = oEvent.getSource().getValue();

			if (matricula === "") {
				sap.m.MessageBox.error("Ingrese una placa");
				return;
			}

			var filters = [];
			var filter = new sap.ui.model.Filter("Matricula", "EQ", matricula);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_SRV.read("/VehiculoSet", {
				filters: filters,
				success: function (result) {

					if (result.results.length === 0) {
						sap.m.MessageBox.error("Matricula no existe");
						sap.ui.core.BusyIndicator.hide();
						return;
					}

					if (result.results[0].FlgAF === "X") {
						sap.m.MessageBox.error("El vehículo es un Activo Fijo");
						sap.ui.core.BusyIndicator.hide();
						return;
					}

					var jsonModel = new sap.ui.model.json.JSONModel(result.results[0]);
					thes.getView().setModel(jsonModel, "vehiculo");

					var ordenNueva = thes.getView().getModel("ordenNueva").getData();
					ordenNueva.Idvehi = result.results[0].Idvehi;
					thes.getView().getModel("ordenNueva").refresh();

					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		ordenNueva: function () {

			var json = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Werks": werks,
				"Idvehi": "",
				"FechaInicio": "",
				"Descripcion": "",
				"Objty": "",
				"Objid": "",
				"Ilart": "",
				"Stand": stort
			};

			var jsonModel = new sap.ui.model.json.JSONModel(json);
			thes.getView().setModel(jsonModel, "ordenNueva");

		},

		onDialogoCabeceraEdit: function () {

			var indices = this.byId("stTablaAbiertas").getTable().getSelectedIndices();

			if (indices.length === 0) {
				return;
			}

			var reg = this.byId("stTablaAbiertas").getTable().getContextByIndex(indices[0]).getProperty();


			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgEditarCabecera")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "dms4100ui.bacrcr001ord.view.EditarCabecera",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgEditarCabecera").open();
			}


			var FechaPromesa = "";
			var HoraPromesa = "";
			if (reg.FechaHoraPromesa.length == 14) {
				FechaPromesa = reg.FechaHoraPromesa.substring(0, 8);
				FechaPromesa = thes.stringTodate(FechaPromesa);
				HoraPromesa = reg.FechaHoraPromesa.substring(8, 15);
			}

			if (FechaPromesa === undefined || FechaPromesa === "") {
				FechaPromesa = new Date();
			}

			var cabecera = {
				"HdId": "1",
				"Scpuser": userInfo.loginName,
				"Scpapp": Scpapp,
				"Aufnr": reg.aufnr,
				"Descripcion": reg.ktext,
				"Beber": reg.beber,
				"UserStatInt": reg.user_stat_int,
				"Pagador": reg.pagador,
				"Siniestro": reg.siniestro,
				"FechaPromesa": FechaPromesa,
				"FechaHora": HoraPromesa,
				"Stand": reg.stort,
				"Kilometraje": reg.kilometraje,
				"Gasolina": reg.gasolina,
				"Idcono": reg.idcono,
				"FechaPromesa2": FechaPromesa,
				"FechaHora2": HoraPromesa,
				"Estatus": reg.estatusc,
				"Contacto": reg.contacto
			};

			var jsonModel = new sap.ui.model.json.JSONModel(cabecera);
			thes.getView().setModel(jsonModel, "EditCabecera");

			thes.obtenerPagador(thes, reg.kunum, reg.vkorg, reg.vtweg);
			thes.obtenerEstatus(thes);
			thes.obtenerUbicacion(thes, reg.werks);
			thes.ObtenerPersonasContactos(thes, reg.kunum);
			thes.obtenerTaller(thes, reg.werks);
			thes.obtenerEstadosGarantias(thes);
		},

		onEditarCabecera: function () {

			var reg = thes.getView().getModel("EditCabecera").getData();

			if (reg.FechaPromesa2) {
				if (reg.FechaPromesa2 != reg.FechaPromesa || reg.FechaHora != reg.FechaHora2) {
					var fecha = this.dateToFechaSAP(reg.FechaPromesa2);
					var fechaHora2 = fecha + reg.FechaHora2;
					fechaHora2 = thes.stringTodateTime(fechaHora2);
					if (fechaHora2.getTime() < new Date().getTime()) {
						sap.m.MessageBox.error("La fecha y hora promesa debe ser mayor a la actual");
						return;
					} else {
						reg.FechaPromesa = reg.FechaPromesa2;
						reg.FechaHora = reg.FechaHora2;
					}
				}
			}

			if (reg.FechaPromesa) {
				reg.FechaPromesa = thes.dateToFechaSAP(reg.FechaPromesa);
			}

			delete reg["FechaPromesa2"];
			delete reg["FechaHora2"];

			if (reg.Kilometraje) {
				reg.Kilometraje = parseFloat(reg.Kilometraje.replace(',', ""));				
			}

			reg.Kilometraje = reg.Kilometraje.toString();

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenChangeOrdenSet", reg, {
				success: function (result, status) {
					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);
						var mensajes = thes.armarMensaje(mensaje);
						thes.mostrarMensajes(mensajes, thes);
					}
					thes.byId("stTablaAbiertas").getTable().clearSelection();
					thes.byId("stTablaAbiertas").getTable().getModel().refresh();
					sap.ui.core.BusyIndicator.hide();
					thes.byId("dlgEditarCabecera").close();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
					thes.byId("dlgEditarCabecera").close();
				}
			});

		},

		onCrearOrdenServicio: function () {

			var reg = thes.getView().getModel("ordenNueva").getData();

			if (reg.Werks === "") {
				sap.m.MessageBox.error("Ingrese un centro");
				return;
			}

			if (reg.Idvehi === "") {
				sap.m.MessageBox.error("Ingrese una matricula");
				return;
			}

			if (reg.FechaInicio === "") {
				sap.m.MessageBox.error("Ingrese una fecha de inicio");
				return;
			}

			if (reg.Descripcion === "") {
				sap.m.MessageBox.error("Ingrese una descripción");
				return;
			}

			if (reg.Ilart === "") {
				sap.m.MessageBox.error("Ingrese un canal de facturación");
				return;
			}

			if (reg.Stand === "") {
				sap.m.MessageBox.error("Ingrese un Emplazamiento");
				return;
			}

			// var jsonServicio = {
			// 	"HdId": "1",
			// 	"Scpuser": "",
			// 	"Scpapp": "",
			// 	"Werks": "4100",
			// 	"Idvehi": "6f019188-ef65-4ec1-b62e-7310b621f9b6",
			// 	"FechaInicio": "20200908",
			// 	"Descripcion": "Pruebas desde el servciio",
			// 	"Objty": "A",
			// 	"Objid": "10000006",
			// 	"Ilart": "ZC1",
			// 	"Stand": "YB_5501"
			// };

			reg.Objty = "A";

			if (!userAttributes.NumeroEmpleado) {
				userAttributes.NumeroEmpleado = "";
			}

			reg.Objid = userAttributes.NumeroEmpleado;

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.create("/OrdenCreateOrdenSet", reg, {
				success: function (result, status) {
					thes.byId("stTablaAbiertas").getTable().clearSelection();
					thes.byId("stTablaAbiertas").getTable().getModel().refresh();

					if (status.headers["sap-message"]) {
						var mensaje = JSON.parse(status.headers["sap-message"]);

						var mensajes = thes.armarMensaje(mensaje);

						thes.mostrarMensajes(mensajes, thes);
					}

					sap.ui.core.BusyIndicator.hide();
					thes.byId("dlgAgregarOrdenes").close();
				},
				error: function (err) {
					thes.byId("stTablaAbiertas").getTable().clearSelection();
					thes.byId("stTablaAbiertas").getTable().getModel().refresh();
					//thes.byId("dlgAgregarOrdenes").close();
					sap.m.MessageBox.error("Error al guardar");
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		onDeleteOrdenServicio: function (oEvent) {
			var indices = this.byId("stTablaAbiertas").getTable().getSelectedIndices();

			for (var i = 0; i < indices.length; i++) {
				var reg = this.byId("stTablaAbiertas").getTable().getContextByIndex(indices[i]).getProperty();

				var eliminar = {
					"HdId": "1",
					"Scpuser": userInfo.loginName,
					"Scpapp": Scpapp,
					"Aufnr": reg.aufnr
				};

				sap.ui.core.BusyIndicator.show();
				Z_OD_SCP_BASRRC001_RES_SRV.create("/CancelarOrdenSet", eliminar, {
					success: function (result, status) {
						thes.byId("stTablaAbiertas").getTable().clearSelection();
						thes.byId("stTablaAbiertas").getTable().getModel().refresh();

						if (status.headers["sap-message"]) {
							var mensaje = JSON.parse(status.headers["sap-message"]);
							var mensajes = thes.armarMensaje(mensaje);
							thes.mostrarMensajes(mensajes, thes);
						}
						sap.ui.core.BusyIndicator.hide();
					},
					error: function (err) {
						thes.byId("stTablaAbiertas").getTable().clearSelection();
						thes.byId("stTablaAbiertas").getTable().getModel().refresh();
						sap.ui.core.BusyIndicator.hide();
						sap.m.MessageBox.error("Error al eliminar!");
					}
				});

			}

		},

		obtenerCuentasBancarias: function (pagador) {

			var filters = [];

			var filter = new sap.ui.model.Filter("BusinessPartner", sap.ui.model.FilterOperator.EQ, pagador);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/ZCDS_VW_CUSTBNKACC", {
				filters: filters,
				success: function (result) {

					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "cuentasBancarias");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerCanalFacturacion: function () {

			var filters = [];

			var filter = new sap.ui.model.Filter("flgMO", sap.ui.model.FilterOperator.EQ, true);
			filters.push(filter);

			var filter = new sap.ui.model.Filter("werks", "EQ", werks);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/OrdenCatCanalFact", {
				filters: filters,
				success: function (result) {

					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "canalFacturacion");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		}

	});
});