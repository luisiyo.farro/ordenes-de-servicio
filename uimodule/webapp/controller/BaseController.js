sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/ui/model/SimpleType",
	"sap/ui/model/ValidateException"
], function (Controller, History, SimpleType, ValidateException) {
	"use strict";
	var Scpapp = "bacrcr001ord";
	return Controller.extend("dms4100ui.bacrcr001ord.controller.BaseController", {

		getRouter: function () {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		obtenerPagador: function (thes, kunum, vkorg, vtweg) {

			var filters = [];
			var filter = new sap.ui.model.Filter("CodCli", "EQ", kunum);
			filters.push(filter);
			var filter = new sap.ui.model.Filter("OrgVent", "EQ", vkorg);
			filters.push(filter);
			var filter = new sap.ui.model.Filter("CanDist", "EQ", vtweg);
			filters.push(filter);

			var Z_OD_SCP_BAVNVT001_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BAVNVT001_SRV");
			Z_OD_SCP_BAVNVT001_SRV.read("/ZCDS_VW_SD18", {
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "pagador");
				},
				error: function (err) {

				}
			});

		},

		obtenerEstatus: function (thes) {

			var Z_OD_SCP_BASRRC001_ORD_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_ORD_SRV");
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/OrdenEstatus", {
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "estatus");
				},
				error: function (err) {

				}
			});

		},

		obtenerUbicacion: function (thes, werks) {

			var filters = [];
			var filter = new sap.ui.model.Filter("werks", "EQ", werks);
			filters.push(filter);

			var Z_OD_SCP_BASRRC001_ORD_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_ORD_SRV");
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/OrdenUbicacion", {
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "ubicacion");
				},
				error: function (err) {

				}
			});

		},		

		ObtenerPersonasContactos: function (thes, kunum) {

			var filters = [];

			var filter = new sap.ui.model.Filter("BusinessPartnerCompany", sap.ui.model.FilterOperator.EQ, kunum);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show();
			var Z_OD_SCP_BASRRC001_ORD_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_ORD_SRV");
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/ZCDS_VW_BUSPARTCONT", {
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "personasContactos");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerTaller: function (thes, werks) {

			var filters = [];

			var filter = new sap.ui.model.Filter("werks", sap.ui.model.FilterOperator.EQ, werks);
			filters.push(filter);

			sap.ui.core.BusyIndicator.show(4);
			var Z_OD_SCP_BASRRC001_ORD_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_ORD_SRV");
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/OrdenEmplazamiento", {
				filters: filters,
				success: function (result) {
					var jsonModel2 = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel2, "taller");
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerEstadosGarantias: function (thes) {

			sap.ui.core.BusyIndicator.show();
			var Z_OD_SCP_BASRRC001_ORD_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_ORD_SRV");
			Z_OD_SCP_BASRRC001_ORD_SRV.read("/CatalogoEstatusSet", {
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "estadosGarantias");
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		obtenerControlesObjetos: function (roleName) {
			var thes = this;
			for (var i = 0; i < roleName.length; i++) {
				if (i === 0) {
					var roleNameFilter = "roleName eq '" + roleName[i] + "'";
				} else {
					roleNameFilter = roleNameFilter + " or roleName eq '" + roleName[i] + "'";
				}

			}

			var url = "/dms4100-security/xsuaa-api-access/FieldControlReport?$filter=appId eq '" + Scpapp + "' and (" + roleNameFilter + ")";

			//var url = "/dms4100-security/xsuaa-api-access/FieldControlReport";

			sap.ui.core.BusyIndicator.show();
			$.ajax({
				url: url,
				async: false,
				success: function (result) {

					for (var i = 0; i < result.value.length; i++) {
						if (thes.byId(result.value[i].fieldName)) {
							thes.byId(result.value[i].fieldName).setVisible(result.value[0].visible);
							thes.byId(result.value[i].fieldName).setEnabled(result.value[0].enabled);
						}
					}

					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});

		},

		zfill: function (number, width) {
			var numberOutput = Math.abs(number); /* Valor absoluto del número */
			var length = number.toString().length; /* Largo del número */
			var zero = "0"; /* String de cero */

			if (width <= length) {
				if (number < 0) {
					return ("-" + numberOutput.toString());
				} else {
					return numberOutput.toString();
				}
			} else {
				if (number < 0) {
					return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
				} else {
					return ((zero.repeat(width - length)) + numberOutput.toString());
				}
			}
		},

		onNavBack: function (oEvent) {
			var oHistory, sPreviousHash;

			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getRouter().navTo("home", {}, true /*no history*/);
			}
		},

		dateToHora: function (fecha) {

			if (fecha) {
				fecha = new Date(fecha.ms);
				var hour, minu, second;
				hour = fecha.getHours();
				if (hour.toString().length === 1) {
					hour = "0" + hour;
				}

				minu = fecha.getMinutes();
				if (minu.toString().length === 1) {
					minu = "0" + minu;
				}

				second = fecha.getSeconds();
				if (second.toString().length === 1) {
					second = "0" + second;
				}

				return hour + ":" + minu + ":" + second;
			}

			return "";

		},

		dateToFechaSAP: function (fecha) {
			if (fecha) {
				var day, month;
				day = fecha.getDate();
				if (day.toString().length === 1) {
					day = "0" + day;
				}

				month = fecha.getMonth();
				month = month + 1;
				if (month.toString().length === 1) {
					month = "0" + month;
				}

				return fecha.getFullYear().toString() + month.toString() + day.toString();
			}

			return "";
		},

		dateToFecha: function (fecha) {

			if (fecha) {
				var day, month;
				day = fecha.getDate();
				if (day.toString().length === 1) {
					day = "0" + day;
				}

				month = fecha.getMonth();
				if (month.toString().length === 1) {
					month = "0" + month;
				}

				return day + "/" + (Number(month) + 1) + "/" + fecha.getFullYear();
			}

			return "";

		},

		dateToFecha2: function (fecha) {

			if (fecha) {
				var day, month;
				day = fecha.getDate();
				if (day.toString().length === 1) {
					day = "0" + day;
				}

				month = fecha.getMonth();
				month = month + 1;
				if (month.toString().length === 1) {
					month = "0" + month;
				}

				return fecha.getFullYear() + "-" + month + "-" + day;
			}

			return "";

		},

		formatNumber: function (NumberFormat) {
			if (NumberFormat !== "") {
				var oFloatNumberFormat = NumberFormat.getFloatInstance({
					maxFractionDigits: 2,
					minFractionDigits: 2,
					groupingEnabled: true
				}, sap.ui.getCore().getConfiguration().getLocale());
				return oFloatNumberFormat;
			}
		},


		DateTimeToView: function (value) {

			if (value) {
				if (value !== "0") {
					var day, month, year, hour, min, seg;
					day = value.getDate();
					if (day.toString().length === 1) {
						day = "0" + day;
					}

					month = value.getMonth();
					if (month.toString().length === 1) {
						month = "0" + month;
					}

					year = value.getFullYear();

					hour = value.getHours();
					if (hour.toString().length === 1) {
						hour = "0" + hour;
					}

					min = value.getMinutes();
					if (min.toString().length === 1) {
						min = "0" + min;
					}

					seg = value.getSeconds();
					if (seg.toString().length === 1) {
						seg = "0" + seg;
					}


					return day + "/" + month + "/" + year + " " + hour + ":" + min + ":" + seg;
				} else {
					return "";
				}
			} else {
				return "";
			}

		},


		toDateMoment: function (value) {

			if (value) {
				if (value !== "0") {
					var day, month, year, hour, min, seg;
					day = value.getDate();
					if (day.toString().length === 1) {
						day = "0" + day;
					}

					month = value.getMonth();
					if (month.toString().length === 1) {
						month = "0" + month;
					}

					year = value.getFullYear();

					hour = value.getHours();
					if (hour.toString().length === 1) {
						hour = "0" + hour;
					}

					min = value.getMinutes();
					if (min.toString().length === 1) {
						min = "0" + min;
					}

					seg = value.getSeconds();
					if (seg.toString().length === 1) {
						seg = "0" + seg;
					}


					return year + "," + month + "," + day + "," + hour + ":" + min + ":" + seg;
				} else {
					return "";
				}
			} else {
				return "";
			}

		},

		toDateView2: function (value) {

			if (value) {
				if (value !== "0") {
					var year = value.substring(0, 4);
					var mes = value.substring(4, 6);
					var dia = value.substring(6, 8);

					return dia + "-" + mes + "-" + year;
				} else {
					return "";
				}
			} else {
				return "";
			}

		},

		stringTodate: function (value) {

			var year = value.substring(0, 4);
			var mes = value.substring(4, 6) - 1;
			var dia = value.substring(6, 8);


			return new Date(year, mes, dia);

		},

		stringTodateTime: function (value) {

			var year = value.substring(0, 4);
			var mes = value.substring(4, 6) - 1;
			var dia = value.substring(6, 8);
			var hora = value.substring(8, 10);
			var minu = value.substring(10, 12);


			return new Date(year, mes, dia, hora, minu);

		},

		sumaFecha: function (d, fecha) {
			if (fecha) {
				var fechaTime = fecha.getTime();
				var dias = d * 24 * 60 * 60 * 1000;
				return (new Date(fechaTime + dias));
			}
			return "";
		},

		toDateView: function (value) {

			if (value) {
				if (value !== "0") {
					var year = value.substring(0, 4);
					var mes = value.substring(4, 6);
					var dia = value.substring(6, 8);
					var hora = value.substring(8, 10);
					var min = value.substring(10, 12);
					var seg = value.substring(12, 14);

					return dia + "/" + mes + "/" + year + " " + hora + ":" + min + ":" + seg;
				} else {
					return "";
				}
			} else {
				return "";
			}

		},

		toDateView2: function (value) {

			if (value) {
				if (value !== "0") {
					var year = value.substring(0, 4);
					var mes = value.substring(4, 6);
					var dia = value.substring(6, 8);

					return dia + "/" + mes + "/" + year;
				} else {
					return "";
				}
			} else {
				return "";
			}

		},

		armarMensaje: function (mensaje) {
			var mensajes = [];
			mensajes.push({
				"Type": mensaje.severity,
				"Message": mensaje.message
			});

			if (mensaje.details.length) {
				for (var i = 0; i < mensaje.details.length; i++) {
					mensajes.push({
						"Type": mensaje.details[i].severity,
						"Message": mensaje.details[i].message
					});
				}
			}
			return mensajes;
		},

		onCancelar: function (oEvent) {
			var id = oEvent.getSource().data("id");
			this.byId(id).close();
		},

		stringToFormatNumber: function (n) {

			n = Number(n);

			return n.toLocaleString("es-MX");

			//return n.toLocaleString(
			//	undefined, // leave undefined to use the browser's locale,
			// or use a string like 'en-US' to override it.
			//	{ minimumFractionDigits: 2 }
			//);

		},

		onIFrame: function (oEvent) {
			var spaceURL = window.location.origin;
			spaceURL = spaceURL.substring(0, 38);
			var deploySrc = spaceURL + 'usercard.cfapps.us10.hana.ondemand.com';
			var oSource = oEvent.getSource();
			var htmlPage = new sap.ui.core.HTML({
				preferDOM: true,
				content: "<iframe src='"+deploySrc+"' frameborder='0' height='500px' width='100%'></iframe>"
			});
			var Popover = new sap.m.Popover({ title: "Estructura Organizacional", verticalScrolling: false, placement: sap.m.PlacementType.Bottom, contentWidth: "500px", contentHeight: "500px", content: htmlPage });
			Popover.openBy(oSource);
		},

		customEMailType: SimpleType.extend("email", {
			formatValue: function (oValue) {
				return oValue;
			},

			parseValue: function (oValue) {
				//parsing step takes place before validating step, value could be altered here
				return oValue;
			},

			validateValue: function (oValue) {
				// The following Regex is only used for demonstration purposes and does not cover all variations of email addresses.
				// It's always better to validate an address by simply sending an e-mail to it.
				var rexMail = /^\w+[\w-+\.]*\@\w+([-\.]\w+)*\.[a-zA-Z]{2,}$/;
				if (!oValue.match(rexMail)) {
					throw new ValidateException("'" + oValue + "' is not a valid e-mail address");
				}
			}
		}),

		mostrarMensajes: function (mensajes, thes) {

			/*var mensajes = [{
				"Type": "E",
				"Message": "Mensaje de prueba"
			}];*/

			for (var i = 0; i < mensajes.length; i++) {
				switch (mensajes[i].Type) {
					case "E":
						mensajes[i].Type = "Error";
						break;
					case "S":
						mensajes[i].Type = "Success";
						break;
					case "W":
						mensajes[i].Type = "Warning";
						break;
					case "I":
						mensajes[i].Type = "Information";
						break;
					case "error":
						mensajes[i].Type = "Error";
						break;
					case "success":
						mensajes[i].Type = "Success";
						break;
					case "warning":
						mensajes[i].Type = "Warning";
						break;
					case "info":
						mensajes[i].Type = "Information";
						break;
				}
			}

			var oMessageTemplate = new sap.m.MessageItem({
				type: '{Type}',
				title: '{Message}'
			});

			var oModel = new sap.ui.model.json.JSONModel(mensajes);

			var oBackButton = new sap.m.Button({
				icon: sap.ui.core.IconPool.getIconURI("nav-back"),
				visible: false,
				press: function () {
					this.oMessageView.navigateBack();
					this.setVisible(false);
				}
			});

			this.oMessageView = new sap.m.MessageView({
				showDetailsPageHeader: false,
				itemSelect: function () {
					oBackButton.setVisible(true);
				},
				items: {
					path: "/",
					template: oMessageTemplate
				}
			});

			this.oMessageView.setModel(oModel);

			this.oDialog = new sap.m.Dialog({
				resizable: true,
				content: this.oMessageView,
				beginButton: new sap.m.Button({
					press: function (oEvent) {
						sap.ui.getCore().byId(oEvent.getSource().getParent().getId()).close();
					},
					text: "Close"
				}),
				customHeader: new sap.m.Bar({
					contentMiddle: [
						new sap.m.Text({
							text: "Mensajes"
						})
					],
					contentLeft: [oBackButton]
				}),
				contentHeight: "50%",
				contentWidth: "50%",
				verticalScrolling: false
			});

			this.oMessageView.navigateBack();
			this.oDialog.open();

		}

	});

});